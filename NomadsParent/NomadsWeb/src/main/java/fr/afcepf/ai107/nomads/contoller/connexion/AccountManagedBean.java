package fr.afcepf.ai107.nomads.contoller.connexion;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import fr.afcepf.ai107.nomads.administrateur.ibusiness.AdministrateurIBusiness;
import fr.afcepf.ai107.nomads.entities.Administrateur;
import fr.afcepf.ai107.nomads.entities.Masseur;
import fr.afcepf.ai107.nomads.entities.Partenaire;
import fr.afcepf.ai107.nomads.masseurs.ibusiness.MasseurIBusiness;
import fr.afcepf.ai107.nomads.partenaire.ibusiness.PartenaireIBusiness;



@ManagedBean(name ="mbAccount")
@SessionScoped
public class AccountManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Masseur masseur = new Masseur();
	private Administrateur admin = new Administrateur();
	private Partenaire partenaire = new Partenaire();
	private List<Masseur> masseurs;
	private List<Administrateur> admins;
	private List<Partenaire> partenaires;
	private String login;
	private String password;
	
	@EJB
	private MasseurIBusiness proxyAccountMasseurBusiness;
    @EJB
    private AdministrateurIBusiness proxyAccountAdminBusiness;
    @EJB
    private PartenaireIBusiness proxyAccountPartenaire;

	public String connection() {
		masseur = proxyAccountMasseurBusiness.connection(login, password);
		admin=proxyAccountAdminBusiness.connection(login, password);
		partenaire = proxyAccountPartenaire.connection(login, password);
		String retour = null;
		
		if(masseur != null ) {
			retour = "/AccueilMasseur.xhtml?faces-redirect=true";
		}else if(admin!=null) {
			retour = "/AccueilAdmin.xhtml?faces-redirect=true";
		}else if(partenaire!=null) {
			retour = "/AccueilPartenaire.xhtml?faces-redirect=true";
		}else {
			retour = "/Connexion.xhtml?faces-redirect=true";
		}		
		
		return retour;
	
	}
    
	public String cancel() {
		
		return "/AccueilUser.xhtml";
	}
	
	
	public String deconnexion() {
		masseur = new Masseur();
		admin = new Administrateur();
		partenaire = new Partenaire();
        login ="";
        password="";
        
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		session.invalidate();
		return "/Connexion.xhtml?faces-redirect=true";
    }
	
	
	public Partenaire getPartenaire() {
		return partenaire;
	}

	public void setPartenaire(Partenaire partenaire) {
		this.partenaire = partenaire;
	}

	public List<Partenaire> getPartenaires() {
		return partenaires;
	}

	public void setPartenaires(List<Partenaire> partenaires) {
		this.partenaires = partenaires;
	}

	public Administrateur getAdmin() {
		return admin;
	}

	public void setAdmin(Administrateur admin) {
		this.admin = admin;
	}

	public List<Administrateur> getAdmins() {
		return admins;
	}

	public void setAdmins(List<Administrateur> admins) {
		this.admins = admins;
	}
	public List<Masseur> getMasseurs() {
		return masseurs;
	}

	public void setMasseurs(List<Masseur> masseurs) {
		this.masseurs = masseurs;
	}

	public Masseur getMasseur() {
		return masseur;
	}

	public void setMasseur(Masseur masseur) {
		this.masseur = masseur;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
