package fr.afcepf.ai107.nomads.prestation.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai107.nomads.entities.InscriptionMasseurs;
import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.Masseur;
import fr.afcepf.ai107.nomads.entities.Partenaire;
import fr.afcepf.ai107.nomads.entities.Prestation;
import fr.afcepf.ai107.nomads.entities.TypePaiement;
import fr.afcepf.ai107.nomads.entities.TypePartenaire;
import fr.afcepf.ai107.nomads.idao.PartenaireIDao;
import fr.afcepf.ai107.nomads.idao.InterventionReelleIDao;
import fr.afcepf.ai107.nomads.idao.PrestationIDao;
import fr.afcepf.ai107.nomads.prestation.ibusiness.PrestationIBusiness;

@Remote(PrestationIBusiness.class)
@Stateless
public class PrestationBusiness implements PrestationIBusiness {

	@EJB
	private PrestationIDao proxyPrestationDao;
	
	@EJB
	private PartenaireIDao proxyPartenaireDao;
	
	@EJB
	private InterventionReelleIDao proxyInterventionReelleDao;
	
	@Override
	public List<Prestation> getListePrestation() {
		// TODO Auto-generated method stub
		List<Prestation> prestations = proxyPrestationDao.getAll();
		return prestations;
	}

	@Override
	public void ajoutPrestation(Prestation prestation) {
		// TODO Auto-generated method stub
		proxyPrestationDao.addPrestation(prestation);
	}

	@Override
	public void AjoutIntervention(InterventionReelle interventionReelle) {
		// TODO Auto-generated method stub
		proxyPrestationDao.addIntervention(interventionReelle);
	}

	//Utiliser dans une drop down list pour la création de la prestation
	@Override
	public List<Partenaire> getPartenaireParType(Integer idTypePartenaire) {
		// TODO Auto-generated method stub
		List<Partenaire> listePartenaireByType = null;
		listePartenaireByType = proxyPrestationDao.getPartenaireByTypePartenaire(idTypePartenaire);
		return listePartenaireByType;
	}

	//Utiliser dans une drop down list pour la création de la prestation
	@Override
	public List<TypePartenaire> getListeTypePartenaire() {
		// TODO Auto-generated method stub
		List<TypePartenaire> listeTypePartenaire = null;
		listeTypePartenaire = proxyPrestationDao.getTypePartenaire();
		return listeTypePartenaire;
	}

	//Pour afficher les masseurs selon le type partenaire sélectionné
	@Override
	public List<Masseur> getListeMasseurPourDDLCreationPrestation(Integer idTypePartenaire) {
		// TODO Auto-generated method stub
		Integer idCategorie = idTypePartenaire;
		List<Masseur> listeMasseur = null;
		listeMasseur = proxyPrestationDao.getListeMasseursByTypePartenaire(idCategorie);
		return listeMasseur;
	}

	@Override
	public List<Partenaire> getListePartenaire() {
		// TODO Auto-generated method stub
		List<Partenaire> part = null;
		part = proxyPrestationDao.getListePartenaire();
		return part;
	}

	@Override
	public List<Masseur> getListeMasseurs() {
		// TODO Auto-generated method stub
		List<Masseur> listeMasseur = null;
		listeMasseur = proxyPrestationDao.getListeMasseur();
		return listeMasseur;
	}


	@Override
	public List<TypePaiement> getListeTypePaiement() {
		// TODO Auto-generated method stub
		List<TypePaiement> typePaiement = null;
		typePaiement = proxyPrestationDao.getAllTypePaiement();
		return typePaiement;
	}

	@Override
	public List<Prestation> getListeHistoriquePrestations() {
		// TODO Auto-generated method stub
		List<Prestation> mesPresta = null;
		mesPresta = proxyPrestationDao.getAllHistoriquePrestation();
		return mesPresta;
	}

	@Override
	public List<InterventionReelle> getListInterventionReelle() {
		// TODO Auto-generated method stub
		List<InterventionReelle> mesInter = null;
		mesInter = proxyPrestationDao.getListeInterventionReelle();
		return mesInter;
	}

	@Override
	public List<InterventionReelle> getListInterventionReellePassees() {
		// TODO Auto-generated method stub
		List<InterventionReelle> mesInter = null;
		mesInter = proxyPrestationDao.getListeInterventionReellePassees();
		return mesInter;
	}
	@Override
	public List<InterventionReelle> getAllInfosInterventionReelleByIdMasseur(Integer idMasseur) {
		List<InterventionReelle> interventions = null;
		interventions = proxyInterventionReelleDao.getAllInterventionsByIdMasseur(idMasseur);
		return interventions;
	}

	@Override
	public List<InterventionReelle> getAllInfosInterventionReelleAVenirByIdMasseur(Integer idMasseur) {
		List<InterventionReelle> interventions = null;
		interventions = proxyInterventionReelleDao.getAllInterventionsByIdMasseur(idMasseur);
		return interventions;
	}

	@Override
	public InterventionReelle getInterventionById(Integer idIntervention) {
		// TODO Auto-generated method stub
		InterventionReelle interv = proxyInterventionReelleDao.getInterventionReelleById(idIntervention);
		return interv;
	}
	
	@Override
	public List<InterventionReelle> getListInterventionsNonPourvues() {
		return proxyInterventionReelleDao.getAllInterventionsNonPourvues();

	}

	@Override
	public List<InterventionReelle> getListeInterventionReelleDispoForMasseur(Integer idMasseur) {
		
		return proxyPrestationDao.getListeInterventionReelleDispoForMasseur(idMasseur);
	}

	@Override
	public List<InscriptionMasseurs> getAllInscriMasseurByIdIntervention(Integer idIntervention) {
		
		return proxyInterventionReelleDao.getAllInscriMasseurByIdIntervention(idIntervention);
	}

	@Override
	public List<InterventionReelle> getAllInfosInterventionReelleAVenir3JByIdMasseur(Integer idMasseur) {
		List<InterventionReelle> interventions = null;
		interventions = proxyInterventionReelleDao.getAllInterventions3JByIdMasseur(idMasseur);
		return interventions;
	}

	@Override
	public List<InterventionReelle> getAllInfosInterventionReellePasseesByIdMasseur(Integer idMasseur) {
		List<InterventionReelle> interventions = null;
		interventions = proxyInterventionReelleDao.getAllInterventionsPasseesByIdMasseur(idMasseur);
		return interventions;
	}
	
	@Override
	public Prestation validationPrestation(Prestation prestation) {
		return proxyPrestationDao.validerDemandePrestation(prestation);
	}


	@Override
	public List<InterventionReelle> getAllInterventionFoNext7DAys() {
		List<InterventionReelle> interventions = null;
		interventions = proxyInterventionReelleDao.getAllInterventionFoNext7DAys();
		return interventions;
	}

	@Override
	public List<InterventionReelle> getAllTodaySInterventions() {
		List<InterventionReelle> interventions = null;
		interventions = proxyInterventionReelleDao.getAllTodaySInterventions();
		return interventions;
	}

	@Override
	public List<InterventionReelle> getListInterventionsNonPourvues7Days() {
		List<InterventionReelle> interventions = null;
		interventions = proxyInterventionReelleDao.getAllInterventionsNonPourvuesNext7Days();
		return interventions;
	}

	@Override
	public Prestation getPrestationById(int prestationId) {
	
		return proxyPrestationDao.getPrestationById(prestationId);
	}

	@Override
	public List<Prestation> getListeDemandePrestation() {
		return proxyPrestationDao.getAllDemande();
	}

	@Override
	public List<InterventionReelle> getAllInterventionsByIdMasseurToday(Integer idMasseur) {
		List<InterventionReelle> interventions = null;
		interventions = proxyInterventionReelleDao.getAllInterventionsByIdMasseurToday(idMasseur);
		return interventions;
	}

	@Override
	public List<InterventionReelle> getAllInterventionFoNext3DAysByIdPart(Integer idPartenaire) {
		List<InterventionReelle> interv = null;
		interv = proxyInterventionReelleDao.getAllInterventionFoNext3DAysByIdPart(idPartenaire);
		return interv;
	}

	@Override
	public List<InterventionReelle> getAllInterventionFoTodayByIdPart(Integer idPartenaire) {
		List<InterventionReelle> interv = null;
		interv = proxyInterventionReelleDao.getAllInterventionFoTodayByIdPart(idPartenaire);
		return interv;
	}

}