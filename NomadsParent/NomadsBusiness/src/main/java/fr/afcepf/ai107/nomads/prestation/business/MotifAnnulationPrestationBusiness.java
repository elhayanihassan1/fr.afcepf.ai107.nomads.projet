package fr.afcepf.ai107.nomads.prestation.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.MotifAnnulationPrestation;
import fr.afcepf.ai107.nomads.idao.MotifAnnulationPrestationIDao;
import fr.afcepf.ai107.nomads.prestation.ibusiness.MotifAnnulationPrestationIBusiness;

@Remote(MotifAnnulationPrestationIBusiness.class)
@Stateless
public class MotifAnnulationPrestationBusiness implements MotifAnnulationPrestationIBusiness {

	@EJB
	MotifAnnulationPrestationIDao proxyMotifAnnulationPresta;
	
	@Override
	public List<String> getListeMotifsAnnulationPrestation() {
		// TODO Auto-generated method stub
		List<String> motifs = null;
		motifs = proxyMotifAnnulationPresta.getAllMotifAnnulation();
		return motifs;
	}

	@Override
	public InterventionReelle updateInterventionAvecMotif(InterventionReelle interv) {
		// TODO Auto-generated method stub
		return proxyMotifAnnulationPresta.updateInterventionAvecMotifAnnulation(interv);
	}

	@Override
	public MotifAnnulationPrestation getMotifByLibelleMotif(String motif) {
		// TODO Auto-generated method stub
		return proxyMotifAnnulationPresta.getMotifByLibelle(motif);
	}

}
