package fr.afcepf.ai107.nomads.outils.ibusiness;

import java.util.List;

import fr.afcepf.ai107.nomads.entities.ContratMasseur;
import fr.afcepf.ai107.nomads.entities.MotifFinContratMasseur;

public interface GestionContratsIBusiness {

	void CreateNewContractMasseur(ContratMasseur contratM);
	List<MotifFinContratMasseur> getAllMotifsFinCOntratMasseur();
	ContratMasseur getContratMasseurByIdMasseur (Integer idMasseur);
	MotifFinContratMasseur getMotifByLibelleMotif(String libelleMotif);
	void modifierContrat (ContratMasseur contrat);
}
