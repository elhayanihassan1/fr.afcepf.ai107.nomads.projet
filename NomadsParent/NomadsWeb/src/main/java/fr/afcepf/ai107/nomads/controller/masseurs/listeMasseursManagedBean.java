package fr.afcepf.ai107.nomads.controller.masseurs;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.script.ScriptContext;
import javax.swing.text.StyledEditorKit.ForegroundAction;

import org.omnifaces.util.Components.ForEach;

import fr.afcepf.ai107.nomads.entities.CategorieMasseur;
import fr.afcepf.ai107.nomads.entities.ContratMasseur;
import fr.afcepf.ai107.nomads.entities.EvolutionProfessionnelle;
import fr.afcepf.ai107.nomads.entities.InscriptionMasseurs;
import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.Masseur;
import fr.afcepf.ai107.nomads.entities.MotifFinContratMasseur;
import fr.afcepf.ai107.nomads.masseurs.ibusiness.InscriptionMasseursIBusiness;
import fr.afcepf.ai107.nomads.masseurs.ibusiness.MasseurIBusiness;
import fr.afcepf.ai107.nomads.outils.ibusiness.GestionContratsIBusiness;
import fr.afcepf.ai107.nomads.outils.ibusiness.OutilsIBusiness;

@ManagedBean (name = "mbListeMasseurs")
@ViewScoped
public class listeMasseursManagedBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@EJB
	private MasseurIBusiness proxyMasseurBusiness;
	
	@EJB
	private InscriptionMasseursIBusiness proxyInscriMasseurBusiness;
	
	@EJB
	private OutilsIBusiness proxyOutilsBusiness;
	
	@EJB
	private GestionContratsIBusiness proxyContratBusiness;
	
	private Masseur masseur;
	private Masseur selectedMasseur;
	private List<Masseur> listeMasseursCompleteActifs;
	private List<Masseur> listeMasseursRecents;
//	private List<Masseur> listeMasseursActifs;
//	private List<Masseur> listeMasseursInactifs;
	private List<String> actifInactif = new ArrayList<String>();
	private List<Masseur> listeMasseursCompleteInactifs;//#################
	private String statutChoisi;
	private String niveauChoisi;
	private String categorieChoisie;
	private List<EvolutionProfessionnelle> evolutionsPro;
	private List<CategorieMasseur> categoriesMass;
	private Date dateJour;
	private List<MotifFinContratMasseur> motifsFCM;
	private String motifSelectionne;
	private ContratMasseur contratM;
	private List<InscriptionMasseurs> inscriptions = new ArrayList<InscriptionMasseurs>();

	@PostConstruct
	public void init() {
		listeMasseursCompleteActifs = proxyMasseurBusiness.getAllInfosMasseursActifs();
		listeMasseursCompleteInactifs =proxyMasseurBusiness.getAllInfosMasseursInactifs();
		listeMasseursRecents = proxyMasseurBusiness.getRecentMasseurs();

//		listeMasseursCompleteActifs = proxyMasseurBusiness.getAllInfosMasseurs("actifs");
//		listeMasseursActifs = proxyMasseurBusiness.getListeMasseursActifs();
//		listeMasseursInactifs = proxyMasseurBusiness.getListeMasseursInactifs();
		
//		System.out.println("######################J'imprime la liste des actifs##############################");
//		for (Masseur masseur : listeMasseursActifs) {
//			System.out.println(masseur.getId_masseur());
//		}
		actifInactif.add("Actifs");
		actifInactif.add("Inactifs");
		evolutionsPro = proxyOutilsBusiness.getAll();
		evolutionsPro.add(0, new EvolutionProfessionnelle(-1,"Tous", null));
		categoriesMass = proxyOutilsBusiness.getAllCategories();
		categoriesMass.add(0, new CategorieMasseur(-1, "Tous", null));
		setMotifsFCM(proxyContratBusiness.getAllMotifsFinCOntratMasseur());													
		
//		listeMasseursCompleteInactifs = listeMasseursActifs;
//		System.out.println("////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////");
//		for (Masseur masseur : listeMasseursCompleteInactifs) {
//			System.out.println(masseur.getId_masseur());
//		}
	}

//	public void onActivChange() { 
//		if (statutChoisi.equals("Inactifs")) {
//			listeMasseursCompleteActifs = proxyMasseurBusiness.getAllInfosMasseursInactifs();
//		}else {
//			listeMasseursCompleteActifs = proxyMasseurBusiness.getAllInfosMasseursActifs();
//		}
//	}
	
	public void OnChange() {
		
		if (statutChoisi != null && statutChoisi !="") {
			if (statutChoisi.equals("Inactifs")) {
				listeMasseursCompleteActifs = proxyMasseurBusiness.getAllInfosMasseursInactifs();
			}else {
				listeMasseursCompleteActifs = proxyMasseurBusiness.getAllInfosMasseursActifs();
			}
		}
		if(niveauChoisi != null && niveauChoisi !="") {
			switch (niveauChoisi) { //Switch avec deux possibilités ==> gérer le choix de chacune des DDL?
			case "Neo-nomad":
				listeMasseursCompleteActifs = proxyMasseurBusiness.getAllInfoForMasseursActifsByLvl("Neo-nomad");
				break;
			case "Nomad confirmé":
				listeMasseursCompleteActifs = proxyMasseurBusiness.getAllInfoForMasseursActifsByLvl("Nomad confirmé");
				break;
			case "Nomad formateur":
				listeMasseursCompleteActifs = proxyMasseurBusiness.getAllInfoForMasseursActifsByLvl("Nomad formateur");
				break;
			case "Tous":
				listeMasseursCompleteActifs = proxyMasseurBusiness.getAllInfosMasseursActifs();
				break;
			default:
				break;
			}
		}
		if(categorieChoisie != null && categorieChoisie !="") {
			switch (categorieChoisie) { //Switch avec deux possibilités ==> gérer le choix de chacune des DDL?
			case "Bar-Restaurants":
				listeMasseursCompleteActifs = proxyMasseurBusiness.getAllInfosForMasseursActifsByCatego("Bar-Restaurants");
				break;
			case "Événements":
				listeMasseursCompleteActifs = proxyMasseurBusiness.getAllInfosForMasseursActifsByCatego("Événements");
				break;
			case "Entreprises":
				listeMasseursCompleteActifs = proxyMasseurBusiness.getAllInfosForMasseursActifsByCatego("Entreprises");
				break;
			case "Tous":
				listeMasseursCompleteActifs = proxyMasseurBusiness.getAllInfosMasseursActifs();
				break;
			default:
				break;
			}
		
		}
	}
	
	public String goToFicheMasseur() {
		return "/FicheMasseurAdm.xhtml?faces-redirect=true&id="+selectedMasseur.getId_masseur();
	}
	
	public String lienFicheMasseur() {
		return "FicheMasseurAdm.xhtml?faces-redirect=true&id=" + masseur.getId_masseur();
	}
	
	public String lienModifierMasseur() {
		return "MiseAJourMasseur.xhtml?faces-redirect=true&id=" + masseur.getId_masseur();
	}
	
	
	
	public void desactiverMasseur() {
//		System.out.println("Je désactive le masseur phase 1 pour " );
//		System.out.println("#####################" + masseur.getNomMasseur() + "########################################################################################################################################");
		contratM = proxyContratBusiness.getContratMasseurByIdMasseur(masseur.getId_masseur());
		System.out.println("Le contrat est celui d'ID : " + contratM.getIdContratMasseur() + " POUR LE CONTRAT DU " + contratM.getDateDebutContratMasseur() + "===============================================================================================================");
		contratM.setDateFinContratMasseur(dateJour);
		System.out.println("Le contrat a la date de fin au  : " + contratM.getDateFinContratMasseur() + "===============================================================================================================");
//		proxyMasseurBusiness.ModifierMasseur(masseur);
		if (motifSelectionne != null) {
			contratM.setMotifFinContratMasseur(proxyContratBusiness.getMotifByLibelleMotif(motifSelectionne));
		
		}
		proxyContratBusiness.modifierContrat(contratM);
		masseur.setDateDepartDefinitifMasseur(dateJour);
		proxyMasseurBusiness.ModifierMasseur(masseur);
		
		if (inscriptions != null) {
			for (InscriptionMasseurs inscri : inscriptions) {
				inscri.setDateDesinscriptionMasseur(new Date());
				proxyInscriMasseurBusiness.mettreAJourInscriptionMasseur(inscri);
			}
			
			listeMasseursCompleteActifs = new ArrayList<Masseur>(); 
			listeMasseursCompleteActifs = proxyMasseurBusiness.getAllInfosMasseursActifs();
//			listeMasseursCompleteActifs = new ArrayList<Masseur>(c)
		}
//		return "MesMasseurs.xhtml?faces-redirect=true";
	}
	
//	public void Test() {
//		System.out.println("TESTTESTTESTTESTTESTTEST---------"+ masseur.getNomMasseur()+"___________________TESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTEST");
//	}
//	public void Test2() {
//		if(motifSelectionne!=null && !motifSelectionne.equals("")) {
//		System.out.println("TESTTESTTESTTESTTESTTEST---------"+ masseur.getNomMasseur()+"___________________TESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTEST");
//		}
//	}
	
		public List<InscriptionMasseurs> recupererInterv() {
			inscriptions = proxyInscriMasseurBusiness.getInscriptionMasseurFuturesByIdMasseur(masseur.getId_masseur(), new Date());
			System.out.println("LA LISTE D4INSCRIPTIONS A VENIR EST DE TAILLE " + inscriptions.size() + "''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''");
			return inscriptions;
		}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//	public void onLvlChange() { 
//		System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&" + niveauChoisi + "//////////////////////////////////////////////");
////		if (niveauChoisi.equals("Neo-nomad")) {
////			listeMasseursCompleteActifs = proxyMasseurBusiness.getAllInfoForMasseursActifsByLvl("Neo-nomad");
////		}else {
////			listeMasseursCompleteActifs = proxyMasseurBusiness.getAllInfosMasseursInactifs();
////		}
//		switch (niveauChoisi) { //Switch avec deux possibilités ==> gérer le choix de chacune des DDL?
//		case "Neo-nomad":
//			listeMasseursCompleteActifs = proxyMasseurBusiness.getAllInfoForMasseursActifsByLvl("Neo-nomad");
//			break;
//		case "Nomad confirmé":
//			listeMasseursCompleteActifs = proxyMasseurBusiness.getAllInfoForMasseursActifsByLvl("Nomad confirmé");
//			break;
//		case "Nomad formateur":
//			listeMasseursCompleteActifs = proxyMasseurBusiness.getAllInfoForMasseursActifsByLvl("Nomad formateur");
//			break;
//		case "Tous":
//			listeMasseursCompleteActifs = proxyMasseurBusiness.getAllInfosMasseursActifs();
//			break;
//		default:
//			break;
//		}
//	}   
//	
//	public void onCategoChange() { 
//
//		switch (categorieChoisie) { //Switch avec deux possibilités ==> gérer le choix de chacune des DDL?
//		case "Bar-Restaurants":
//			listeMasseursCompleteActifs = proxyMasseurBusiness.getAllInfosForMasseursActifsByCatego("Bar-Restaurants");
//			break;
//		case "Événements":
//			listeMasseursCompleteActifs = proxyMasseurBusiness.getAllInfosForMasseursActifsByCatego("Événements");
//			break;
//		case "Entreprises":
//			listeMasseursCompleteActifs = proxyMasseurBusiness.getAllInfosForMasseursActifsByCatego("Entreprises");
//			break;
//		case "Tous":
//			listeMasseursCompleteActifs = proxyMasseurBusiness.getAllInfosMasseursActifs();
//			break;
//		default:
//			break;
//		}
////		for (Masseur mass : listeMasseursCompleteActifs) {
////			System.out.println(mass.getNomMasseur() + " " + mass.getAffectactionsCategorieMasseur());
////		}
//	}   
	
	
	public Masseur getMasseur() {
		return masseur;
	}

	public void setMasseur(Masseur masseur) {
		this.masseur = masseur;
	}


	public List<Masseur> getListeMasseursCompleteActifs() {
		return listeMasseursCompleteActifs;
	}


	public void setListeMasseursCompleteActifs(List<Masseur> listeMasseursCompleteActifs) {
		this.listeMasseursCompleteActifs = listeMasseursCompleteActifs;
	}



//	/**
//	 * @return the listeMasseursActifs
//	 */
//	public List<Masseur> getListeMasseursActifs() {
//		return listeMasseursActifs;
//	}
//
//
//
//	/**
//	 * @param listeMasseursActifs the listeMasseursActifs to set
//	 */
//	public void setListeMasseursActifs(List<Masseur> listeMasseursActifs) {
//		this.listeMasseursActifs = listeMasseursActifs;
//	}
//
//
//
//	/**
//	 * @return the listeMasseursInactifs
//	 */
//	public List<Masseur> getListeMasseursInactifs() {
//		return listeMasseursInactifs;
//	}
//
//
//
//	/**
//	 * @param listeMasseursInactifs the listeMasseursInactifs to set
//	 */
//	public void setListeMasseursInactifs(List<Masseur> listeMasseursInactifs) {
//		this.listeMasseursInactifs = listeMasseursInactifs;
//	}



	/**
	 * @return the actifInactif
	 */
	public List<String> getActifInactif() {
		return actifInactif;
	}



	/**
	 * @param actifInactif the actifInactif to set
	 */
	public void setActifInactif(List<String> actifInactif) {
		this.actifInactif = actifInactif;
	}

	/**
	 * @return the masseursAAfficher
	 */
	public List<Masseur> getMasseursAAfficher() {
		return listeMasseursCompleteInactifs;
	}

	/**
	 * @param masseursAAfficher the masseursAAfficher to set
	 */
	public void setMasseursAAfficher(List<Masseur> masseursAAfficher) {
		this.listeMasseursCompleteInactifs = masseursAAfficher;
	}

	/**
	 * @return the statutChoisi
	 */
	public String getStatutChoisi() {
		return statutChoisi;
	}

	/**
	 * @param statutChoisi the statutChoisi to set
	 */
	public void setStatutChoisi(String statutChoisi) {
		this.statutChoisi = statutChoisi;
	}

	/**
	 * @return the listeMasseursCompleteInactifs
	 */
	public List<Masseur> getListeMasseursCompleteInactifs() {
		return listeMasseursCompleteInactifs;
	}

	/**
	 * @param listeMasseursCompleteInactifs the listeMasseursCompleteInactifs to set
	 */
	public void setListeMasseursCompleteInactifs(List<Masseur> listeMasseursCompleteInactifs) {
		this.listeMasseursCompleteInactifs = listeMasseursCompleteInactifs;
	}

	/**
	 * @return the niveauChoisi
	 */
	public String getNiveauChoisi() {
		return niveauChoisi;
	}

	/**
	 * @param niveauChoisi the niveauChoisi to set
	 */
	public void setNiveauChoisi(String niveauChoisi) {
		this.niveauChoisi = niveauChoisi;
	}

	/**
	 * @return the evolutionsPro
	 */
	public List<EvolutionProfessionnelle> getEvolutionsPro() {
		return evolutionsPro;
	}

	/**
	 * @param evolutionsPro the evolutionsPro to set
	 */
	public void setEvolutionsPro(List<EvolutionProfessionnelle> evolutionsPro) {
		this.evolutionsPro = evolutionsPro;
	}

	/**
	 * @return the categorieChoisie
	 */
	public String getCategorieChoisie() {
		return categorieChoisie;
	}

	/**
	 * @param categorieChoisie the categorieChoisie to set
	 */
	public void setCategorieChoisie(String categorieChoisie) {
		this.categorieChoisie = categorieChoisie;
	}

	/**
	 * @return the categoriesMass
	 */
	public List<CategorieMasseur> getCategoriesMass() {
		return categoriesMass;
	}

	/**
	 * @param categoriesMass the categoriesMass to set
	 */
	public void setCategoriesMass(List<CategorieMasseur> categoriesMass) {
		this.categoriesMass = categoriesMass;
	}


	public Date getDateJour() {
		return dateJour;
	}

	public void setDateJour(Date dateJour) {
		this.dateJour = dateJour;
	}

	public List<MotifFinContratMasseur> getMotifsFCM() {
		return motifsFCM;
	}

	public void setMotifsFCM(List<MotifFinContratMasseur> motifsFCM) {
		this.motifsFCM = motifsFCM;
	}

	public String getMotifSelectionne() {
		return motifSelectionne;
	}

	public void setMotifSelectionne(String motifSelectionne) {
		this.motifSelectionne = motifSelectionne;
	}

	public Masseur getSelectedMasseur() {
		return selectedMasseur;
	}

	public void setSelectedMasseur(Masseur selectedMasseur) {
		this.selectedMasseur = selectedMasseur;
	}

	/**
	 * @return the contratM
	 */
	public ContratMasseur getContratM() {
		return contratM;
	}

	/**
	 * @param contratM the contratM to set
	 */
	public void setContratM(ContratMasseur contratM) {
		this.contratM = contratM;
	}

	

	/**
	 * @return the inscriptions
	 */
	public List<InscriptionMasseurs> getInscriptions() {
		return inscriptions;
	}

	/**
	 * @param inscriptions the inscriptions to set
	 */
	public void setInscriptions(List<InscriptionMasseurs> inscriptions) {
		this.inscriptions = inscriptions;
	}

	public List<Masseur> getListeMasseursRecents() {
		return listeMasseursRecents;
	}

	public void setListeMasseursRecents(List<Masseur> listeMasseursRecents) {
		this.listeMasseursRecents = listeMasseursRecents;
	}

	

	
}
