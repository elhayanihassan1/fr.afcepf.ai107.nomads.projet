package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import javax.persistence.Table;

@Entity
@Table(name = "contrat_masseur")
public class ContratMasseur implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_contrat_masseur")
	private Integer idContratMasseur;

	@Column(name = "date_debut_contrat_masseur")
	private Date dateDebutContratMasseur;

	@Column(name = "date_fin_contrat_masseur")
	private Date dateFinContratMasseur;
	
	@ManyToOne
	@JoinColumn (name  = "id_masseur", referencedColumnName = "id_masseur")
	private Masseur masseur;
	
	@ManyToOne
	@JoinColumn (name = "id_motif_fin_contrat_masseur", referencedColumnName = "id_motif_fin_contrat_masseur")
	private MotifFinContratMasseur motifFinContratMasseur;

	/**
	 * 
	 */
	public ContratMasseur() {
		super();
	}

	/**
	 * @param idContratMasseur
	 * @param dateDebutContratMasseur
	 * @param dateFinContratMasseur
	 * @param masseur
	 * @param motifFinContratMasseur
	 */
	public ContratMasseur(Integer idContratMasseur, Date dateDebutContratMasseur, Date dateFinContratMasseur,
			Masseur masseur, MotifFinContratMasseur motifFinContratMasseur) {
		super();
		this.idContratMasseur = idContratMasseur;
		this.dateDebutContratMasseur = dateDebutContratMasseur;
		this.dateFinContratMasseur = dateFinContratMasseur;
		this.masseur = masseur;
		this.motifFinContratMasseur = motifFinContratMasseur;
	}

	/**
	 * @return the idContratMasseur
	 */
	public int getIdContratMasseur() {
		return idContratMasseur;
	}

	/**
	 * @param idContratMasseur the idContratMasseur to set
	 */
	public void setIdContratMasseur(Integer idContratMasseur) {
		this.idContratMasseur = idContratMasseur;
	}

	/**
	 * @return the dateDebutContratMasseur
	 */
	public Date getDateDebutContratMasseur() {
		return dateDebutContratMasseur;
	}

	/**
	 * @param dateDebutContratMasseur the dateDebutContratMasseur to set
	 */
	public void setDateDebutContratMasseur(Date dateDebutContratMasseur) {
		this.dateDebutContratMasseur = dateDebutContratMasseur;
	}

	/**
	 * @return the dateFinContratMasseur
	 */
	public Date getDateFinContratMasseur() {
		return dateFinContratMasseur;
	}

	/**
	 * @param dateFinContratMasseur the dateFinContratMasseur to set
	 */
	public void setDateFinContratMasseur(Date dateFinContratMasseur) {
		this.dateFinContratMasseur = dateFinContratMasseur;
	}

	/**
	 * @return the masseur
	 */
	public Masseur getMasseur() {
		return masseur;
	}

	/**
	 * @param masseur the masseur to set
	 */
	public void setMasseur(Masseur masseur) {
		this.masseur = masseur;
	}

	/**
	 * @return the motifFinContratMasseur
	 */
	public MotifFinContratMasseur getMotifFinContratMasseur() {
		return motifFinContratMasseur;
	}

	/**
	 * @param motifFinContratMasseur the motifFinContratMasseur to set
	 */
	public void setMotifFinContratMasseur(MotifFinContratMasseur motifFinContratMasseur) {
		this.motifFinContratMasseur = motifFinContratMasseur;
	}

}