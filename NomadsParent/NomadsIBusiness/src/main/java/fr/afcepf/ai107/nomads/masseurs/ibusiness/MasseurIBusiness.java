package fr.afcepf.ai107.nomads.masseurs.ibusiness;

import java.util.List;


import fr.afcepf.ai107.nomads.entities.CategorieMasseur;
import fr.afcepf.ai107.nomads.entities.IndisponibiliteMasseur;
import fr.afcepf.ai107.nomads.entities.Masseur;
import fr.afcepf.ai107.nomads.entities.MotifIndisponibiliteMasseur;
import fr.afcepf.ai107.nomads.entities.Sexe;

public interface MasseurIBusiness {

	Masseur connection(String login, String password);
	List<Sexe> getSexes();
	void ajouterMasseur (Masseur masseur);
	boolean exists (Masseur masseur);
	Sexe getSexeById (int id);
	List<Masseur> getAllMasseurs();
	List<String> getCategorieMasseurByIdM (int idMasseur);
	List<Masseur> getAllInfosMasseursActifs();
	List<Masseur> getAllInfosMasseursInactifs();
	List<Masseur> getListeMasseursActifs();
	List<Masseur> getListeMasseursInactifs();
	List<Masseur> getAllInfoForMasseursActifsByLvl(String niveau);
	List<Masseur> getAllInfoForMasseursInactifsByLvl(String niveau); 
	List<Masseur> getAllInfosForMasseursActifsByCatego(String categorie);
	Masseur getInfosMasseurById (Integer idMasseur);
	boolean identifiantsOk(String login, String password);
	void ModifierMasseur (Masseur masseur);
	List<Masseur> getRecentMasseurs();

	List<Masseur>getMasseurByIdCategForCreationPresta(Integer idTypeDePartenaire);
	

	List<Masseur> getRecentMasseurs1month();
	List<Masseur> getRecentDepartMasseurs1month();
	List<IndisponibiliteMasseur> getIndispoMasseurFutByIdMasseur(Integer idMasseur);
	List<MotifIndisponibiliteMasseur> getAllMotifsIndispoMass();
	List<IndisponibiliteMasseur> getIndispoMasseurPasseesByIdMasseur(Integer idMasseur);
	void AjouterIndisponibilitéMasseur(IndisponibiliteMasseur NvelleIndisp);
	boolean EstNeonomad(Integer idMasseur);
	
	
	
	
	
//	List<Masseur>getAllInfosMasseurs(String actif_inactif);
}

