
package fr.afcepf.ai107.nomads.idao;

import java.util.List;

import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.Masseur;
import fr.afcepf.ai107.nomads.entities.Partenaire;
import fr.afcepf.ai107.nomads.entities.Prestation;
import fr.afcepf.ai107.nomads.entities.TypePaiement;
import fr.afcepf.ai107.nomads.entities.TypePartenaire;

public interface PrestationIDao extends GenericIDao<Prestation> {

	List<Prestation> getAll();
	
	Prestation addPrestation(Prestation prestation);
	
	InterventionReelle addIntervention(InterventionReelle interventionReelle);
	
	//Utiliser dans une drop down list pour la création de la prestation
	List<Partenaire> getPartenaireByTypePartenaire(Integer idTypePartenaire);
	
	//Utiliser dans une drop down list pour la création de la prestation
	List<TypePartenaire> getTypePartenaire();
	
	List<Partenaire> getListePartenaire();
	
	List<Masseur> getListeMasseur();
	
	List<Masseur> getListeMasseursByTypePartenaire(Integer idCategorie);
	
	List<TypePaiement> getAllTypePaiement();
	
	List<Prestation> getAllHistoriquePrestation();
	
	List<InterventionReelle> getListeInterventionReelle();
	
	List<InterventionReelle> getListeInterventionReellePassees();
	
	List<InterventionReelle> getListeInterventionReelleDispoForMasseur(Integer idMasseur);
	
	Prestation validerDemandePrestation(Prestation prestation);
	Prestation getPrestationById(int prestationId);
	List<Prestation> getAllDemande();
}
