package fr.afcepf.ai107.nomads.controller.mads;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import fr.afcepf.ai107.nomads.entities.Masseur;
import fr.afcepf.ai107.nomads.entities.TypePackMads;
import fr.afcepf.ai107.nomads.masseurs.ibusiness.MasseurIBusiness;
import fr.afcepf.ai107.nomads.outils.ibusiness.TypePackMadsIBusiness;

@ManagedBean(name = "mbDetailCommande")
@ViewScoped
public class DetailCommandeManagedBean {
	
	@EJB
	private MasseurIBusiness proxyMasseurBusiness;
	
	@EJB
	private TypePackMadsIBusiness proxyTypePackMadsBusiness;
	
	@ManagedProperty(value = "#{mbAccount.masseur}")
	private Masseur masseurConnected;
	
	
	private TypePackMads packMads;
		
	@PostConstruct
	public void init() {
		int idPack = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("idPack"));
			packMads=proxyTypePackMadsBusiness.getByIdPack(idPack);
	}
//	public String detailCommande() {
//		return "/DetailCommande.xhtml?faces-redirect=true&idPack="+packMads.getIdTypePack();
//	}

	public Masseur getMasseurConnected() {
		return masseurConnected;
	}

	public void setMasseurConnected(Masseur masseurConnected) {
		this.masseurConnected = masseurConnected;
	}

	public TypePackMads getPackMads() {
		return packMads;
	}

	public void setPackMads(TypePackMads packMads) {
		this.packMads = packMads;
	}
	
}
