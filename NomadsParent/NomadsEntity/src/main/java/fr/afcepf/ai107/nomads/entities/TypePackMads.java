package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import javax.persistence.Table;

@Entity
@Table(name = "type_pack_mads")
public class TypePackMads implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_type_pack_mads")
	private int idTypePack;

	@Column(name = "libelle_type_pack_mads")
	private String libelleTypePackMads;
	
	@Column(name = "valeur_type_pack_mads")
	private Integer valeurTypePackMads;

	@Column(name = "prix_type_pack_mads")
	private Double prixTypePackMads;
	
	@Column(name = "removed_type_pack_mads", columnDefinition = "BOOLEAN default false")
	private Boolean removedTypePackMads;
	
	@Column(name ="")
	@OneToMany (mappedBy = "typePackMads", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private Set<Transaction> transactions;

	/**
	 * 
	 */
	public TypePackMads() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param idTypePack
	 * @param libelleTypePackMads
	 * @param valeurTypePackMads
	 * @param prixTypePackMads
	 * @param removedTypePackMads
	 * @param transactions
	 */
	public TypePackMads(int idTypePack, String libelleTypePackMads, Integer valeurTypePackMads, Double prixTypePackMads,
			Boolean removedTypePackMads, Set<Transaction> transactions) {
		super();
		this.idTypePack = idTypePack;
		this.libelleTypePackMads = libelleTypePackMads;
		this.valeurTypePackMads = valeurTypePackMads;
		this.prixTypePackMads = prixTypePackMads;
		this.removedTypePackMads = removedTypePackMads;
		this.transactions = transactions;
	}

	/**
	 * @return the idTypePack
	 */
	public int getIdTypePack() {
		return idTypePack;
	}

	/**
	 * @param idTypePack the idTypePack to set
	 */
	public void setIdTypePack(int idTypePack) {
		this.idTypePack = idTypePack;
	}

	/**
	 * @return the libelleTypePackMads
	 */
	public String getLibelleTypePackMads() {
		return libelleTypePackMads;
	}

	/**
	 * @param libelleTypePackMads the libelleTypePackMads to set
	 */
	public void setLibelleTypePackMads(String libelleTypePackMads) {
		this.libelleTypePackMads = libelleTypePackMads;
	}

	/**
	 * @return the valeurTypePackMads
	 */
	public Integer getValeurTypePackMads() {
		return valeurTypePackMads;
	}

	/**
	 * @param valeurTypePackMads the valeurTypePackMads to set
	 */
	public void setValeurTypePackMads(Integer valeurTypePackMads) {
		this.valeurTypePackMads = valeurTypePackMads;
	}

	/**
	 * @return the prixTypePackMads
	 */
	public Double getPrixTypePackMads() {
		return prixTypePackMads;
	}

	/**
	 * @param prixTypePackMads the prixTypePackMads to set
	 */
	public void setPrixTypePackMads(Double prixTypePackMads) {
		this.prixTypePackMads = prixTypePackMads;
	}

	/**
	 * @return the removedTypePackMads
	 */
	public Boolean getRemovedTypePackMads() {
		return removedTypePackMads;
	}

	/**
	 * @param removedTypePackMads the removedTypePackMads to set
	 */
	public void setRemovedTypePackMads(Boolean removedTypePackMads) {
		this.removedTypePackMads = removedTypePackMads;
	}

	/**
	 * @return the transactions
	 */
	public Set<Transaction> getTransactions() {
		return transactions;
	}

	/**
	 * @param transactions the transactions to set
	 */
	public void setTransactions(Set<Transaction> transactions) {
		this.transactions = transactions;
	}

}