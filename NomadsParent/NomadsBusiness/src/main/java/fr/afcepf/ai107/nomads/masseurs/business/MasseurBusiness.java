package fr.afcepf.ai107.nomads.masseurs.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai107.nomads.entities.CategorieMasseur;
import fr.afcepf.ai107.nomads.entities.HistoriqueEvolutionProfessionnelleMasseur;
import fr.afcepf.ai107.nomads.entities.IndisponibiliteMasseur;
import fr.afcepf.ai107.nomads.entities.Masseur;
import fr.afcepf.ai107.nomads.entities.MotifIndisponibiliteMasseur;
import fr.afcepf.ai107.nomads.entities.Sexe;
import fr.afcepf.ai107.nomads.idao.CategorieMasseurIDao;
import fr.afcepf.ai107.nomads.idao.HistoriqueEvoProMasseurIDao;
import fr.afcepf.ai107.nomads.idao.IndisponibiliteMasseurIDao;
import fr.afcepf.ai107.nomads.idao.MasseurIDao;
import fr.afcepf.ai107.nomads.idao.MotifIndispoMassIDao;
import fr.afcepf.ai107.nomads.idao.SexeIDao;
import fr.afcepf.ai107.nomads.masseurs.ibusiness.MasseurIBusiness;


@Remote(MasseurIBusiness.class)
@Stateless
public class MasseurBusiness implements MasseurIBusiness {

	@EJB
	private MasseurIDao proxyMasseurDAO;
	@EJB
	private SexeIDao proxySexeDAO;
	@EJB
	private CategorieMasseurIDao proxyCategorieMasseurDAO;
	@EJB
	private IndisponibiliteMasseurIDao proxyIndisponibiliteMasseurs;
	
	@EJB
	private MotifIndispoMassIDao proxyMotifIndispoMass;
	
	@EJB
	private HistoriqueEvoProMasseurIDao proxyHistoEvoProBusiness;

	@Override
	public Masseur connection(String login, String password) {

		return proxyMasseurDAO.authenticate(login, password);
	}

	@Override
	public List<Sexe> getSexes() {
		List<Sexe> sexes = null;
		sexes = proxySexeDAO.getAll();
		return sexes;
	}

	@Override
	public void ajouterMasseur(Masseur masseur) {
		proxyMasseurDAO.add(masseur);
		
	}

	@Override
	public boolean exists(Masseur masseur) {
		boolean existe = proxyMasseurDAO.exists(masseur);
		return existe;
	}

	/**
	*A partir d'un ID sexe, récupérer l'objet sexe associé
	 */
	@Override
	public Sexe getSexeById(int id) {
		Sexe sexe = proxySexeDAO.getById(id);
		return sexe;
	}

	/**
	 * Récupération de tous les masseurs en base
	 */
	@Override
	public List<Masseur> getAllMasseurs() {
		List<Masseur> masseurs = proxyMasseurDAO.findAll();
		return masseurs;
	}

	@Override
	public List<String> getCategorieMasseurByIdM(int idMasseur) {
		List<String> cat;
		cat = proxyCategorieMasseurDAO.categoriesByIdMasseur(idMasseur);
		return cat;
	}

	/**
	 *Methode renvoyant la liste de tous les masseurs actifs au moment de la requete, en remontant également la liste de leur évolution professionnelle et de leur catégorie 
	 */
	@Override
	public List<Masseur> getAllInfosMasseursActifs() {
		List<Masseur> masseurs = proxyMasseurDAO.getAllInfoForMasseursActifs();
		return masseurs;
	}

	@Override
	public List<Masseur> getListeMasseursActifs() {
		List<Masseur> masseurs = proxyIndisponibiliteMasseurs.getListeMasseursActifs();
		return masseurs;
	}
	
	@Override
	public List<Masseur> getRecentMasseurs() {
		List<Masseur> recentMasseurs = proxyMasseurDAO.getRecentMasseurs();
		return recentMasseurs;
	}

	@Override
	public List<Masseur> getListeMasseursInactifs() {
		List<Masseur> masseursInactifs = proxyIndisponibiliteMasseurs.getListeMasseursIndispo();
		return masseursInactifs;
	}

	@Override
	public List<Masseur> getAllInfosMasseursInactifs() {
		List<Masseur> masseursInactifsTtesInfos = proxyMasseurDAO.getAllInfoForMasseursInactifs();
		return masseursInactifsTtesInfos;
	}

	@Override
	public List<Masseur> getAllInfoForMasseursActifsByLvl(String niveau) {
		List<Masseur> masseursActifParCat = proxyMasseurDAO.getAllInfoForMasseursActifsByLvl(niveau);
		return masseursActifParCat;
	}

	@Override
	public List<Masseur> getAllInfoForMasseursInactifsByLvl(String niveau) {
		List<Masseur> masseursInactifParCat = proxyMasseurDAO.getAllInfoForMasseursInactifsByLvl(niveau);
		return masseursInactifParCat;
	}

	@Override
	public List<Masseur> getAllInfosForMasseursActifsByCatego(String categorie) {
		List<Masseur> masseursCatego = proxyMasseurDAO.getAllInfosForMasseursActifsByCatego(categorie);
		return masseursCatego;
	}

	@Override
	public Masseur getInfosMasseurById(Integer idMasseur) {
		Masseur masseur = proxyMasseurDAO.getInfosbyId(idMasseur);
		return masseur;
	}

	@Override
	public boolean identifiantsOk(String login, String password) {
		boolean areOk = false;
		Masseur masseur = proxyMasseurDAO.authenticate(login, password);
		if(masseur != null) {
			areOk = true;
		}
		return areOk;
	}

	@Override
	public void ModifierMasseur(Masseur masseur) {
		proxyMasseurDAO.update(masseur);
		
	}

	@Override
	public List<Masseur> getMasseurByIdCategForCreationPresta(Integer idTypeDePartenaire) {
		// TODO Auto-generated method stub
		Integer idCategorie = idTypeDePartenaire;
		List<Masseur> listeMasseur = null;
		listeMasseur = proxyMasseurDAO.getMasseurForCreationPresta(idCategorie);
		return listeMasseur;
	}

	public List<Masseur> getRecentMasseurs1month() {
		List<Masseur> recentMasseurs = proxyMasseurDAO.getRecentMasseurs1month();
		return recentMasseurs;
	}

	@Override
	public List<Masseur> getRecentDepartMasseurs1month() {
		List<Masseur> recentDepartsMasseurs = proxyMasseurDAO.getRecentDepartMasseurs1month();
		return recentDepartsMasseurs;
	}

	@Override
	public List<IndisponibiliteMasseur> getIndispoMasseurFutByIdMasseur(Integer idMasseur) {
		List<IndisponibiliteMasseur> indispos = null;
		indispos = proxyIndisponibiliteMasseurs.getIndispoMasseurFutByIdMasseur(idMasseur);
		return indispos;
	}

	@Override
	public List<MotifIndisponibiliteMasseur> getAllMotifsIndispoMass() {
		List<MotifIndisponibiliteMasseur> motifs = null;
		motifs=proxyMotifIndispoMass.findAll();
		return motifs;
	}

	@Override
	public List<IndisponibiliteMasseur> getIndispoMasseurPasseesByIdMasseur(Integer idMasseur) {
		List<IndisponibiliteMasseur> indispos = null;
		indispos = proxyIndisponibiliteMasseurs.getIndispoMasseurPasseesByIdMasseur(idMasseur);
		return indispos;
	}

	@Override
	public void AjouterIndisponibilitéMasseur(IndisponibiliteMasseur NvelleIndisp) {
		proxyIndisponibiliteMasseurs.add(NvelleIndisp);

	}

	@Override
	public boolean EstNeonomad(Integer idMasseur) {
		boolean estNeo = false;
		estNeo = proxyHistoEvoProBusiness.EstNeonomad(idMasseur);
		return estNeo;
	}

	
	
//	@Override
//	public List<Masseur> getAllInfosMasseurs(String actif_inactif) {
//		List<Masseur> masseurTtesInfos = proxyMasseurDAO.getAllInfosForMasseurs(actif_inactif);
//		return masseurTtesInfos;
//	}
//	
	
	

//	@Override
//	public List<Masseur> getMasseursBySexe(Sexe sexe) {
//		return null;
//	}
//
//	@Override
//	public List<Masseur> findAll() {
//		return null;
//	}
	


}
