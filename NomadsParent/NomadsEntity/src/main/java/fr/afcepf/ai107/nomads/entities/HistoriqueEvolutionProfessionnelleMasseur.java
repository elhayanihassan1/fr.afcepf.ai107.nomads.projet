package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "historique_evolution_professionnelle_masseur")
public class HistoriqueEvolutionProfessionnelleMasseur implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_historique_evolution_professionnelle_masseur")
	private Integer idHistoriqueEvolutionProfessionnelleMasseur;

	@Column(name = "date_debut_evolution_professionnelle_masseur")
	private Date dateDebutEvolutionProfessionnelleMasseur;

	@Column(name = "date_fin_evolution_professionnelle_masseur")
	private Date dateFinEvolutionProfessionnelleMasseur;
	
	@ManyToOne
	@JoinColumn (name= "id_masseur", referencedColumnName = "id_masseur")
	private Masseur masseur;
	
	@ManyToOne
	@JoinColumn (name = "id_evolution_professionnelle", referencedColumnName = "id_evolution_professionnelle" )
	private EvolutionProfessionnelle evolutionsProfessionnelle;

	public HistoriqueEvolutionProfessionnelleMasseur() {
		super();
		
	}

	public HistoriqueEvolutionProfessionnelleMasseur(Integer idHistoriqueEvolutionProfessionnelleMasseur,
			Date dateDebutEvolutionProfessionnelleMasseur, Date dateFinEvolutionProfessionnelleMasseur, Masseur masseur,
			EvolutionProfessionnelle evolutionsProfessionnelle) {
		super();
		this.idHistoriqueEvolutionProfessionnelleMasseur = idHistoriqueEvolutionProfessionnelleMasseur;
		this.dateDebutEvolutionProfessionnelleMasseur = dateDebutEvolutionProfessionnelleMasseur;
		this.dateFinEvolutionProfessionnelleMasseur = dateFinEvolutionProfessionnelleMasseur;
		this.masseur = masseur;
		this.evolutionsProfessionnelle = evolutionsProfessionnelle;
	}
	
	public HistoriqueEvolutionProfessionnelleMasseur(
			Date dateDebutEvolutionProfessionnelleMasseur, Date dateFinEvolutionProfessionnelleMasseur, Masseur masseur,
			EvolutionProfessionnelle evolutionsProfessionnelle) {
		super();
		this.dateDebutEvolutionProfessionnelleMasseur = dateDebutEvolutionProfessionnelleMasseur;
		this.dateFinEvolutionProfessionnelleMasseur = dateFinEvolutionProfessionnelleMasseur;
		this.masseur = masseur;
		this.evolutionsProfessionnelle = evolutionsProfessionnelle;
	}

	public Integer getIdHistoriqueEvolutionProfessionnelleMasseur() {
		return idHistoriqueEvolutionProfessionnelleMasseur;
	}

	public void setIdHistoriqueEvolutionProfessionnelleMasseur(Integer idHistoriqueEvolutionProfessionnelleMasseur) {
		this.idHistoriqueEvolutionProfessionnelleMasseur = idHistoriqueEvolutionProfessionnelleMasseur;
	}

	public Date getDateDebutEvolutionProfessionnelleMasseur() {
		return dateDebutEvolutionProfessionnelleMasseur;
	}

	public void setDateDebutEvolutionProfessionnelleMasseur(Date dateDebutEvolutionProfessionnelleMasseur) {
		this.dateDebutEvolutionProfessionnelleMasseur = dateDebutEvolutionProfessionnelleMasseur;
	}

	public Date getDateFinEvolutionProfessionnelleMasseur() {
		return dateFinEvolutionProfessionnelleMasseur;
	}

	public void setDateFinEvolutionProfessionnelleMasseur(Date dateFinEvolutionProfessionnelleMasseur) {
		this.dateFinEvolutionProfessionnelleMasseur = dateFinEvolutionProfessionnelleMasseur;
	}

	public Masseur getMasseur() {
		return masseur;
	}

	public void setMasseur(Masseur masseur) {
		this.masseur = masseur;
	}

	public EvolutionProfessionnelle getEvolutionsProfessionnelle() {
		return evolutionsProfessionnelle;
	}

	public void setEvolutionsProfessionnelle(EvolutionProfessionnelle evolutionsProfessionnelle) {
		this.evolutionsProfessionnelle = evolutionsProfessionnelle;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}