package fr.afcepf.ai107.nomads.outils.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai107.nomads.entities.AffectationCategorieMasseur;
import fr.afcepf.ai107.nomads.entities.CategorieMasseur;
import fr.afcepf.ai107.nomads.entities.EvolutionProfessionnelle;
import fr.afcepf.ai107.nomads.entities.HistoriqueEvolutionProfessionnelleMasseur;
import fr.afcepf.ai107.nomads.entities.Transaction;
import fr.afcepf.ai107.nomads.entities.TypePackMads;
import fr.afcepf.ai107.nomads.entities.VilleCp;
import fr.afcepf.ai107.nomads.idao.AffectationCategorieMasseurIDao;
import fr.afcepf.ai107.nomads.idao.CategorieMasseurIDao;
import fr.afcepf.ai107.nomads.idao.EvolutionProIDao;
import fr.afcepf.ai107.nomads.idao.HistoriqueEvoProMasseurIDao;
import fr.afcepf.ai107.nomads.idao.TransactionIDao;
import fr.afcepf.ai107.nomads.idao.VilleCpIDao;
import fr.afcepf.ai107.nomads.outils.ibusiness.OutilsIBusiness;

@Remote(OutilsIBusiness.class)
@Stateless
public class OutilsBusiness implements OutilsIBusiness {

	@EJB
	private VilleCpIDao proxyVilleCp;
	
	@EJB
	private EvolutionProIDao proxyEvoPro;
	
	@EJB
	private CategorieMasseurIDao proxyCategoMass;
	
	@EJB
	private HistoriqueEvoProMasseurIDao proxyHistoEvoProMasseur;
	
	@EJB
	private AffectationCategorieMasseurIDao proxyAffectationCatMass;
	
	@EJB
	private TransactionIDao proxyTransaction;
	
	@Override
	public List<String>listeCp() {
		List<String> codesPostaux = null;
			codesPostaux = proxyVilleCp.getListCp();
		return codesPostaux;
	}

	@Override
	public List<String> listeVilleByCp(String codeP) {
		List<String> villesByCp = null;
			villesByCp = proxyVilleCp.getVilleByCp(codeP);
		return villesByCp;
	}

	@Override
	public VilleCp VilleCpParNomVille(String ville) {
		VilleCp vc = null;
		vc = proxyVilleCp.getVilleCpbyNomVille(ville);
		return vc;
	}

	@Override
	public List<EvolutionProfessionnelle> getAll() {
		List<EvolutionProfessionnelle> evolutions = proxyEvoPro.findAll();
		return evolutions;
	}

	@Override
	public List<CategorieMasseur> getAllCategories() {
		List<CategorieMasseur> categories = proxyCategoMass.findAll();
		return categories;
	}

	/**
	 * Permet la récupération de l'historique de l'évolution professionnelle du masseur dont l'id est idMasseur 
	 */
	@Override
	public List<EvolutionProfessionnelle> getListeEvoProByIdMasseur(Integer idMasseur) {
		List<EvolutionProfessionnelle> evoProMasseur = null;
		evoProMasseur = proxyEvoPro.getEvoProByIdMasseur(idMasseur);
		return evoProMasseur;
	}

	@Override
	public List<CategorieMasseur> getListCategoMassByIdMass(Integer idMasseur) {
		List<CategorieMasseur> catMass = null;
		catMass = proxyCategoMass.listCategoMasseurByIdMasseur(idMasseur);
		return catMass;
	}

	@Override
	public List<HistoriqueEvolutionProfessionnelleMasseur> historiqueEvoProMassByIdMasseur(Integer idMasseur) {
		List<HistoriqueEvolutionProfessionnelleMasseur> listHistoByMass = null;
		listHistoByMass = proxyHistoEvoProMasseur.historiqueByIdMasseur(idMasseur);
		return listHistoByMass;
	}

	@Override
	public List<AffectationCategorieMasseur> getListAffectationCatMassByIdMass(Integer idMasseur) {
		 List<AffectationCategorieMasseur> listAffectationsCat = null;
		 listAffectationsCat=proxyAffectationCatMass.getListAffectationCatMassByIdMass(idMasseur);
		return listAffectationsCat;
	}

	@Override
	public List<Transaction> getListTransactionByIdMasseur(Integer idMasseur) {
		List<Transaction> transactions = null;
		transactions = proxyTransaction.getListTransactionByIdMasseur(idMasseur);
		return transactions;
	}

	@Override
	public EvolutionProfessionnelle getEvoProMasseurById (int IdEvoPro) {
		EvolutionProfessionnelle evo = proxyEvoPro.getById(IdEvoPro);
		return evo;
	}

	@Override
	public CategorieMasseur getCategoMasseurById(int idCategoMass) {
		CategorieMasseur catego = proxyCategoMass.getById(idCategoMass);
		return catego;
	}

	@Override
	public void updateAffectationCategorieMasseur(AffectationCategorieMasseur catego) {
		proxyAffectationCatMass.update(catego);
		
	}

	@Override
	public List<CategorieMasseur> getListCatMassDispoByIdMass(Integer idMasseur) {
		List<CategorieMasseur> categoDispo = proxyCategoMass.listCategoDispoMasseurByIdMasseur(idMasseur);
		return categoDispo;
	}

	@Override
	public void PersisterAffectationCategorie(AffectationCategorieMasseur affCat) {
		proxyAffectationCatMass.add(affCat);
		
	}

	@Override
	public void PersisterHistoEvolutionPro(HistoriqueEvolutionProfessionnelleMasseur histEvPro) {
		proxyHistoEvoProMasseur.add(histEvPro);
		
	}

	@Override
	public void updateHistoEvoPro(HistoriqueEvolutionProfessionnelleMasseur hist) {
		proxyHistoEvoProMasseur.update(hist);
		
	}
	public Transaction addTransaction(Transaction transaction) {
		return proxyTransaction.add(transaction);		
	}
}
