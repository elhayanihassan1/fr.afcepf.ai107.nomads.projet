package fr.afcepf.ai107.nomads.outils.ibusiness;

import java.util.List;

import fr.afcepf.ai107.nomads.entities.AffectationCategorieMasseur;
import fr.afcepf.ai107.nomads.entities.CategorieMasseur;
import fr.afcepf.ai107.nomads.entities.EvolutionProfessionnelle;
import fr.afcepf.ai107.nomads.entities.HistoriqueEvolutionProfessionnelleMasseur;
import fr.afcepf.ai107.nomads.entities.Transaction;
import fr.afcepf.ai107.nomads.entities.TypePackMads;
import fr.afcepf.ai107.nomads.entities.VilleCp;

public interface OutilsIBusiness {
	List<String> listeCp();
	List<String> listeVilleByCp (String codeP);
	VilleCp VilleCpParNomVille (String ville);
	List<EvolutionProfessionnelle> getAll();
	List<CategorieMasseur> getAllCategories();
	List<EvolutionProfessionnelle> getListeEvoProByIdMasseur (Integer idMasseur);
	List<CategorieMasseur> getListCategoMassByIdMass (Integer idMasseur);
	List<HistoriqueEvolutionProfessionnelleMasseur> historiqueEvoProMassByIdMasseur(Integer idMasseur);
	List<AffectationCategorieMasseur> getListAffectationCatMassByIdMass(Integer idMasseur);
	List<Transaction> getListTransactionByIdMasseur(Integer idMasseur);
	EvolutionProfessionnelle getEvoProMasseurById (int IdEvoPro);
	CategorieMasseur getCategoMasseurById (int idCategoMass);
	void updateAffectationCategorieMasseur (AffectationCategorieMasseur catego);
	List<CategorieMasseur> getListCatMassDispoByIdMass(Integer idMasseur);
	void PersisterAffectationCategorie (AffectationCategorieMasseur affCat);
	void PersisterHistoEvolutionPro (HistoriqueEvolutionProfessionnelleMasseur histEvPro);
	void updateHistoEvoPro (HistoriqueEvolutionProfessionnelleMasseur hist);
	Transaction addTransaction(Transaction transaction);
	
}
