package fr.afcepf.ai107.nomads.contoller.menu;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

@ManagedBean
@ViewScoped
public class MenuBeanAdmin implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private MenuModel model = new DefaultMenuModel();

	@PostConstruct //à l'initialisation, création des menus?
	public void init() {
		addMenu("Accueil", "Retour à mon Accueil");
		addMenu("Masseurs", "Liste des masseurs", "Ajouter un masseur");
		addMenu("Partenaires", "Liste des partenaires", "Ajouter un partenaire");
        addMenu("Mes Prestations", "Liste des prestations", "Ajouter une prestation", "Liste des demandes à traiter");
        addMenu("Mon tableau de bord", "Mon tableau de bord");
}


	//Constructeur par défaut pour ajouter des sous-menus?
	public DefaultSubMenu addMenu(String label, String... items) {  
		return addMenu(null, label, items);
	}

	//Constructeur surchargé avec un sous menu "Parent" en param
	public DefaultSubMenu addMenu(DefaultSubMenu parentMenu,String label, String... items) {
		
		DefaultSubMenu theMenu = new DefaultSubMenu(label);
		
		for (Object item : items) {
			DefaultMenuItem mi = new DefaultMenuItem(item);
			mi.setUrl(displayURL(item.toString()));
			theMenu.addElement(mi);
		}
		
				
		if (parentMenu == null) {
			model.addElement(theMenu);
		} else {
			parentMenu.addElement(theMenu);
		}
		return theMenu;
	}

	public MenuModel getMenuModel() {
		return model;
	}
	
	private String displayURL (String nomMenu) {
		String URLretournee = "#";
		switch (nomMenu) {
		case "Retour à mon Accueil":
			URLretournee="AccueilAdmin.xhtml";
			break;
		case "Accueil":
			URLretournee="AccueilAdmin.xhtml";
			break;
		case "Masseurs":
			URLretournee="MesMasseurs.xhtml";
			break;
		case "Liste des masseurs":
			URLretournee="MesMasseurs.xhtml";
			break;

		case "Ajouter un masseur":
			URLretournee="AjouterMasseur.xhtml";
			break;

		case "Partenaire":
			URLretournee="MesPartenaires.xhtml";
			break;
		case "Liste des partenaires":
			URLretournee="MesPartenaires.xhtml";
			break;

		case "Ajouter un partenaire":
			URLretournee="AjouterPartenaire.xhtml";
			break;
			
		case "Liste des prestations":
			URLretournee="ListePrestations.xhtml";
			break;
			
		case "Ajouter une prestation":
			URLretournee="AjoutPrestaIntervention.xhtml";
			break;
		case "Liste des demandes à traiter":
			URLretournee="ListeDesDemandeATraiter.xhtml";
			break;
		case "Mon tableau de bord":
			URLretournee="Dashboard.xhtml";
			break;
			
			
		default:
			break;
		}
	
		return URLretournee;
	}	
}
