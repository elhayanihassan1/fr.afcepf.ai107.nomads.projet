package fr.afcepf.ai107.nomads.idao;

import fr.afcepf.ai107.nomads.entities.ContratMasseur;

public interface ContratMasseurIDao extends GenericIDao<ContratMasseur> {
	ContratMasseur getContratByIdMasseur (Integer idMasseur);

}
