package fr.afcepf.ai107.nomads.idao;

import java.util.List;

import fr.afcepf.ai107.nomads.entities.IndisponibiliteMasseur;
import fr.afcepf.ai107.nomads.entities.Masseur;
import fr.afcepf.ai107.nomads.entities.MotifIndisponibiliteMasseur;

public interface IndisponibiliteMasseurIDao extends GenericIDao<IndisponibiliteMasseur> {

	List<Masseur> getListeMasseursActifs();
	List<Masseur> getListeMasseursIndispo();
	List<IndisponibiliteMasseur> getIndispoMasseurFutByIdMasseur(Integer idMasseur);
//	List<MotifIndisponibiliteMasseur> getAllMotifsIndispoMass();
	List<IndisponibiliteMasseur> getIndispoMasseurPasseesByIdMasseur(Integer idMasseur);

}
