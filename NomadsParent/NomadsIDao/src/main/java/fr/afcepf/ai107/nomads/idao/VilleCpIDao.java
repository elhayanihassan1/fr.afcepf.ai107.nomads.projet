package fr.afcepf.ai107.nomads.idao;

import java.util.List;

import fr.afcepf.ai107.nomads.entities.VilleCp;

public interface VilleCpIDao extends GenericIDao<VilleCp> {
	//List<VilleCp> getListCp ();
	List<String> getListCp ();
	List<String> getVilleByCp(String codePostal);
	VilleCp getVilleCpbyNomVille (String ville);

}
