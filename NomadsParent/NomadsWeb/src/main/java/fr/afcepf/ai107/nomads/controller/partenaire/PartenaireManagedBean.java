package fr.afcepf.ai107.nomads.controller.partenaire;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.mail.Part;

import fr.afcepf.ai107.nomads.entities.Administrateur;
import fr.afcepf.ai107.nomads.entities.ContratPartenaire;
import fr.afcepf.ai107.nomads.entities.EvolutionProfessionnelle;
import fr.afcepf.ai107.nomads.entities.IndisponibilitePartenaire;
import fr.afcepf.ai107.nomads.entities.Partenaire;
import fr.afcepf.ai107.nomads.entities.TypePartenaire;
import fr.afcepf.ai107.nomads.outils.ibusiness.OutilsIBusiness;
import fr.afcepf.ai107.nomads.partenaire.ibusiness.PartenaireIBusiness;


@ManagedBean(name = "mbPartenaire")
@ViewScoped
public class PartenaireManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@ManagedProperty (value = "#{mbAccount.admin}" )
	private Administrateur administrateur;
	@EJB
	private PartenaireIBusiness  proxyPartenaireBusiness;

	@EJB
	private OutilsIBusiness proxyVilleCpBusiness;

	private Partenaire selectedPartenaire = new Partenaire();
	private Partenaire nouveauPartenaire = new Partenaire();
	private int TypePartenaireId;
	private List<TypePartenaire> listeTypePartenaire ;
	private List<String> codesPostaux = new ArrayList<String>();
	private List<String> motifs = new ArrayList<String>();
	private List<String> villes = new ArrayList<String>();
	private String selectedCP;
	private String selectedVille;
	private List<Partenaire> listePartenaire;
	private Integer idPartenaire;
	private String success;
	private Date dateFinContrat;
	private Integer referencePartenaire;
	private Integer idTypePartenaire;
	private Integer listePartenaireSize;
	private ContratPartenaire contratPartenaire = new ContratPartenaire();
	private String motifFinContrat;
	private int dernierContratEnregistre;
	private String message = "";
	private IndisponibilitePartenaire nouvelleIndispo = new IndisponibilitePartenaire();
	private boolean aEteModifie = false;
	private boolean aEteAjoute = false;
	private String statutDispoSelected;
	private List<Partenaire> listePartenaireInactifs = null;
	private List<Partenaire> listePartenaireActifs = null;
	private List<String> actifInactif = new ArrayList<String>();
	
	@PostConstruct
	public void init() {
		
		listePartenaire=proxyPartenaireBusiness.getAllPartenaires();
		
		actifInactif.add("Tous");
		actifInactif.add("Disponibles");
		actifInactif.add("Indisponibles");
		
		codesPostaux = proxyVilleCpBusiness.listeCp();
		
		listeTypePartenaire=proxyPartenaireBusiness.getTypePartenaire();
		
		motifs = (proxyPartenaireBusiness.getAllMotifsFinContratPartenaire());

	}

	public void onCPChange() { 
		if (selectedCP !=null && !selectedCP.equals("")){
			villes = proxyVilleCpBusiness.listeVilleByCp(selectedCP);
		}else {
			villes = new ArrayList<String>();
		}
			
		if (statutDispoSelected != null && statutDispoSelected !="") {
			if (statutDispoSelected.equals("Indisponibles")) {
				listePartenaire = proxyPartenaireBusiness.getAllPartenaireInactifs();
			} 
			if (statutDispoSelected.equals("Disponibles")) {
			listePartenaire=proxyPartenaireBusiness.getAllPartenaireActifs();
			}
			if (statutDispoSelected.equals("Tous")) {
				listePartenaire=proxyPartenaireBusiness.getAllPartenaires();
				}
		}
	
	}

	public String fichePartenaire() {
		return "FichePartenaire.xhtml?faces-redirect=true&idPartSelected="+selectedPartenaire.getIdPartenaire(); 
	}
	
	public String modifierPartenaire() {
		return "ModifierPartenaire.xhtml?faces-redirect=true&idPartSelected="+selectedPartenaire.getIdPartenaire(); 
	}
	
	public String ajouterPartenaire() {
		return "AjouterPartenaire.xhtml?faces-redirect=true";
	}

	public List<Partenaire> desactiverPartenaire() {
		IndisponibilitePartenaire nouvelleIndisponibilite = null;
		contratPartenaire=proxyPartenaireBusiness.getContratPartenaireById(selectedPartenaire);
		if (motifFinContrat != null) {
			contratPartenaire.setMotifFinContratPartenaire(proxyPartenaireBusiness.getMotifByLibelleMotif(motifFinContrat));
		}

		contratPartenaire.setDateFinContratPartenaire(dateFinContrat);
		nouvelleIndispo.setDateDebutIndisponibilitePartenaire(dateFinContrat);
		nouvelleIndispo.setDateFinIndisponibilitePartenaire(null);
		nouvelleIndispo.setPartenaire(selectedPartenaire);		
		proxyPartenaireBusiness.desactiverPartenaire(contratPartenaire);		
		proxyPartenaireBusiness.ajouterIndispoPart(nouvelleIndispo);		
		success="La modification a été effectuée";	
		return proxyPartenaireBusiness.getAllPartenaireActifs();
	}

	public Partenaire getSelectedPartenaire() {
		return selectedPartenaire;
	}
	public void setSelectedPartenaire(Partenaire selectedPartenaire) {
		this.selectedPartenaire = selectedPartenaire;
	}
	public Partenaire getNouveauPartenaire() {
		return nouveauPartenaire;
	}
	public void setNouveauPartenaire(Partenaire nouveauPartenaire) {
		this.nouveauPartenaire = nouveauPartenaire;
	}
	public int getTypePartenaireId() {
		return TypePartenaireId;
	}
	public void setTypePartenaireId(int typePartenaireId) {
		TypePartenaireId = typePartenaireId;
	}
	public List<TypePartenaire> getListeTypePartenaire() {
		return listeTypePartenaire;
	}
	public void setListeTypePartenaire(List<TypePartenaire> listeTypePartenaire) {
		this.listeTypePartenaire = listeTypePartenaire;
	}
	public List<String> getCodesPostaux() {
		return codesPostaux;
	}
	public void setCodesPostaux(List<String> codesPostaux) {
		this.codesPostaux = codesPostaux;
	}
	public List<String> getVilles() {
		return villes;
	}
	public void setVilles(List<String> villes) {
		this.villes = villes;
	}
	public String getSelectedCP() {
		return selectedCP;
	}
	public void setSelectedCP(String selectedCP) {
		this.selectedCP = selectedCP;
	}
	public String getSelectedVille() {
		return selectedVille;
	}
	public void setSelectedVille(String selectedVille) {
		this.selectedVille = selectedVille;
	}
	public List<Partenaire> getListePartenaire() {
		return listePartenaire;
	}
	public void setListePartenaire(List<Partenaire> listePartenaire) {
		this.listePartenaire = listePartenaire;
	}
	public int getIdPartenaire() {
		return idPartenaire;
	}
	public void setIdPartenaire(int idPartenaire) {
		this.idPartenaire = idPartenaire;
	}
	public String getSuccess() {
		return success;
	}
	public void setSuccess(String success) {
		this.success = success;
	}

	public Date getDateFinContrat() {
		return dateFinContrat;
	}

	public void setDateFinContrat(Date dateFinContrat) {
		this.dateFinContrat = dateFinContrat;
	}

	public Integer getReferencePartenaire() {
		return referencePartenaire;
	}

	public void setReferencePartenaire(Integer referencePartenaire) {
		this.referencePartenaire = referencePartenaire;
	}

	public void setIdPartenaire(Integer idPartenaire) {
		this.idPartenaire = idPartenaire;
	}

	public Integer getIdTypePartenaire() {
		return idTypePartenaire;
	}

	public void setIdTypePartenaire(Integer idTypePartenaire) {
		this.idTypePartenaire = idTypePartenaire;
	}

	public Integer getListePartenaireSize() {
		return listePartenaireSize;
	}

	public void setListePartenaireSize(Integer listePartenaireSize) {
		this.listePartenaireSize = listePartenaireSize;
	}

	public ContratPartenaire getContratPartenaire() {
		return contratPartenaire;
	}

	public void setContratPartenaire(ContratPartenaire contratPartenaire) {
		this.contratPartenaire = contratPartenaire;
	}

	public List<String> getMotifs() {
		return motifs;
	}

	public void setMotifs(List<String> motifs) {
		this.motifs = motifs;
	}

	public String getMotifFinContrat() {
		return motifFinContrat;
	}

	public void setMotifFinContrat(String motifFinContrat) {
		this.motifFinContrat = motifFinContrat;
	}

	public int getDernierContratEnregistre() {
		return dernierContratEnregistre;
	}

	public void setDernierContratEnregistre(int dernierContratEnregistre) {
		this.dernierContratEnregistre = dernierContratEnregistre;
	}

	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public IndisponibilitePartenaire getNouvelleIndispo() {
		return nouvelleIndispo;
	}
	public void setNouvelleIndispo(IndisponibilitePartenaire nouvelleIndispo) {
		this.nouvelleIndispo = nouvelleIndispo;
	}
	public boolean isaEteModifie() {
		return aEteModifie;
	}
	public void setaEteModifie(boolean aEteModifie) {
		this.aEteModifie = aEteModifie;
	}
	public boolean isaEteAjoute() {
		return aEteAjoute;
	}
	public void setaEteAjoute(boolean aEteAjoute) {
		this.aEteAjoute = aEteAjoute;
	}
	public List<Partenaire> getListePartenaireInactifs() {
		return listePartenaireInactifs;
	}
	public void setListePartenaireInactifs(List<Partenaire> listePartenaireInactifs) {
		this.listePartenaireInactifs = listePartenaireInactifs;
	}
	public List<Partenaire> getListePartenaireActifs() {
		return listePartenaireActifs;
	}
	public void setListePartenaireActifs(List<Partenaire> listePartenaireActifs) {
		this.listePartenaireActifs = listePartenaireActifs;
	}
	public List<String> getActifInactif() {
		return actifInactif;
	}
	public void setActifInactif(List<String> actifInactif) {
		this.actifInactif = actifInactif;
	}
	public String getStatutDispoSelected() {
		return statutDispoSelected;
	}
	public void setStatutDispoSelected(String statutDispoSelected) {
		this.statutDispoSelected = statutDispoSelected;
	}

	public Administrateur getAdministrateur() {
		return administrateur;
	}

	public void setAdministrateur(Administrateur administrateur) {
		this.administrateur = administrateur;
	}


}