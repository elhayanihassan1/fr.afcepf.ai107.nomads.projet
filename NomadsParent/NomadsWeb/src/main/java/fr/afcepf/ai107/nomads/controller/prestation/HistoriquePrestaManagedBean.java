package fr.afcepf.ai107.nomads.controller.prestation;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.Prestation;
import fr.afcepf.ai107.nomads.prestation.ibusiness.PrestationIBusiness;

@ManagedBean(name = "mbHistoriquePresta")
@RequestScoped
public class HistoriquePrestaManagedBean {

	@EJB
	private PrestationIBusiness proxyPrestationBusiness;
	
	private List<InterventionReelle> prestations = new ArrayList<InterventionReelle>();
	
	@PostConstruct
	public void init() {
		prestations = proxyPrestationBusiness.getListInterventionReellePassees();
	//	for(Prestation presta : prestations ) {
	//		System.out.println(presta.getLibellePrestation());
	//	}
	}
	
	public String onButtonClick() {
		return "/ListePrestations.xhtml?faces-redirect=true";
	}

	/**
	 * @return the prestations
	 */
	public List<InterventionReelle> getPrestations() {
		return prestations;
	}

	/**
	 * @param prestations the prestations to set
	 */
	public void setPrestations(List<InterventionReelle> prestations) {
		this.prestations = prestations;
	}
	
	
}
