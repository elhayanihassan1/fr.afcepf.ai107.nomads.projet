package fr.afcepf.ai107.nomads.administrateur.business;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai107.nomads.administrateur.ibusiness.AdministrateurIBusiness;
import fr.afcepf.ai107.nomads.entities.Administrateur;
import fr.afcepf.ai107.nomads.idao.AdministrateurIDao;


@Remote(AdministrateurIBusiness.class)
@Stateless
public class AdministrateurBusiness  implements AdministrateurIBusiness{

	
	@EJB
	private AdministrateurIDao proxyAdministrateurDAO;
	
	
	@Override
	public Administrateur connection(String login, String password) {
	
		
		return proxyAdministrateurDAO.authenticate(login, password);
	}

}
