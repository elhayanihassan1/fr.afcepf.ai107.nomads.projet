package fr.afcepf.ai107.nomads.controller.masseurs;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import fr.afcepf.ai107.nomads.entities.Administrateur;
import fr.afcepf.ai107.nomads.entities.AffectationCategorieMasseur;
import fr.afcepf.ai107.nomads.entities.EvolutionProfessionnelle;
import fr.afcepf.ai107.nomads.entities.HistoriqueEvolutionProfessionnelleMasseur;
import fr.afcepf.ai107.nomads.entities.InscriptionMasseurs;
import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.Masseur;
import fr.afcepf.ai107.nomads.entities.RetourExperienceMasseur;
import fr.afcepf.ai107.nomads.entities.Transaction;
import fr.afcepf.ai107.nomads.entities.TypePackMads;
import fr.afcepf.ai107.nomads.masseurs.ibusiness.InscriptionMasseursIBusiness;
import fr.afcepf.ai107.nomads.masseurs.ibusiness.MasseurIBusiness;
import fr.afcepf.ai107.nomads.outils.ibusiness.OutilsIBusiness;
import fr.afcepf.ai107.nomads.outils.ibusiness.TypePackMadsIBusiness;
import fr.afcepf.ai107.nomads.prestation.ibusiness.PrestationIBusiness;
import fr.afcepf.ai107.nomads.retours.ibusiness.RetoursMasseursIBusiness;

@ManagedBean(name = "mbFicheMasseurMasseur")
@ViewScoped // Session? Request? entre les deux?... REQUEST: tout fonctionne sauf le bouton
			// de redirection==> Test passage en View
public class FicheMasseurManagedMasseur {

	@EJB
	private MasseurIBusiness proxyMasseurBusiness;

	@EJB
	private OutilsIBusiness proxyOutilBusiness;

	@EJB
	private PrestationIBusiness proxyPrestationBusiness;
	
	@EJB
	private InscriptionMasseursIBusiness proxyInscriMassBusiness;

	@EJB
	private RetoursMasseursIBusiness proxyRetourMasseur;
	
	@EJB
	private TypePackMadsIBusiness proxyTypePackMads;
	
	@ManagedProperty(value = "#{mbAccount.masseur}")
	private Masseur masseurConnected;

	
	private Masseur leMassConn = new Masseur();

	private List<InterventionReelle> interventionsAVenir;
	private List<InterventionReelle> interventionsAVenir3J;
	private List<HistoriqueEvolutionProfessionnelleMasseur> listeHistoEvoProMass;
	private HistoriqueEvolutionProfessionnelleMasseur dernierHistoEvoPro;
	private List<AffectationCategorieMasseur> listeAffectationCatMass;
	private Double soldeMads;
	private List<Transaction> listeTransactionsMasseur;
	private List<InterventionReelle> listeInterventionsReellesMasseur;
	private List<InterventionReelle> interventionsPassees; // #####################A IMPLEMENTER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	private List<InterventionReelle> interventionsAAfficher;
	
	private Transaction nouvelleTransaction;
	private InterventionReelle selectedIntervention;
	private boolean historique = false;
	


	private TypePackMads packMads;
	private Date currentDate=new Date();
	
	private LocalDateTime localDateTime = LocalDateTime.ofInstant(currentDate.toInstant(), ZoneId.systemDefault());
	private Date dateFromLocalDT = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
//	private EvolutionProfessionnelle derniereEvoPro;
//	
	List<RetourExperienceMasseur> monRetour;
	
	private boolean estNeoNomad;
	
//	
	@PostConstruct
	public void init() {
		System.out.println("############# Je suis devant la port du TRY");
		try {
			nouvelleTransaction = new Transaction();
			System.out.println("############### je suis dans le try");
				int idTypePack = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("typePack"));
				packMads=proxyTypePackMads.getByIdPack(idTypePack);
//				ajoutMadsMasseur(packMads);
				
				System.out.println("####### je suis dans la méthode ajout Mads");
				nouvelleTransaction.setTypePackMads(packMads);
				nouvelleTransaction.setDateTransaction(dateFromLocalDT);
				System.out.println("####### je set la date à "+dateFromLocalDT);
				nouvelleTransaction.setMasseur(masseurConnected);
				nouvelleTransaction.setNombreDePack(1);
				System.out.println("####### je set le masseur à "+masseurConnected.getNomMasseur());
				nouvelleTransaction=proxyOutilBusiness.addTransaction(nouvelleTransaction);
				System.out.println("####### j'ajoute une nouvelle transaction");
				System.out.println("############### je sors dans le try");
	
		} catch (Exception e) {
			System.out.println("############### je suis dans le catch");
				leMassConn=masseurConnected;
		}
		System.out.println(masseurConnected.getNomMasseur());
		interventionsAVenir = proxyPrestationBusiness
				.getAllInfosInterventionReelleAVenirByIdMasseur(masseurConnected.getId_masseur());
		interventionsAAfficher = interventionsAVenir;
		interventionsAVenir3J = proxyPrestationBusiness.getAllInfosInterventionReelleAVenir3JByIdMasseur(masseurConnected.getId_masseur());
		listeHistoEvoProMass = proxyOutilBusiness.historiqueEvoProMassByIdMasseur(masseurConnected.getId_masseur());
		dernierHistoEvoPro = listeHistoEvoProMass.get(listeHistoEvoProMass.size() - 1);
		listeAffectationCatMass = proxyOutilBusiness.getListAffectationCatMassByIdMass(masseurConnected.getId_masseur());
		listeTransactionsMasseur = proxyOutilBusiness.getListTransactionByIdMasseur(masseurConnected.getId_masseur()); 
		
		listeInterventionsReellesMasseur = proxyPrestationBusiness.getAllInfosInterventionReelleByIdMasseur(masseurConnected.getId_masseur());
		interventionsPassees = proxyPrestationBusiness.getAllInfosInterventionReellePasseesByIdMasseur(masseurConnected.getId_masseur());
		
		
		Double totalMads = 0.0;
		Double totalDepenses = 0.0;
		
		if(listeTransactionsMasseur != null) {
			for (Transaction transaction : listeTransactionsMasseur) {
				totalMads += transaction.getNombreDePack() * transaction.getTypePackMads().getValeurTypePackMads();
			}
		}else {
			totalMads = 0.0;
		}
		if(listeInterventionsReellesMasseur !=null) {
			for (InterventionReelle interventionReelle : listeInterventionsReellesMasseur) {
				Integer truc =interventionReelle.getPrestation().getCoutMads();
				if( truc != null) {
					totalDepenses += interventionReelle.getPrestation().getCoutMads();
				}
			}
		}else {
			totalDepenses = 0.0;
		}
		soldeMads = totalMads - totalDepenses;
		if (soldeMads == null) {
			soldeMads = 0.0;
		}


		setEstNeoNomad(proxyMasseurBusiness.EstNeonomad(masseurConnected.getId_masseur()));

	

		
//		monRetour = proxyRetourMasseur.getRetourExByIdIntervention(selectedIntervention, masseurConnected);

	}//fin post construct
//	protected void ajoutMadsMasseur(TypePackMads packMads) {
//		System.out.println("####### je suis dans la méthode ajout Mads");
//		nouvelleTransaction.setDateTransaction(dateFromLocalDT);
//		System.out.println("####### je set la date à "+dateFromLocalDT);
//		nouvelleTransaction.setMasseur(masseurConnected);
//		nouvelleTransaction.setNombreDePack(1);
//		System.out.println("####### je set le masseur à "+masseurConnected.getNomMasseur());
//		proxyOutilBusiness.addTransaction(nouvelleTransaction);
//		System.out.println("####### j'ajoute une nouvelle transaction");
//	}
	public String sInscrireAPrestation() {
		return "/InscriptionPrestationMasseur.xhtml?faces-redirect=true";
	}
	
	public void consulterHistorique() {
		historique = true;
	

		System.out.println("Je rentre dans l'historique à consulter, enfin la méthode...ùùùùùùùùùùùùùùùùùùùùùùùùùùùùùùùùùùù");
		interventionsAAfficher = new ArrayList<InterventionReelle>(interventionsPassees) ; //Réinitialisation de la collection
//		System.out.println(interventionsAAfficher.size());
//		for (InterventionReelle itv : interventionsAAfficher) {
//			System.out.println(itv.getPrestation().getLibellePrestation());
//		};
//		System.out.println(interventionsPassees.size());
//		for (InterventionReelle itv : interventionsPassees) {
//			System.out.println(itv.getPrestation().getLibellePrestation());
//		};
	}
	
	public void consulterPrestAvenir () {
		System.out.println("###########################################@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ JE SUIS RENTRÉ DANS LA METHODE DE RETOUR DES PRSTAS A VENIR !:!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		interventionsAVenir = proxyPrestationBusiness
				.getAllInfosInterventionReelleAVenirByIdMasseur(masseurConnected.getId_masseur());
		if (interventionsAVenir != null) {
		interventionsAAfficher = new ArrayList<InterventionReelle>(interventionsAVenir);
		}else {
			interventionsAAfficher = null;
		}
		historique = false;
	}
	
	public String mettreAjour() { 
	
return "/MiseAJourMasseurParMasseur.xhtml?faces-redirect=true";
}
	public String acheterMads() {
		return "AcheterMads.xhtml?faces-redirect=true";
	}

	public String lienFichePrestation() {
		System.out.println("Je rentre dans le lien");
		System.out.println("valeur de selectedInterv : " + selectedIntervention.getPrestation().getLibellePrestation());
		return "FichePrestation.xhtml?faces-redirect=true&id=" + selectedIntervention.getIdInterventionReelle();
	}
	
	public void seDesinscrire() {
		InscriptionMasseurs inscri = new InscriptionMasseurs();
		inscri = proxyInscriMassBusiness.GetInscriptionByMasseurAndInterv(masseurConnected, selectedIntervention);
		inscri.setDateDesinscriptionMasseur(new Date());
		proxyInscriMassBusiness.mettreAJourInscriptionMasseur(inscri);
		interventionsAAfficher = proxyPrestationBusiness
				.getAllInfosInterventionReelleAVenirByIdMasseur(masseurConnected.getId_masseur());
	}
	
	public String faireUnRetourDexperience() {
		System.out.println(selectedIntervention);
		String lol = null;
		monRetour = proxyRetourMasseur.getRetourExByIdIntervention(selectedIntervention, masseurConnected);
		System.out.println(monRetour + "PKZRFGZPRIGHZOIFHZIGHZEPIFJZEPGJZP");
		if(monRetour == null) {
		lol = "/RetourExperienceMasseur.xhtml?faces-redirect=true&id="+selectedIntervention.getIdInterventionReelle();
		}else {
			lol = "/AffichageRetourMasseur.xhtml?faces-redirect=true&id="+selectedIntervention.getIdInterventionReelle();
		}
		return lol;
	}
	
//	public boolean estVisible() {
//		boolean visible = false;
//		List<RetourExperienceMasseur> monRetour;
//		System.out.println(selectedIntervention + "efizepfzpefjepof^fokef^fezfjoej + " + masseurConnected);
//		monRetour = proxyRetourMasseur.getRetourExByIdIntervention(selectedIntervention, masseurConnected);
//		System.out.println(monRetour);
//		if(monRetour == null) {
//			visible = true;
//		}else {
//			visible = false;
//		}
//		return visible;
//	}

	// ========================================================================================================================================
	
	public List<InterventionReelle> getInterventionsAVenir() {
		return interventionsAVenir;
	}


	/**
	 * @return the monRetour
	 */
	public List<RetourExperienceMasseur> getMonRetour() {
		return monRetour;
	}
	/**
	 * @param monRetour the monRetour to set
	 */
	public void setMonRetour(List<RetourExperienceMasseur> monRetour) {
		this.monRetour = monRetour;
	}
	public void setInterventionsAVenir(List<InterventionReelle> interventionsAVenir) {
		this.interventionsAVenir = interventionsAVenir;
	}

	public Masseur getLeMassConn() {
		return leMassConn;
	}

	public void setLeMassConn(Masseur leMassConn) {
		this.leMassConn = leMassConn;
	}

	/**
	 * @return the masseurConnected
	 */
	public Masseur getMasseurConnected() {
		return masseurConnected;
	}

	/**
	 * @param masseurConnected the masseurConnected to set
	 */
	public void setMasseurConnected(Masseur masseurConnected) {
		this.masseurConnected = masseurConnected;
	}

	public HistoriqueEvolutionProfessionnelleMasseur getDernierHistoEvoPro() {
		return dernierHistoEvoPro;
	}

	public void setDernierHistoEvoPro(HistoriqueEvolutionProfessionnelleMasseur dernierHistoEvoPro) {
		this.dernierHistoEvoPro = dernierHistoEvoPro;
	}

	public List<AffectationCategorieMasseur> getListeAffectationCatMass() {
		return listeAffectationCatMass;
	}

	public void setListeAffectationCatMass(List<AffectationCategorieMasseur> listeAffectationCatMass) {
		this.listeAffectationCatMass = listeAffectationCatMass;
	}

	public Double getSoldeMads() {
		return soldeMads;
	}

	public void setSoldeMads(Double soldeMads) {
		this.soldeMads = soldeMads;
	}

	public List<InterventionReelle> getInterventionsPassees() {
		return interventionsPassees;
	}

	public void setInterventionsPassees(List<InterventionReelle> interventionsPassees) {
		this.interventionsPassees = interventionsPassees;
	}

	/**
	 * @return the listeHistoEvoProMass
	 */
	public List<HistoriqueEvolutionProfessionnelleMasseur> getListeHistoEvoProMass() {
		return listeHistoEvoProMass;
	}

	/**
	 * @param listeHistoEvoProMass the listeHistoEvoProMass to set
	 */
	public void setListeHistoEvoProMass(List<HistoriqueEvolutionProfessionnelleMasseur> listeHistoEvoProMass) {
		this.listeHistoEvoProMass = listeHistoEvoProMass;
	}

	/**
	 * @return the listeTransactionsMasseur
	 */
	public List<Transaction> getListeTransactionsMasseur() {
		return listeTransactionsMasseur;
	}

	/**
	 * @param listeTransactionsMasseur the listeTransactionsMasseur to set
	 */
	public void setListeTransactionsMasseur(List<Transaction> listeTransactionsMasseur) {
		this.listeTransactionsMasseur = listeTransactionsMasseur;
	}

	/**
	 * @return the listeInterventionsReellesMasseur
	 */
	public List<InterventionReelle> getListeInterventionsReellesMasseur() {
		return listeInterventionsReellesMasseur;
	}

	/**
	 * @param listeInterventionsReellesMasseur the listeInterventionsReellesMasseur to set
	 */
	public void setListeInterventionsReellesMasseur(List<InterventionReelle> listeInterventionsReellesMasseur) {
		this.listeInterventionsReellesMasseur = listeInterventionsReellesMasseur;
	}
	public List<InterventionReelle> getInterventionsAVenir3J() {
		return interventionsAVenir3J;
	}
	public void setInterventionsAVenir3J(List<InterventionReelle> interventionsAVenir3J) {
		this.interventionsAVenir3J = interventionsAVenir3J;
	}
	public boolean isHistorique() {
		return historique;
	}
	public void setHistorique(boolean historique) {
		this.historique = historique;
	}
	public List<InterventionReelle> getInterventionsAAfficher() {
		return interventionsAAfficher;
	}
	public void setInterventionsAAfficher(List<InterventionReelle> interventionsAAfficher) {
		this.interventionsAAfficher = interventionsAAfficher;
	}
	/**
	 * @return the selectedIntervention
	 */
	public InterventionReelle getSelectedIntervention() {
		return selectedIntervention;
	}
	/**
	 * @param selectedIntervention the selectedIntervention to set
	 */
	public void setSelectedIntervention(InterventionReelle selectedIntervention) {
		this.selectedIntervention = selectedIntervention;
	}

	public boolean isEstNeoNomad() {
		return estNeoNomad;
	}
	public void setEstNeoNomad(boolean estNeoNomad) {
		this.estNeoNomad = estNeoNomad;
	}
	public TypePackMads getPackMads() {
		return packMads;
	}
	public void setPackMads(TypePackMads packMads) {
		this.packMads = packMads;
	}
	public Date getCurrentDate() {
		return currentDate;
	}
	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}
	public LocalDateTime getLocalDateTime() {
		return localDateTime;
	}
	public void setLocalDateTime(LocalDateTime localDateTime) {
		this.localDateTime = localDateTime;
	}
	public Date getDateFromLocalDT() {
		return dateFromLocalDT;
	}
	public void setDateFromLocalDT(Date dateFromLocalDT) {
		this.dateFromLocalDT = dateFromLocalDT;
	}
	public Transaction getNouvelleTransaction() {
		return nouvelleTransaction;
	}
	public void setNouvelleTransaction(Transaction nouvelleTransaction) {
		this.nouvelleTransaction = nouvelleTransaction;

	}



}
