package fr.afcepf.ai107.nomads.outils.ibusiness;

import java.util.List;

import fr.afcepf.ai107.nomads.entities.TypePackMads;

public interface TypePackMadsIBusiness {
	
	public List<TypePackMads> allPackMads();
	public TypePackMads add(TypePackMads newPack);
	boolean delete(TypePackMads deletedPack);
	public TypePackMads udpate(TypePackMads updatedPack);
	public TypePackMads getByIdPack(int idPackMads);
	List<TypePackMads> allNonRemovedPackMads();
	List<TypePackMads> allRemovedPackMads();

}