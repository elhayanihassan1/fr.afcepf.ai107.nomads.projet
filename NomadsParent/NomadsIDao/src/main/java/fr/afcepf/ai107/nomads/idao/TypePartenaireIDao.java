package fr.afcepf.ai107.nomads.idao;

import java.util.List;

import fr.afcepf.ai107.nomads.entities.TypePartenaire;


public interface TypePartenaireIDao extends GenericIDao<TypePartenaire> {
	List<TypePartenaire> getAll();
    
	TypePartenaire getById( int id);
	TypePartenaire getTypePartenaireByIdIntervention(Integer idInterv);
}
