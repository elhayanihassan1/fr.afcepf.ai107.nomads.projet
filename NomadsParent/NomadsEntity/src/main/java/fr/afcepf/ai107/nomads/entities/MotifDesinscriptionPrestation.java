package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import javax.persistence.Table;

@Entity
@Table(name = "motif_desinscription_prestation")
public class MotifDesinscriptionPrestation implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_motif_desinscription_prestation")
	private int idMotifDesinscriptionPrestation;

	@Column(name = "libelle_motif_desinscription_prestation")
	private String libelleMotifDesinsciptionPrestation;
	
	@OneToMany (mappedBy = "motifDesinscriptionPrestation", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY )
	private Set<InscriptionMasseurs> inscriptionsMasseur;

	public MotifDesinscriptionPrestation() {
		super();
		
	}

	public MotifDesinscriptionPrestation(int idMotifDesinscriptionPrestation,
			String libelleMotifDesinsciptionPrestation, Set<InscriptionMasseurs> inscriptionsMasseur) {
		super();
		this.idMotifDesinscriptionPrestation = idMotifDesinscriptionPrestation;
		this.libelleMotifDesinsciptionPrestation = libelleMotifDesinsciptionPrestation;
		this.inscriptionsMasseur = inscriptionsMasseur;
	}

	public int getIdMotifDesinscriptionPrestation() {
		return idMotifDesinscriptionPrestation;
	}

	public void setIdMotifDesinscriptionPrestation(int idMotifDesinscriptionPrestation) {
		this.idMotifDesinscriptionPrestation = idMotifDesinscriptionPrestation;
	}

	public String getLibelleMotifDesinsciptionPrestation() {
		return libelleMotifDesinsciptionPrestation;
	}

	public void setLibelleMotifDesinsciptionPrestation(String libelleMotifDesinsciptionPrestation) {
		this.libelleMotifDesinsciptionPrestation = libelleMotifDesinsciptionPrestation;
	}

	public Set<InscriptionMasseurs> getInscriptionsMasseur() {
		return inscriptionsMasseur;
	}

	public void setInscriptionsMasseur(Set<InscriptionMasseurs> inscriptionsMasseur) {
		this.inscriptionsMasseur = inscriptionsMasseur;
	}
	
}