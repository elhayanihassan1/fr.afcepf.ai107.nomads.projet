package fr.afcepf.ai107.nomads.controller.retours;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import fr.afcepf.ai107.nomads.contoller.menu.MenuBeanPartenaire;
import fr.afcepf.ai107.nomads.controller.partenaire.InfoPartenaireManagedBean;
import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.Partenaire;
import fr.afcepf.ai107.nomads.entities.RetourExperiencePartenaire;
import fr.afcepf.ai107.nomads.partenaire.ibusiness.PartenaireIBusiness;
import fr.afcepf.ai107.nomads.prestation.ibusiness.PrestationIBusiness;
import fr.afcepf.ai107.nomads.retours.ibusiness.RetoursPartenairesIBusiness;

@ManagedBean(name = "retourPartenaire")
@ViewScoped
public class FaireUnRetourPartenaireManagedBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private PrestationIBusiness proxyPrestationBusiness;
	@EJB
	private RetoursPartenairesIBusiness proxyRetourPartenaire;
	@EJB
	private PartenaireIBusiness proxyPartenaire;
	

//	private String typeAccount = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("typeAccount");
//	public Integer monPartenaireCo() {
//		Integer part = null;
//		if(typeAccount == "partenaire") {
//			part = MenuBeanPartenaire.getPartenaireId();
//		}
//		System.out.println(part + "KPSIHU9GF8GYUIHOK");
//		return part;
//	}
	@ManagedProperty(value = "#{mbAccount.partenaire}")
	private Partenaire partenaireConnected;
	
	private InterventionReelle interventionReelle = new InterventionReelle();
	private Integer notePonctualite;
	private Integer noteQualite;
	private Integer noteRelationnelle;
	private String commentaire;
	private String libellePrestation;
//	private Integer idPartenaire = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id"));
	private Integer idIntervention = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id"));
	private RetourExperiencePartenaire monAvis = new RetourExperiencePartenaire();
	private List<RetourExperiencePartenaire> monRetour;


	@PostConstruct
	private void init() {
	
		interventionReelle = proxyPrestationBusiness.getInterventionById(idIntervention);
	//	partenaireConnected = proxyPartenaire.getInfosPartenaireById(monPartenaireCo());
	//	partenaireConnected = proxyPartenaire.getInfosPartenaireById(idPartenaire);
	}
	
	public RetourExperiencePartenaire MonRtour(){
		RetourExperiencePartenaire interv;
		interventionReelle = proxyPrestationBusiness.getInterventionById(idIntervention);
	//	partenaireConnected = proxyPartenaire.getInfosPartenaireById(idPartenaire);
		monRetour = proxyRetourPartenaire.getRetourByIntervEtMasseur(interventionReelle, partenaireConnected);
		interv = monRetour.get(0);
		return interv;
	}

	
public String ajouterCommentaire() {
		
		if(commentaire != null) {
		monAvis.setLibelleRetourExperiencePartenaire(commentaire);
		}
		monAvis.setDateRetourExperiencePartenaire(new Date());
	//	monAvis.setIdRetourExperienceMasseur(null); changer en integer au besoin
		if(interventionReelle != null) {
		monAvis.setInterventionReelle(interventionReelle);
		}
	//	partenaireConnected = proxyPartenaire.getInfosPartenaireById(idPartenaire);
		if (partenaireConnected != null) {
		monAvis.setPartenaire(partenaireConnected);
		}
		if(notePonctualite != null ) {
		monAvis.setNotePonctualite(notePonctualite);
		}
		if(noteQualite != null) {
		monAvis.setNoteQualiteMassage(noteQualite);
		}
		if(noteRelationnelle != null) {
		monAvis.setNoteRelationnel(noteRelationnelle);
		}
	
		proxyRetourPartenaire.ajouterUnRetour(monAvis);
		return "/MesPrestationsPartenaire.xhtml?faces-redirect=true";
	}

	

//	/**
// * @return the typeAccount
// */
//public String getTypeAccount() {
//	return typeAccount;
//}
//
///**
// * @param typeAccount the typeAccount to set
// */
//public void setTypeAccount(String typeAccount) {
//	this.typeAccount = typeAccount;
//}


	/**
	 * @return the proxyPrestationBusiness
	 */
	public PrestationIBusiness getProxyPrestationBusiness() {
		return proxyPrestationBusiness;
	}

//	/**
//	 * @return the idPartenaire
//	 */
//	public Integer getIdPartenaire() {
//		return idPartenaire;
//	}
//
//	/**
//	 * @param idPartenaire the idPartenaire to set
//	 */
//	public void setIdPartenaire(Integer idPartenaire) {
//		this.idPartenaire = idPartenaire;
//	}

	/**
	 * @param proxyPrestationBusiness the proxyPrestationBusiness to set
	 */
	public void setProxyPrestationBusiness(PrestationIBusiness proxyPrestationBusiness) {
		this.proxyPrestationBusiness = proxyPrestationBusiness;
	}

	/**
	 * @return the partenaireConnected
	 */
	public Partenaire getPartenaireConnected() {
		return partenaireConnected;
	}

	/**
	 * @param partenaireConnected the partenaireConnected to set
	 */
	public void setPartenaireConnected(Partenaire partenaireConnected) {
		this.partenaireConnected = partenaireConnected;
	}

	/**
	 * @return the interventionReelle
	 */
	public InterventionReelle getInterventionReelle() {
		return interventionReelle;
	}

	/**
	 * @param interventionReelle the interventionReelle to set
	 */
	public void setInterventionReelle(InterventionReelle interventionReelle) {
		this.interventionReelle = interventionReelle;
	}

	/**
	 * @return the notePonctualite
	 */
	public Integer getNotePonctualite() {
		return notePonctualite;
	}

	/**
	 * @param notePonctualite the notePonctualite to set
	 */
	public void setNotePonctualite(Integer notePonctualite) {
		this.notePonctualite = notePonctualite;
	}

	/**
	 * @return the noteQualite
	 */
	public Integer getNoteQualite() {
		return noteQualite;
	}

	/**
	 * @param noteQualite the noteQualite to set
	 */
	public void setNoteQualite(Integer noteQualite) {
		this.noteQualite = noteQualite;
	}

	/**
	 * @return the noteRelationnelle
	 */
	public Integer getNoteRelationnelle() {
		return noteRelationnelle;
	}

	/**
	 * @param noteRelationnelle the noteRelationnelle to set
	 */
	public void setNoteRelationnelle(Integer noteRelationnelle) {
		this.noteRelationnelle = noteRelationnelle;
	}

	/**
	 * @return the commentaire
	 */
	public String getCommentaire() {
		return commentaire;
	}

	/**
	 * @param commentaire the commentaire to set
	 */
	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	/**
	 * @return the libellePrestation
	 */
	public String getLibellePrestation() {
		return libellePrestation;
	}

	/**
	 * @param libellePrestation the libellePrestation to set
	 */
	public void setLibellePrestation(String libellePrestation) {
		this.libellePrestation = libellePrestation;
	}

	/**
	 * @return the idIntervention
	 */
	public Integer getIdIntervention() {
		return idIntervention;
	}

	/**
	 * @param idIntervention the idIntervention to set
	 */
	public void setIdIntervention(Integer idIntervention) {
		this.idIntervention = idIntervention;
	}

	/**
	 * @return the monAvis
	 */
	public RetourExperiencePartenaire getMonAvis() {
		return monAvis;
	}

	/**
	 * @param monAvis the monAvis to set
	 */
	public void setMonAvis(RetourExperiencePartenaire monAvis) {
		this.monAvis = monAvis;
	}

	/**
	 * @return the monRetour
	 */
	public List<RetourExperiencePartenaire> getMonRetour() {
		return monRetour;
	}

	/**
	 * @param monRetour the monRetour to set
	 */
	public void setMonRetour(List<RetourExperiencePartenaire> monRetour) {
		this.monRetour = monRetour;
	}
	
	
	
	
	
}
