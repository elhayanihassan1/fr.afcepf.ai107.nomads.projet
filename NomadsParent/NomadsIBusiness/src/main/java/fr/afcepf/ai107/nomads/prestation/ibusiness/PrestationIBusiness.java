package fr.afcepf.ai107.nomads.prestation.ibusiness;

import java.util.List;

import fr.afcepf.ai107.nomads.entities.InscriptionMasseurs;
import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.Masseur;
import fr.afcepf.ai107.nomads.entities.Partenaire;
import fr.afcepf.ai107.nomads.entities.Prestation;
import fr.afcepf.ai107.nomads.entities.TypePaiement;
import fr.afcepf.ai107.nomads.entities.TypePartenaire;

public interface PrestationIBusiness {

	List<Prestation> getListePrestation();
	
	void ajoutPrestation(Prestation prestation);
	void AjoutIntervention(InterventionReelle interventionReelle);
	//Utiliser dans une drop down list pour la création de la prestation
	List<Partenaire> getPartenaireParType(Integer idTypePartenaire);
	//Utiliser dans une drop down list pour la création de la prestation

	List<TypePartenaire> getListeTypePartenaire();
	List<Masseur> getListeMasseurPourDDLCreationPrestation(Integer idTypePartenaire);
	List<Partenaire> getListePartenaire();
	List<Masseur> getListeMasseurs();
	List<TypePaiement> getListeTypePaiement();
	List<Prestation> getListeHistoriquePrestations();
	List<InterventionReelle> getListInterventionReelle();
	List<InterventionReelle> getListInterventionReellePassees();
	List<InterventionReelle> getListInterventionsNonPourvues();

//	List<String> getListeTypePartenaire();
	List<InterventionReelle> getAllInfosInterventionReelleByIdMasseur (Integer idMasseur);
	List<InterventionReelle> getAllInfosInterventionReelleAVenirByIdMasseur (Integer idMasseur);
	List<InterventionReelle> getAllInfosInterventionReelleAVenir3JByIdMasseur (Integer idMasseur);
	List<InterventionReelle> getAllInfosInterventionReellePasseesByIdMasseur (Integer idMasseur);
	
	List<InterventionReelle> getListeInterventionReelleDispoForMasseur(Integer idMasseur);
	
	List<InscriptionMasseurs> getAllInscriMasseurByIdIntervention(Integer idIntervention);
	

	InterventionReelle getInterventionById(Integer idIntervention);
	List<InterventionReelle> getAllInterventionFoNext7DAys ();
	List<InterventionReelle> getAllTodaySInterventions ();
	List<InterventionReelle> getListInterventionsNonPourvues7Days();


	Prestation validationPrestation(Prestation prestation);
	Prestation getPrestationById(int prestationId);
	List<Prestation> getListeDemandePrestation();
	List<InterventionReelle> getAllInterventionsByIdMasseurToday(Integer idMasseur);
	
	List<InterventionReelle> getAllInterventionFoNext3DAysByIdPart (Integer idPartenaire);
	List<InterventionReelle> getAllInterventionFoTodayByIdPart (Integer idPartenaire);

}
