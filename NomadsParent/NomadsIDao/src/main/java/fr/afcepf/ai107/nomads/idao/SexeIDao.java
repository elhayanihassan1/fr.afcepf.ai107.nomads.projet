package fr.afcepf.ai107.nomads.idao;

import java.util.List;

import fr.afcepf.ai107.nomads.entities.Sexe;

public interface SexeIDao extends GenericIDao<Sexe> {
	List<Sexe> getAll();

}
