package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "masseur")
public class MasseurOld implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_masseur")
	private int idMasseur;
	
	@Column(name = "nom")
	private String nom;
	
	@Column(name = "prenom")
	private String prenom;
	
	@Column(name = "numero_telephone")
	private String NumeroTelephone;
	
	@Column(name = "date_naissance")
	private Date DateNaissance;
	
	@Column(name = "adresse_masseur")
	private String Adresse;
	
	@Column(name = "date_inscription")
	private Date Inscription;
	
	@Column(name = "date_desinscription")
	private Date Desinscription;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "id_sexe")
	private Sexe sexe;
	
	@Column(name = "ville")
	private String Ville;
	
	@Column(name = "code_postal")
	private String CodePostal;
	
	@Column(name = "nombre_mads")
	private int NombreMads;
	
	@Column(name = "actif_masseur")
	private boolean Actif;
	
	@OneToMany(mappedBy = "masseur", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<InscriptionPrestationOld> inscriptionsPrestations;

	public MasseurOld() {
		super();
	}

	public MasseurOld(Integer idMasseur, String nom, String prenom, String numeroTelephone, Date dateNaissance,
			String adresse, Date inscription, Date desinscription, Sexe sexe, String ville, String codePostal,
			int nombreMads, boolean actif, Set<InscriptionPrestationOld> inscriptionsPrestations) {
		super();
		this.idMasseur = idMasseur;
		this.nom = nom;
		this.prenom = prenom;
		NumeroTelephone = numeroTelephone;
		DateNaissance = dateNaissance;
		Adresse = adresse;
		Inscription = inscription;
		Desinscription = desinscription;
		this.sexe = sexe;
		Ville = ville;
		CodePostal = codePostal;
		NombreMads = nombreMads;
		Actif = actif;
		this.inscriptionsPrestations = inscriptionsPrestations;
	}

	public Integer getIdMasseur() {
		return idMasseur;
	}

	public void setIdMasseur(Integer idMasseur) {
		this.idMasseur = idMasseur;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNumeroTelephone() {
		return NumeroTelephone;
	}

	public void setNumeroTelephone(String numeroTelephone) {
		NumeroTelephone = numeroTelephone;
	}

	public Date getDateNaissance() {
		return DateNaissance;
	}

	public void setDateNaissance(Date dateNaissance) {
		DateNaissance = dateNaissance;
	}

	public String getAdresse() {
		return Adresse;
	}

	public void setAdresse(String adresse) {
		Adresse = adresse;
	}

	public Date getInscription() {
		return Inscription;
	}

	public void setInscription(Date inscription) {
		Inscription = inscription;
	}

	public Date getDesinscription() {
		return Desinscription;
	}

	public void setDesinscription(Date desinscription) {
		Desinscription = desinscription;
	}

	public Sexe getSexe() {
		return sexe;
	}

	public void setSexe(Sexe sexe) {
		this.sexe = sexe;
	}

	public String getVille() {
		return Ville;
	}

	public void setVille(String ville) {
		Ville = ville;
	}

	public String getCodePostal() {
		return CodePostal;
	}

	public void setCodePostal(String codePostal) {
		CodePostal = codePostal;
	}

	public int getNombreMads() {
		return NombreMads;
	}

	public void setNombreMads(int nombreMads) {
		NombreMads = nombreMads;
	}

	public boolean isActif() {
		return Actif;
	}

	public void setActif(boolean actif) {
		Actif = actif;
	}

	public Set<InscriptionPrestationOld> getInscriptionsPrestations() {
		return inscriptionsPrestations;
	}

	public void setInscriptionsPrestations(Set<InscriptionPrestationOld> inscriptionsPrestations) {
		this.inscriptionsPrestations = inscriptionsPrestations;
	}

}
