package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import javax.persistence.Table;

@Entity
@Table(name = "masseur")
public class Masseur implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_masseur")
	private Integer id_masseur;

	@Column(name = "nom_masseur")
	private String nomMasseur;

	@Column(name = "prenom_masseur")
	private String prenomMasseur;

	@Column(name = "date_de_naissance_masseur")
	private Date dateDeNaissanceMasseur;

	@Column(name = "adresse_mail_masseur")
	private String adresseMailMasseur;

	@Column(name = "telephone_masseur")
	private String telephoneMasseur;

	@Column(name = "adresse_postale_masseur")
	private String adressePostaleMasseur;

	@Column(name = "date_premier_enregistrement_masseur")
	private Date datePremierEnregistrementMasseur;

	@Column(name = "date_depart_definitif_masseur")
	private Date dateDepartDefinitifMasseur;

	@Column(name = "login_masseur")
	private String loginMasseur;

	@Column(name = "password_masseur")
	private String passwordMasseur;

	@Column(name = "photo_masseur")
	private String photoMasseur;
	
	@ManyToOne
	@JoinColumn(name = "id_sexe", referencedColumnName = "id_sexe")
	private Sexe sexe;

	@OneToMany(mappedBy = "masseur", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private List<AffectationCategorieMasseur> affectactionsCategorieMasseur;
	
	@OneToMany (mappedBy = "masseur", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private List<HistoriqueEvolutionProfessionnelleMasseur> evolutionsProfessionnelles;
	
	@OneToMany (mappedBy ="masseur", cascade = CascadeType.PERSIST,fetch = FetchType.LAZY )
	private List<ContratMasseur> contratsMasseur;
	
	@OneToMany (mappedBy ="masseur", cascade = CascadeType.PERSIST,fetch = FetchType.LAZY )
	private List<IndisponibiliteMasseur> indisponibilitesMasseur;
	
	@OneToMany (mappedBy ="masseur", cascade = CascadeType.PERSIST,fetch = FetchType.LAZY )
	private List<Transaction> transactions;
	
	@ManyToOne
	@JoinColumn(name = "id_ville_cp", referencedColumnName = "id_ville_cp")
	private VilleCp villeCp;
	
	@OneToMany (mappedBy ="masseur", cascade = CascadeType.PERSIST,fetch = FetchType.LAZY )
	private List<LocationMateriel> locationsMateriel;
	
	@OneToMany (mappedBy ="masseur", cascade = CascadeType.ALL,fetch = FetchType.EAGER )
	private List<InscriptionMasseurs> inscriptionsMasseurs ;
	
	@OneToMany (mappedBy ="masseur", cascade = CascadeType.PERSIST,fetch = FetchType.LAZY )
	private List<RetourExperienceMasseur> retoursExperienceMasseurs;
	
	@OneToMany (mappedBy ="masseur", cascade = CascadeType.PERSIST,fetch = FetchType.LAZY )
	private List<InscriptionSessionFormation> inscriptionsSessionFormations;

	

	/**
	 * 
	 */
	public Masseur() {
		super();
	}

	/**
	 * @param id_masseur
	 * @param nomMasseur
	 * @param prenomMasseur
	 * @param dateDeNaissanceMasseur
	 * @param adresseMailMasseur
	 * @param telephoneMasseur
	 * @param adressePostaleMasseur
	 * @param datePremierEnregistrementMasseur
	 * @param dateDepartDefinitifMasseur
	 * @param loginMasseur
	 * @param passwordMasseur
	 * @param photoMasseur
	 * @param sexe
	 * @param affectactionsCategorieMasseur
	 * @param evolutionsProfessionnelles
	 * @param contratsMasseur
	 * @param indisponibilitesMasseur
	 * @param transactions
	 * @param villeCp
	 * @param locationsMateriel
	 * @param inscriptionsMasseurs
	 * @param retoursExperienceMasseurs
	 * @param inscriptionsSessionFormations
	 */
	public Masseur(Integer id_masseur, String nomMasseur, String prenomMasseur, Date dateDeNaissanceMasseur,
			String adresseMailMasseur, String telephoneMasseur, String adressePostaleMasseur,
			Date datePremierEnregistrementMasseur, Date dateDepartDefinitifMasseur, String loginMasseur,
			String passwordMasseur, String photoMasseur, Sexe sexe,
			List<AffectationCategorieMasseur> affectactionsCategorieMasseur,
			List<HistoriqueEvolutionProfessionnelleMasseur> evolutionsProfessionnelles,
			List<ContratMasseur> contratsMasseur, List<IndisponibiliteMasseur> indisponibilitesMasseur,
			List<Transaction> transactions, VilleCp villeCp, List<LocationMateriel> locationsMateriel,
			List<InscriptionMasseurs> inscriptionsMasseurs, List<RetourExperienceMasseur> retoursExperienceMasseurs,
			List<InscriptionSessionFormation> inscriptionsSessionFormations) {
		super();
		this.id_masseur = id_masseur;
		this.nomMasseur = nomMasseur;
		this.prenomMasseur = prenomMasseur;
		this.dateDeNaissanceMasseur = dateDeNaissanceMasseur;
		this.adresseMailMasseur = adresseMailMasseur;
		this.telephoneMasseur = telephoneMasseur;
		this.adressePostaleMasseur = adressePostaleMasseur;
		this.datePremierEnregistrementMasseur = datePremierEnregistrementMasseur;
		this.dateDepartDefinitifMasseur = dateDepartDefinitifMasseur;
		this.loginMasseur = loginMasseur;
		this.passwordMasseur = passwordMasseur;
		this.photoMasseur = photoMasseur;
		this.sexe = sexe;
		this.affectactionsCategorieMasseur = affectactionsCategorieMasseur;
		this.evolutionsProfessionnelles = evolutionsProfessionnelles;
		this.contratsMasseur = contratsMasseur;
		this.indisponibilitesMasseur = indisponibilitesMasseur;
		this.transactions = transactions;
		this.villeCp = villeCp;
		this.locationsMateriel = locationsMateriel;
		this.inscriptionsMasseurs = inscriptionsMasseurs;
		this.retoursExperienceMasseurs = retoursExperienceMasseurs;
		this.inscriptionsSessionFormations = inscriptionsSessionFormations;
	}

	/**
	 * @return the id_masseur
	 */
	public Integer getId_masseur() {
		return id_masseur;
	}

	/**
	 * @param id_masseur the id_masseur to set
	 */
	public void setId_masseur(Integer id_masseur) {
		this.id_masseur = id_masseur;
	}

	/**
	 * @return the nomMasseur
	 */
	public String getNomMasseur() {
		return nomMasseur;
	}

	/**
	 * @param nomMasseur the nomMasseur to set
	 */
	public void setNomMasseur(String nomMasseur) {
		this.nomMasseur = nomMasseur;
	}

	/**
	 * @return the prenomMasseur
	 */
	public String getPrenomMasseur() {
		return prenomMasseur;
	}

	/**
	 * @param prenomMasseur the prenomMasseur to set
	 */
	public void setPrenomMasseur(String prenomMasseur) {
		this.prenomMasseur = prenomMasseur;
	}

	/**
	 * @return the dateDeNaissanceMasseur
	 */
	public Date getDateDeNaissanceMasseur() {
		return dateDeNaissanceMasseur;
	}

	/**
	 * @param dateDeNaissanceMasseur the dateDeNaissanceMasseur to set
	 */
	public void setDateDeNaissanceMasseur(Date dateDeNaissanceMasseur) {
		this.dateDeNaissanceMasseur = dateDeNaissanceMasseur;
	}

	/**
	 * @return the adresseMailMasseur
	 */
	public String getAdresseMailMasseur() {
		return adresseMailMasseur;
	}

	/**
	 * @param adresseMailMasseur the adresseMailMasseur to set
	 */
	public void setAdresseMailMasseur(String adresseMailMasseur) {
		this.adresseMailMasseur = adresseMailMasseur;
	}

	/**
	 * @return the telephoneMasseur
	 */
	public String getTelephoneMasseur() {
		return telephoneMasseur;
	}

	/**
	 * @param telephoneMasseur the telephoneMasseur to set
	 */
	public void setTelephoneMasseur(String telephoneMasseur) {
		this.telephoneMasseur = telephoneMasseur;
	}

	/**
	 * @return the adressePostaleMasseur
	 */
	public String getAdressePostaleMasseur() {
		return adressePostaleMasseur;
	}

	/**
	 * @param adressePostaleMasseur the adressePostaleMasseur to set
	 */
	public void setAdressePostaleMasseur(String adressePostaleMasseur) {
		this.adressePostaleMasseur = adressePostaleMasseur;
	}

	/**
	 * @return the datePremierEnregistrementMasseur
	 */
	public Date getDatePremierEnregistrementMasseur() {
		return datePremierEnregistrementMasseur;
	}

	/**
	 * @param datePremierEnregistrementMasseur the datePremierEnregistrementMasseur to set
	 */
	public void setDatePremierEnregistrementMasseur(Date datePremierEnregistrementMasseur) {
		this.datePremierEnregistrementMasseur = datePremierEnregistrementMasseur;
	}

	/**
	 * @return the dateDepartDefinitifMasseur
	 */
	public Date getDateDepartDefinitifMasseur() {
		return dateDepartDefinitifMasseur;
	}

	/**
	 * @param dateDepartDefinitifMasseur the dateDepartDefinitifMasseur to set
	 */
	public void setDateDepartDefinitifMasseur(Date dateDepartDefinitifMasseur) {
		this.dateDepartDefinitifMasseur = dateDepartDefinitifMasseur;
	}

	/**
	 * @return the loginMasseur
	 */
	public String getLoginMasseur() {
		return loginMasseur;
	}

	/**
	 * @param loginMasseur the loginMasseur to set
	 */
	public void setLoginMasseur(String loginMasseur) {
		this.loginMasseur = loginMasseur;
	}

	/**
	 * @return the passwordMasseur
	 */
	public String getPasswordMasseur() {
		return passwordMasseur;
	}

	/**
	 * @param passwordMasseur the passwordMasseur to set
	 */
	public void setPasswordMasseur(String passwordMasseur) {
		this.passwordMasseur = passwordMasseur;
	}

	/**
	 * @return the photoMasseur
	 */
	public String getPhotoMasseur() {
		return photoMasseur;
	}

	/**
	 * @param photoMasseur the photoMasseur to set
	 */
	public void setPhotoMasseur(String photoMasseur) {
		this.photoMasseur = photoMasseur;
	}

	/**
	 * @return the sexe
	 */
	public Sexe getSexe() {
		return sexe;
	}

	/**
	 * @param sexe the sexe to set
	 */
	public void setSexe(Sexe sexe) {
		this.sexe = sexe;
	}

	/**
	 * @return the affectactionsCategorieMasseur
	 */
	public List<AffectationCategorieMasseur> getAffectactionsCategorieMasseur() {
		return affectactionsCategorieMasseur;
	}

	/**
	 * @param affectactionsCategorieMasseur the affectactionsCategorieMasseur to set
	 */
	public void setAffectactionsCategorieMasseur(List<AffectationCategorieMasseur> affectactionsCategorieMasseur) {
		this.affectactionsCategorieMasseur = affectactionsCategorieMasseur;
	}

	/**
	 * @return the evolutionsProfessionnelles
	 */
	public List<HistoriqueEvolutionProfessionnelleMasseur> getEvolutionsProfessionnelles() {
		return evolutionsProfessionnelles;
	}

	/**
	 * @param evolutionsProfessionnelles the evolutionsProfessionnelles to set
	 */
	public void setEvolutionsProfessionnelles(List<HistoriqueEvolutionProfessionnelleMasseur> evolutionsProfessionnelles) {
		this.evolutionsProfessionnelles = evolutionsProfessionnelles;
	}

	/**
	 * @return the contratsMasseur
	 */
	public List<ContratMasseur> getContratsMasseur() {
		return contratsMasseur;
	}

	/**
	 * @param contratsMasseur the contratsMasseur to set
	 */
	public void setContratsMasseur(List<ContratMasseur> contratsMasseur) {
		this.contratsMasseur = contratsMasseur;
	}

	/**
	 * @return the indisponibilitesMasseur
	 */
	public List<IndisponibiliteMasseur> getIndisponibilitesMasseur() {
		return indisponibilitesMasseur;
	}

	/**
	 * @param indisponibilitesMasseur the indisponibilitesMasseur to set
	 */
	public void setIndisponibilitesMasseur(List<IndisponibiliteMasseur> indisponibilitesMasseur) {
		this.indisponibilitesMasseur = indisponibilitesMasseur;
	}

	/**
	 * @return the transactions
	 */
	public List<Transaction> getTransactions() {
		return transactions;
	}

	/**
	 * @param transactions the transactions to set
	 */
	public void setTransactions(List<Transaction> transactions) {
		this.transactions = transactions;
	}

	/**
	 * @return the villeCp
	 */
	public VilleCp getVilleCp() {
		return villeCp;
	}

	/**
	 * @param villeCp the villeCp to set
	 */
	public void setVilleCp(VilleCp villeCp) {
		this.villeCp = villeCp;
	}

	/**
	 * @return the locationsMateriel
	 */
	public List<LocationMateriel> getLocationsMateriel() {
		return locationsMateriel;
	}

	/**
	 * @param locationsMateriel the locationsMateriel to set
	 */
	public void setLocationsMateriel(List<LocationMateriel> locationsMateriel) {
		this.locationsMateriel = locationsMateriel;
	}

	/**
	 * @return the inscriptionsMasseurs
	 */
	public List<InscriptionMasseurs> getInscriptionsMasseurs() {
		return inscriptionsMasseurs;
	}

	/**
	 * @param inscriptionsMasseurs the inscriptionsMasseurs to set
	 */
	public void setInscriptionsMasseurs(List<InscriptionMasseurs> inscriptionsMasseurs) {
		this.inscriptionsMasseurs = inscriptionsMasseurs;
	}

	/**
	 * @return the retoursExperienceMasseurs
	 */
	public List<RetourExperienceMasseur> getRetoursExperienceMasseurs() {
		return retoursExperienceMasseurs;
	}

	/**
	 * @param retoursExperienceMasseurs the retoursExperienceMasseurs to set
	 */
	public void setRetoursExperienceMasseurs(List<RetourExperienceMasseur> retoursExperienceMasseurs) {
		this.retoursExperienceMasseurs = retoursExperienceMasseurs;
	}

	/**
	 * @return the inscriptionsSessionFormations
	 */
	public List<InscriptionSessionFormation> getInscriptionsSessionFormations() {
		return inscriptionsSessionFormations;
	}

	/**
	 * @param inscriptionsSessionFormations the inscriptionsSessionFormations to set
	 */
	public void setInscriptionsSessionFormations(List<InscriptionSessionFormation> inscriptionsSessionFormations) {
		this.inscriptionsSessionFormations = inscriptionsSessionFormations;
	}


	@Override
	public String toString() {
		return "Masseur [id_masseur=" + id_masseur + ", nomMasseur=" + nomMasseur + ", prenomMasseur=" + prenomMasseur
				+ ", dateDeNaissanceMasseur=" + dateDeNaissanceMasseur + ", adresseMailMasseur=" + adresseMailMasseur
				+ ", telephoneMasseur=" + telephoneMasseur + ", adressePostaleMasseur=" + adressePostaleMasseur
				+ ", datePremierEnregistrementMasseur=" + datePremierEnregistrementMasseur
				+ ", dateDepartDefinitifMasseur=" + dateDepartDefinitifMasseur + ", loginMasseur=" + loginMasseur
				+ ", passwordMasseur=" + passwordMasseur + ", photoMasseur=" + photoMasseur + 
			 "]";
	}

	
}