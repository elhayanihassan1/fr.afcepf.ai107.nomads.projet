package fr.afcepf.ai107.nomads.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.nomads.entities.TypePartenaire;
import fr.afcepf.ai107.nomads.idao.TypePartenaireIDao;



@Remote(TypePartenaireIDao.class)
@Stateless
public class TypePartenaireDao extends GenericDao<TypePartenaire> implements TypePartenaireIDao{

	@PersistenceContext(unitName = "NomadsPU")
	private EntityManager em;
	
	private String ReqGetAll = "SELECT tp FROM TypePartenaire tp";
	private String REQ_TYPE_PART= "SELECT p FROM TypePartenaire p INNER JOIN FETCH p.partenaire a INNER JOIN FETCH a.prestation e INNER JOIN FETCH e.interventionReelle i WHERE i.idInterventionReelle :paramId";
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TypePartenaire> getAll() {
		List<TypePartenaire> typePartenaire = null;
		
		Query requete = em.createQuery(ReqGetAll);
		typePartenaire = requete.getResultList();
		
		return typePartenaire;
	}
		
		@SuppressWarnings("unchecked")
		@Override
		public TypePartenaire getById(int id) {
			Query query = em.createQuery("Select tp from TypePartenaire tp where tp.idTypePartenaire = :paramId");
			query.setParameter("paramId", id);
			List<TypePartenaire> typePartenaire = query.getResultList();
			return typePartenaire.get(0);
		}

		@Override
		public TypePartenaire getTypePartenaireByIdIntervention(Integer idInterv) {
			// TODO Auto-generated method stub
			TypePartenaire typePart = null;
			Query query = em.createQuery(REQ_TYPE_PART);
			query.setParameter("paramId", idInterv);
			typePart = (TypePartenaire) query.getSingleResult();
			return typePart;
		}
	}

