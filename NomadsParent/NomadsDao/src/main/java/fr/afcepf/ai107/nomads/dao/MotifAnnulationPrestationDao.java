package fr.afcepf.ai107.nomads.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.MotifAnnulationPrestation;
import fr.afcepf.ai107.nomads.idao.MotifAnnulationPrestationIDao;

@Remote(MotifAnnulationPrestationIDao.class)
@Stateless
public class MotifAnnulationPrestationDao extends GenericDao<MotifAnnulationPrestation> implements MotifAnnulationPrestationIDao {

	@PersistenceContext (unitName = "NomadsPU")
	private EntityManager em;
	
	private final String REQ_GET_ALL_MOTIFS = "SELECT m.libelleMotifAnnulationPrestation FROM MotifAnnulationPrestation m";
	private final String REQ_GET_MOTIF_BY_LIBELLE ="SELECT m FROM MotifAnnulationPrestation m.libelleMotifAnnulationPrestation = :ParamT";

	
	@Override
	public List<String> getAllMotifAnnulation() {
		// TODO Auto-generated method stub
		List <String> mesMotifs = null;
		Query query = em.createQuery(REQ_GET_ALL_MOTIFS);
		mesMotifs = query.getResultList();
		return mesMotifs;
	}

	@Override
	public InterventionReelle updateInterventionAvecMotifAnnulation(InterventionReelle interv) {
		// TODO Auto-generated method stub
		em.merge(interv);
		return interv;
	}

	@Override
	public MotifAnnulationPrestation getMotifByLibelle(String motif) {
		// TODO Auto-generated method stub
		MotifAnnulationPrestation myMotif = null;
		Query query = em.createQuery(REQ_GET_MOTIF_BY_LIBELLE);
		query.setParameter("paramT", motif);
		myMotif = (MotifAnnulationPrestation) query.getSingleResult();
		return myMotif;
	}


}
