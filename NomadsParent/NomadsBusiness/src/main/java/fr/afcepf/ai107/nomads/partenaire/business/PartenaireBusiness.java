package fr.afcepf.ai107.nomads.partenaire.business;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.mail.Part;
import javax.persistence.Query;

import fr.afcepf.ai107.nomads.entities.ContratPartenaire;
import fr.afcepf.ai107.nomads.entities.IndisponibilitePartenaire;
import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.MotifFinContratPartenaire;
import fr.afcepf.ai107.nomads.entities.MotifIndisponibilitePartenaire;
import fr.afcepf.ai107.nomads.entities.Partenaire;
import fr.afcepf.ai107.nomads.entities.Prestation;
import fr.afcepf.ai107.nomads.entities.TypePartenaire;
import fr.afcepf.ai107.nomads.idao.PartenaireIDao;
import fr.afcepf.ai107.nomads.idao.TypePartenaireIDao;
import fr.afcepf.ai107.nomads.partenaire.ibusiness.PartenaireIBusiness;




@Remote(PartenaireIBusiness.class)
@Stateless
public class PartenaireBusiness  implements PartenaireIBusiness{

	
	@EJB
	private PartenaireIDao proxyPartenaireDAO;

	
	@EJB
	private TypePartenaireIDao proxyTypePartenaireDAO;
	
	@Override
	public Partenaire connection(String login, String password) {
	return proxyPartenaireDAO.authenticate(login, password);
	}

	@Override
	public Partenaire ajouterPartenaire(Partenaire partenaire) {
		return proxyPartenaireDAO.add(partenaire);
		
	}

	@Override
	public List<TypePartenaire> getTypePartenaire() {
	
		List<TypePartenaire> typePartenaire = null;
		typePartenaire = proxyTypePartenaireDAO.getAll();
		return typePartenaire;
	}

	@Override
	public TypePartenaire getTypePartenaireById(int id) {
		
		return proxyTypePartenaireDAO.getById(id);
	}
	
	@Override
	public List<Partenaire> getAllPartenaires() {
		List<Partenaire> partenaires = null;
		partenaires = proxyPartenaireDAO.getAllPartenaires();
		return partenaires;
	}
	public List<Partenaire> getAllPartenaireInactifs(){
		return proxyPartenaireDAO.getAllPartenaireInactifs();
		
	}
	
	public List<Partenaire> getAllPartenaireActifs(){
		List<Partenaire> liste1 = proxyPartenaireDAO.getAllPartenaireActifs1();
		List<Partenaire> liste2 = proxyPartenaireDAO.getAllPartenaireActifs2();
		List<Partenaire> liste = liste1;
		liste.addAll(liste2);
		return liste;
	}
	
	public List<Partenaire> getAllPartenaireTypePart(String typePartenaire) {
		return proxyPartenaireDAO.getAllPartenaireTypePart(typePartenaire);
	}
	

	@Override
	public Partenaire modifierPartenaire(Partenaire partenaireRecupere) {
		return proxyPartenaireDAO.updatePartenaire(partenaireRecupere);
	}

	@Override
	public ContratPartenaire desactiverPartenaire(ContratPartenaire contratPartenaire) {
		return proxyPartenaireDAO.updateContratPartenaire(contratPartenaire);
	}

	@Override
	public Partenaire getInfosPartenaireById(int partenaireid) {
		return proxyPartenaireDAO.getInfosPartenaireById(partenaireid);
	}

	@Override
	public ContratPartenaire ajouterContratPartenaire(ContratPartenaire nouveauContrat) {
		return proxyPartenaireDAO.ajouterContratPartenaire(nouveauContrat);
	}
	
	public ContratPartenaire getContratPartenaireById (Partenaire partenaire) {
		return proxyPartenaireDAO.getContratPartenaireById(partenaire);
	}

	public IndisponibilitePartenaire ajouterDisponibilitePartenaire (IndisponibilitePartenaire indisponibilite, Date debut, Date fin) {
		return indisponibilite;
	}
	
	public List<String>  getAllMotifsFinContratPartenaire(){
		return proxyPartenaireDAO.getAllMotifsFinContratPartenaire();
	}
	
	public MotifFinContratPartenaire getMotifByLibelleMotif(String motif) {
		return proxyPartenaireDAO.getMotifByLibelleMotif(motif);
	}
	
	public IndisponibilitePartenaire updateIndispoPart(IndisponibilitePartenaire indispoPart) {
		return proxyPartenaireDAO.updateIndispoPart(indispoPart);
	}
	
	public IndisponibilitePartenaire getIndispoEnCoursPart(Partenaire partenaire) throws Exception{
		IndisponibilitePartenaire indispoEnCours=null;
		try {
			indispoEnCours = proxyPartenaireDAO.getIndispoEnCoursPart(partenaire);
		} catch (Exception e) {
			throw e;
		}
		return indispoEnCours;		
	}

	@Override
	public IndisponibilitePartenaire ajouterIndispoPart(IndisponibilitePartenaire indispoPart) {
		return proxyPartenaireDAO.ajouterIndispoPart(indispoPart);
		
	}

	@Override
	public List<Prestation> getListePrestaAVenirByIdPartenaire(int partenaireId) {
		return proxyPartenaireDAO.getListePrestaAVenirByIdPartenaire(partenaireId);
	}

	@Override
	public List<Prestation> getListePrestaPasseByIdPartenaire(int partenaireId) {
		return proxyPartenaireDAO.getListePrestaPasseByIdPartenaire(partenaireId);
	}
	
	public boolean verifierDisponiblePartenaire(Partenaire partenaire) {
		boolean verifEstDisponible=true;
		List<Partenaire> partenaireIndisponibles = proxyPartenaireDAO.getAllPartenaireInactifs();
		for (Partenaire partenaireIndispo : partenaireIndisponibles) {
			if(partenaireIndispo.getIdPartenaire()==partenaire.getIdPartenaire()) {
				verifEstDisponible=false;
			}else{
				verifEstDisponible=true;
			}
		}
		return verifEstDisponible;
	}
	public List<IndisponibilitePartenaire> getHistoriqueIndispoByPart(Partenaire partenaire) throws Exception{
		List<IndisponibilitePartenaire> indispo=null;
				try {
					indispo = proxyPartenaireDAO.getHistoriqueIndispoByPart(partenaire);
				} catch (Exception e) {
					throw e;
				}
		return indispo;
	}
	
	public List<String> getAllMotifsIndispoPartenaire(){
		return proxyPartenaireDAO.getAllMotifsIndispoPartenaire();
	}
	
	public MotifIndisponibilitePartenaire getMotifIndispoByLibelleMotifIndispo(String motif) {
		return proxyPartenaireDAO.getMotifIndispoByLibelleMotifIndispo(motif);
	}

	@Override
	public List<InterventionReelle> getAllInterventionAvenirByPArtenaireBu(int partenaireId) {
		// TODO Auto-generated method stub
		return proxyPartenaireDAO.getAllInterventionAvenirByPArtenaire(partenaireId);
	}

	@Override
	public List<InterventionReelle> getAllInterventionPasseesByPArtenaireBu(int partenaireId) {
		// TODO Auto-generated method stub
		return proxyPartenaireDAO.getAllInterventionPasseesByPArtenaire(partenaireId);
	}

	@Override
	public TypePartenaire getTypePartenaireByIdIntervention(Integer idInterv) {
		// TODO Auto-generated method stub
		return proxyTypePartenaireDAO.getTypePartenaireByIdIntervention(idInterv);
	}

	public List<Partenaire> getPartenairesActifsAjD() {
		List<Partenaire> partenaires = null;
		partenaires = proxyPartenaireDAO.getPartenairesActifsAjD();
		return partenaires;
	}

	@Override
	public List<Partenaire> getNouveauxPartenaires1Month() {
		List<Partenaire> partenaires = null;
		partenaires = proxyPartenaireDAO.getNouveauxPartenaires1Mois();
		return partenaires;
	}
	
	@Override
	public List<Partenaire> getDepartPartenaires1Mois() {
		List<Partenaire> partenaires = null;
		partenaires = proxyPartenaireDAO.getDepartPartenaires1Mois();
		return partenaires;

	}
}

