package fr.afcepf.ai107.nomads.controller.mads;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import fr.afcepf.ai107.nomads.entities.TypePackMads;
import fr.afcepf.ai107.nomads.outils.ibusiness.TypePackMadsIBusiness;

@ManagedBean (name="mbMads")
@ViewScoped
public class TypePackMadsManageBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int idTypePack;
	private String libelleTypePackMads="";
	private Double prixTypePackMads = null;
	private Integer valeurTypePackMads = null;
	private Boolean removed = false;
	private TypePackMads selectedPack;
	private TypePackMads newPack = new TypePackMads();
	private TypePackMads packMads;
	private List<TypePackMads> allPackMads = new ArrayList<TypePackMads>();
	private List<TypePackMads> allActivePacks = new ArrayList<TypePackMads>();
	private List<TypePackMads> allNonActivePacks = new ArrayList<TypePackMads>();

	
	@EJB
	private TypePackMadsIBusiness proxyTypePackMads;
	
	
	@PostConstruct
	public void init() {
	allActivePacks = proxyTypePackMads.allNonRemovedPackMads();
	}
	
	public void deletePackMads() {
		selectedPack.setRemovedTypePackMads(!removed);
		proxyTypePackMads.udpate(selectedPack);
		setAllActivePacks(proxyTypePackMads.allNonRemovedPackMads());
	}
	
	public void addPackMads() {
		newPack.setRemovedTypePackMads(removed) ;
		proxyTypePackMads.add(newPack);
		setAllActivePacks(proxyTypePackMads.allNonRemovedPackMads());
	}
	
	public void updatePackMads() {
		proxyTypePackMads.udpate(selectedPack);
		}
	
	public void oldPackMadsView() {
		setAllActivePacks(proxyTypePackMads.allRemovedPackMads());
		}
	public void allActivePacks() {
		setAllActivePacks(proxyTypePackMads.allNonRemovedPackMads());
		}

	/**
	 * @return the idTypePack
	 */
	public int getIdTypePack() {
		return idTypePack;
	}

	/**
	 * @param idTypePack the idTypePack to set
	 */
	public void setIdTypePack(int idTypePack) {
		this.idTypePack = idTypePack;
	}

	/**
	 * @return the libelleTypePackMads
	 */
	public String getLibelleTypePackMads() {
		return libelleTypePackMads;
	}

	/**
	 * @param libelleTypePackMads the libelleTypePackMads to set
	 */
	public void setLibelleTypePackMads(String libelleTypePackMads) {
		this.libelleTypePackMads = libelleTypePackMads;
	}

	/**
	 * @return the prixTypePackMads
	 */
	public Double getPrixTypePackMads() {
		return prixTypePackMads;
	}

	/**
	 * @param prixTypePackMads the prixTypePackMads to set
	 */
	public void setPrixTypePackMads(Double prixTypePackMads) {
		this.prixTypePackMads = prixTypePackMads;
	}

	/**
	 * @return the valeurTypePackMads
	 */
	public Integer getValeurTypePackMads() {
		return valeurTypePackMads;
	}

	/**
	 * @param valeurTypePackMads the valeurTypePackMads to set
	 */
	public void setValeurTypePackMads(Integer valeurTypePackMads) {
		this.valeurTypePackMads = valeurTypePackMads;
	}

	/**
	 * @return the removed
	 */
	public Boolean getRemoved() {
		return removed;
	}

	/**
	 * @param removed the removed to set
	 */
	public void setRemoved(Boolean removed) {
		this.removed = removed;
	}

	/**
	 * @return the selectedPack
	 */
	public TypePackMads getSelectedPack() {
		return selectedPack;
	}

	/**
	 * @param selectedPack the selectedPack to set
	 */
	public void setSelectedPack(TypePackMads selectedPack) {
		this.selectedPack = selectedPack;
	}

	/**
	 * @return the newPack
	 */
	public TypePackMads getNewPack() {
		return newPack;
	}

	/**
	 * @param newPack the newPack to set
	 */
	public void setNewPack(TypePackMads newPack) {
		this.newPack = newPack;
	}

	/**
	 * @return the packMads
	 */
	public TypePackMads getPackMads() {
		return packMads;
	}

	/**
	 * @param packMads the packMads to set
	 */
	public void setPackMads(TypePackMads packMads) {
		this.packMads = packMads;
	}

	/**
	 * @return the allPackMads
	 */
	public List<TypePackMads> getAllPackMads() {
		return allPackMads;
	}

	/**
	 * @param allPackMads the allPackMads to set
	 */
	public void setAllPackMads(List<TypePackMads> allPackMads) {
		this.allPackMads = allPackMads;
	}

	/**
	 * @return the allActivePacks
	 */
	public List<TypePackMads> getAllActivePacks() {
		return allActivePacks;
	}

	/**
	 * @param allActivePacks the allActivePacks to set
	 */
	public void setAllActivePacks(List<TypePackMads> allActivePacks) {
		this.allActivePacks = allActivePacks;
	}

	/**
	 * @return the allNonActivePacks
	 */
	public List<TypePackMads> getAllNonActivePacks() {
		return allNonActivePacks;
	}

	/**
	 * @param allNonActivePacks the allNonActivePacks to set
	 */
	public void setAllNonActivePacks(List<TypePackMads> allNonActivePacks) {
		this.allNonActivePacks = allNonActivePacks;
	}

}
