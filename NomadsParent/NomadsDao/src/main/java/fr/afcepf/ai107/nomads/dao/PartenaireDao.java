package fr.afcepf.ai107.nomads.dao;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.nomads.entities.ContratPartenaire;
import fr.afcepf.ai107.nomads.entities.IndisponibilitePartenaire;
import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.MotifFinContratPartenaire;
import fr.afcepf.ai107.nomads.entities.MotifIndisponibilitePartenaire;
import fr.afcepf.ai107.nomads.entities.Partenaire;
import fr.afcepf.ai107.nomads.entities.Prestation;
import fr.afcepf.ai107.nomads.idao.PartenaireIDao;


@Remote(PartenaireIDao.class)
@Stateless
public class PartenaireDao  extends GenericDao<Partenaire> implements PartenaireIDao{
	static final String REQ_AUTHEN_PART = "SELECT p FROM Partenaire p WHERE p.loginPartenaire = :paramLogin AND p.passwordPartenaire = :paramPassword";
	static final String REQ_GETALL_PART = "SELECT p FROM Partenaire p";
	static final String REQ_ALL_PART_INACTIFS = "SELECT distinct p FROM Partenaire p INNER JOIN FETCH p.indisponibilitePartenaire ind WHERE ind.dateFinIndisponibilitePartenaire IS NULL";
	static final String REQ_ALL_PART_ACTIFS1 = "SELECT distinct p FROM Partenaire p INNER JOIN FETCH p.indisponibilitePartenaire ind WHERE ind.dateFinIndisponibilitePartenaire IS NOT NULL";
	static final String REQ_ALL_PART_ACTIFS2 = "SELECT distinct p FROM Partenaire p WHERE p.idPartenaire NOT IN (SELECT ip.partenaire.idPartenaire FROM IndisponibilitePartenaire ip)";
	static final String REQ_ALL_PART_ACTIFS3 = "SSELECT p FROM Partenaire p, ContratPartenaire cp  WHERE p.idPartenaire = cp.idContratPartenaire AND cp.dateFinContratPartenaire IS NULL";
	static final String REQ_GETBYID_PART = "SELECT p FROM Partenaire p WHERE p.idPartenaire = :paramIdPartenaire";
	static final String REQ_CONTRAT_PART_ID = "SELECT cp FROM ContratPartenaire cp WHERE cp.partenaire.idPartenaire = :paramIdPartenaire AND cp.dateFinContratPartenaire IS NULL";
	static final String REQ_LISTE_MOTIF_FIN_CONTRAT = "SELECT m.libelleMotifFinContratPartenaire FROM MotifFinContratPartenaire m";
	static final String REQ_MOTIF_BY_MOTIF = "SELECT m FROM MotifFinContratPartenaire m WHERE m.libelleMotifFinContratPartenaire = :paramLibeleMotif";
	static final String REQ_DERNIERE_INDISPO_PART = "SELECT i FROM IndisponibilitePartenaire i INNER JOIN FETCH i.partenaire p WHERE p.idPartenaire = :paramIdPartenaire AND i.dateFinIndisponibilitePartenaire IS NULL";
	static final String REQ_GET_PRESTA_FUTUR_BYID_PART = "SELECT ps FROM Prestation ps INNER JOIN ps.partenaire p  INNER JOIN FETCH ps.interventionReelle i WHERE p.idPartenaire = :paramIdPartenaire AND i.datePrestation >= CURRENT_DATE";
	static final String REQ_GET_PRESTA_PAST_BYID_PART = "SELECT ps FROM Prestation ps INNER JOIN ps.partenaire p  INNER JOIN FETCH ps.interventionReelle i WHERE p.idPartenaire = :paramIdPartenaire AND i.datePrestation < CURRENT_DATE";
	static final String REQ_HISTO_INDISPO_PART = "SELECT ind FROM IndisponibilitePartenaire ind WHERE ind.idIndisponibilitePartenaire = :paramIdPartenaire AND ind.dateFinIndisponibilitePartenaire IS NOT NULL";
	static final String REQ_GET_PART_BY_TYPEPART = "SELECT p FROM Partenaire p INNER JOIN FETCH p.typePartenaire tp WHERE tp.libelleTypePartenaire = :paramTypePart";	
	static final String REQ_HISTORIQUE_INDISPO_BY_PART = "SELECT ind FROM IndisponibilitePartenaire ind INNER JOIN FETCH ind.partenaire p WHERE p.idPartenaire = :paramIdPartenaire and ind.dateFinIndisponibilitePartenaire IS NOT NULL";
	static final String REQ_LISTE_MOTIF_INDISPO_PART = "SELECT m.libelleMotifIndisponibilitePartenaire FROM MotifIndisponibilitePartenaire m";
	static final String REQ_MOTIFINDISPO_BY_MOTIFINDISPO ="SELECT m FROM MotifIndisponibilitePartenaire m WHERE m.libelleMotifIndisponibilitePartenaire = :paramLibeleMotif";

	static final String REQ_GET_LIST_INTERVENTION_A_VENIR_BY_PARTENAIRE = "SELECT m FROM InterventionReelle m INNER JOIN FETCH m.prestation p INNER JOIN FETCH p.partenaire a WHERE a.idPartenaire = :paramIdPartenaire AND m.datePrestation >= CURRENT_DATE";
	static final String REQ_GET_LIST_INTERVENTION_PASSEES_BY_PARTENAIRE = "SELECT m FROM InterventionReelle m INNER JOIN FETCH m.prestation p INNER JOIN FETCH p.partenaire a WHERE a.idPartenaire = :paramIdPartenaire AND m.datePrestation < CURRENT_DATE";

//	static final String REQ_GET_PART_ACTIFS3 = "SELECT p FROM Partenaire p INNER JOIN FETCH p.indisponibilitePartenaire ip WHERE p.contratPartenaire.dateFinContratPartenaire is null AND ip.dateFinIndisponibilitePartenaire < current_date";
	static final String REQ_GET_PART_ACTIFS3 = "SELECT p FROM Partenaire p INNER JOIN FETCH p.indisponibilitePartenaire ip WHERE p.idPartenaire NOT IN (SELECT cpar.partenaire.idPartenaire FROM ContratPartenaire cpar WHERE cpar.dateFinContratPartenaire is not null) AND ip.dateFinIndisponibilitePartenaire < current_date";
//	static final String REQ_NEW_PART_1M = "SELECT p FROM Partenaire p WHERE p.datePremierEnregistrementPartenaire > :paramDate and p.contratPartenaire.dateFinContratPartenaire is null";
	static final String REQ_NEW_PART_1M = "SELECT p FROM Partenaire p WHERE p.datePremierEnregistrementPartenaire > :paramDate and p.idPartenaire NOT IN (SELECT cpar.partenaire.idPartenaire FROM ContratPartenaire cpar WHERE cpar.dateFinContratPartenaire is not null)";
	static final String REQ_DEPART_PART_1M = "SELECT p FROM Partenaire p INNER JOIN p.contratPartenaire pcp WHERE pcp.dateFinContratPartenaire > :paramDate";
	

	
	@PersistenceContext (unitName = "NomadsPU")
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public Partenaire authenticate(String login, String password) {

		Partenaire partenaire = null;
		List<Partenaire> partenaires = null;
		Query queryPartenaire = em.createQuery(REQ_AUTHEN_PART);
		queryPartenaire.setParameter("paramLogin", login);
		queryPartenaire.setParameter("paramPassword", password);
		partenaires=queryPartenaire.getResultList();
		if(partenaires.size() > 0) {
			partenaire= partenaires.get(0);
		}
		return partenaire;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Partenaire> getAllPartenaires() {
		List<Partenaire> partenaires = null;
		Query queryPartenaire = em.createQuery(REQ_GETALL_PART);
		partenaires=queryPartenaire.getResultList();
		return partenaires;
	}

	public List<Partenaire> getAllPartenaireInactifs() {
		List<Partenaire> partenairesInactifs = null;
		Query query = em.createQuery(REQ_ALL_PART_INACTIFS);
		partenairesInactifs=query.getResultList();
		return partenairesInactifs;
	}


	public List<Partenaire> getAllPartenaireActifs1() {
		List<Partenaire> partenairesActifs = null;
		Query query = em.createQuery(REQ_ALL_PART_ACTIFS1);
		partenairesActifs=query.getResultList()
				;
		return partenairesActifs;
	}
	public List<Partenaire> getAllPartenaireActifs2() {
		List<Partenaire> partenairesActifs = null;
		Query query = em.createQuery(REQ_ALL_PART_ACTIFS2);
		partenairesActifs=query.getResultList();
		return partenairesActifs;
	}
	

	public List<Partenaire> getAllPartenaireTypePart(String typePartenaire) {
		List<Partenaire> partenairesTypePart = null;
		Query query = em.createQuery(REQ_GET_PART_BY_TYPEPART);
		query.setParameter("paramTypePart", typePartenaire);
		partenairesTypePart=query.getResultList();
		return partenairesTypePart;
	}

	public Partenaire updatePartenaire (Partenaire partenaire) {
		em.merge(partenaire);
		return partenaire;
	}

	public Partenaire add(Partenaire partenaire) {
		em.persist(partenaire);
		return partenaire;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Partenaire getInfosPartenaireById(int partenaireid) {
		List<Partenaire> partenaires = null;
		Query query = em.createQuery(REQ_GETBYID_PART);
		query.setParameter("paramIdPartenaire", partenaireid);
		partenaires= query.getResultList();
		return partenaires.get(0);
	}

	@Override
	public ContratPartenaire ajouterContratPartenaire(ContratPartenaire nouveauContrat) {
		em.persist(nouveauContrat);
		return nouveauContrat;
	}

	@Override
	public ContratPartenaire updateContratPartenaire(ContratPartenaire contratPartenaire) {
		em.merge(contratPartenaire);
		return contratPartenaire;
	}

	public ContratPartenaire getContratPartenaireById (Partenaire partenaire) {
		ContratPartenaire contratPartenaire = new ContratPartenaire();
		List<ContratPartenaire> listeContrat = null;
		Query query = em.createQuery(REQ_CONTRAT_PART_ID);
		query.setParameter("paramIdPartenaire", partenaire.getIdPartenaire());
		contratPartenaire=(ContratPartenaire) query.getSingleResult();
		return contratPartenaire;

	}

	public List<String> getAllMotifsFinContratPartenaire() {
		List<String> listeMotifs = null;
		Query query = em.createQuery(REQ_LISTE_MOTIF_FIN_CONTRAT);
		listeMotifs=query.getResultList();
		return listeMotifs;
	}

	@Override
	public MotifFinContratPartenaire getMotifByLibelleMotif(String motif) {
		MotifFinContratPartenaire leMotif = null;
		Query query = em.createQuery(REQ_MOTIF_BY_MOTIF);
		query.setParameter("paramLibeleMotif", motif);
		leMotif = (MotifFinContratPartenaire) query.getSingleResult();
		return leMotif;
	}

	public IndisponibilitePartenaire getIndispoEnCoursPart(Partenaire partenaire) throws Exception{
		IndisponibilitePartenaire indispo = null;
		try {
			Query query = em.createQuery(REQ_DERNIERE_INDISPO_PART);
			query.setParameter("paramIdPartenaire", partenaire.getIdPartenaire());
			indispo= (IndisponibilitePartenaire) query.getSingleResult();
		} catch (Exception e) {
			throw new Exception("Ce partenaire est disponible");
		}
		
		return indispo;
	}

	public IndisponibilitePartenaire updateIndispoPart(IndisponibilitePartenaire indispoPart) {
		em.merge(indispoPart);
		return indispoPart;
	}

	@Override
	public IndisponibilitePartenaire ajouterIndispoPart(IndisponibilitePartenaire nouvelleIndispoPart) {
		em.persist(nouvelleIndispoPart);
		return nouvelleIndispoPart;
	}
	
	public List<String> getAllMotifsIndispoPartenaire() {
		List<String> listeMotifs = null;
		Query query = em.createQuery(REQ_LISTE_MOTIF_INDISPO_PART);
		listeMotifs=query.getResultList();
		return listeMotifs;
	}

	@Override
	public MotifIndisponibilitePartenaire getMotifIndispoByLibelleMotifIndispo(String motif) {
		MotifIndisponibilitePartenaire leMotif = null;
		Query query = em.createQuery(REQ_MOTIFINDISPO_BY_MOTIFINDISPO);
		query.setParameter("paramLibeleMotif", motif);
		leMotif = (MotifIndisponibilitePartenaire) query.getSingleResult();
		return leMotif;
	}

	@Override
	public List<Prestation> getListePrestaAVenirByIdPartenaire(int partenaireId) {
		List<Prestation> prestations = null;
		Query query = em.createQuery(REQ_GET_PRESTA_FUTUR_BYID_PART);
		query.setParameter("paramIdPartenaire", partenaireId);
		prestations= query.getResultList();
		return prestations;
	}

	@Override
	public List<Prestation> getListePrestaPasseByIdPartenaire(int partenaireId) {
		List<Prestation> prestations = null;
		Query query = em.createQuery(REQ_GET_PRESTA_PAST_BYID_PART);
		query.setParameter("paramIdPartenaire", partenaireId);
		prestations= query.getResultList();
		return prestations;
	}

	@Override
	public Prestation addPrestation(Prestation prestation) {
		em.persist(prestation);
		return prestation;
	}

	@Override
	public InterventionReelle addIntervention(InterventionReelle interventionReelle) {
		em.persist(interventionReelle);
		return interventionReelle;
	}
	
	public List<IndisponibilitePartenaire> getHistoriqueIndispoByPart(Partenaire partenaire) throws Exception {
		List<IndisponibilitePartenaire> historique = null;
		try{
			Query query=em.createQuery(REQ_HISTORIQUE_INDISPO_BY_PART);
			query.setParameter("paramIdPartenaire", partenaire.getIdPartenaire());
			historique=query.getResultList();
			
		}catch (Exception e) {
			throw new Exception("Ce partenaire n'a pas d'historique d'indisponibilités");
		}
		return historique;
		
	}

	@Override
	public List<InterventionReelle> getAllInterventionAvenirByPArtenaire(int partenaireId) {
		// TODO Auto-generated method stub
		List<InterventionReelle> prestations = null;
		Query query = em.createQuery(REQ_GET_LIST_INTERVENTION_A_VENIR_BY_PARTENAIRE);
		query.setParameter("paramIdPartenaire", partenaireId);
		prestations= query.getResultList();
		return prestations;
	}

	@Override
	public List<InterventionReelle> getAllInterventionPasseesByPArtenaire(int partenaireId) {
		// TODO Auto-generated method stub
		List<InterventionReelle> prestations = null;
		Query query = em.createQuery(REQ_GET_LIST_INTERVENTION_PASSEES_BY_PARTENAIRE);
		query.setParameter("paramIdPartenaire", partenaireId);
		prestations= query.getResultList();
		return prestations;
	}

	public List<Partenaire> getPartenairesActifsAjD() {
		List<Partenaire> partenaires = null;
		Query requete = em.createQuery(REQ_GET_PART_ACTIFS3);
		partenaires = requete.getResultList();
		return partenaires;
	}

	@Override
	public List<Partenaire> getNouveauxPartenaires1Mois() {
		List<Partenaire> partenaires = null;
		Query requete = em.createQuery(REQ_NEW_PART_1M);
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		Date date = (Date) cal.getTime();
		requete.setParameter("paramDate", date);
		partenaires = requete.getResultList();
		return partenaires;
	}

	@Override
	public List<Partenaire> getDepartPartenaires1Mois() {
		List<Partenaire> partenaires = null;
		Query requete = em.createQuery(REQ_DEPART_PART_1M);
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		Date date = (Date) cal.getTime();
		requete.setParameter("paramDate", date);
		partenaires = requete.getResultList();
		return partenaires;
		
	}
}
