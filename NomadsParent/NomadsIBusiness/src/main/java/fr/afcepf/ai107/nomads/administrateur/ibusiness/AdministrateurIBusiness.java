package fr.afcepf.ai107.nomads.administrateur.ibusiness;

import fr.afcepf.ai107.nomads.entities.Administrateur;

public interface AdministrateurIBusiness {
  
	
	Administrateur connection(String login, String password);
}
