package fr.afcepf.ai107.nomads.controller.retours;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.Masseur;
import fr.afcepf.ai107.nomads.entities.RetourExperienceMasseur;
import fr.afcepf.ai107.nomads.prestation.ibusiness.PrestationIBusiness;
import fr.afcepf.ai107.nomads.retours.ibusiness.RetoursMasseursIBusiness;

@ManagedBean(name = "faireUnRetourMasseur")
@ViewScoped
public class FaireUnRetourMasseurManagedBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private PrestationIBusiness proxyPrestationBusiness;
	
	@EJB
	private RetoursMasseursIBusiness proxyRetourMass;
	
	@EJB
	private PrestationIBusiness proxyPrestaBusiness;
	
	@ManagedProperty (value="#{mbAccount.masseur}")
	private Masseur connectedMasseur; 
	
	private InterventionReelle interventionReelle = new InterventionReelle();
	private Integer noteAccueil;
	private Integer noteClientele;
	private Integer noteAmbiance;
	private String commentaire;
	private Integer nbClientsMasses;
	private Double montantGainsRecoltes;
	private String libellePrestation;
	private Integer idIntervention = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id"));
	
//	@ManagedProperty(value="#{mbFicheMasseurMasseur.selectedIntervention}")
	
	
	private RetourExperienceMasseur monAvis = new RetourExperienceMasseur();
	
	//@ManagedProperty(value ="#{mbFicheMasseurMasseur.monRetour}")
	private List<RetourExperienceMasseur> listeRetours;
	


	
	@PostConstruct
	private void init() {
	//	libellePrestation = interventionReelle.getPrestation().getLibellePrestation();
	//	System.out.println(interventionReelle.getIdInterventionReelle() + interventionReelle.getPrestation().getLibellePrestation());
		interventionReelle = proxyPrestationBusiness.getInterventionById(idIntervention);
	}
	
	
	public RetourExperienceMasseur MonRtour(){
		RetourExperienceMasseur interv;
		interventionReelle = proxyPrestationBusiness.getInterventionById(idIntervention);
		listeRetours = proxyRetourMass.getRetourExByIdIntervention(interventionReelle, connectedMasseur);
		interv = listeRetours.get(0);
		return interv;
	}
	
//	public Set<RetourExperienceMasseur> retourParListe() 
//		listeRetours.add(monAvis);
//		return listeRetours;
//	}
	
	public String ajouterCommentaire() {
		
		if(commentaire != null) {
		monAvis.setCommentaireRetourExperienceMasseur(commentaire);
		}
		monAvis.setDateRetourExperienceMasseur(new Date());
	//	monAvis.setIdRetourExperienceMasseur(null); changer en integer au besoin
		if(interventionReelle != null) {
		monAvis.setInterventionReelle(interventionReelle);
		}
		if (connectedMasseur != null) {
		monAvis.setMasseur(connectedMasseur);
		}
		if(nbClientsMasses != null) {
		monAvis.setNombreClientsMasses(nbClientsMasses);
		}
		if(noteAccueil != null ) {
		monAvis.setNoteAccueilEvaluationMasseur(noteAccueil);
		}
		if(noteAmbiance != null) {
		monAvis.setNoteAmbianceEvaluationMasseur(noteAmbiance);
		}
		if(noteClientele != null) {
		monAvis.setNoteClienteleEvaluationMasseur(noteClientele);
		}
		if (montantGainsRecoltes != null) {
			monAvis.setChiffreAffaire(montantGainsRecoltes);
			}
	//	listeRetours.add(monAvis);
	//	interventionReelle.setRetoursExperienceMasseurs(listeRetours);
		// faire l'insert
		proxyRetourMass.ajouterUnRetour(monAvis);
		return "/MesPrestationsMasseur.xhtml?faces-redirect=true";
	}
	
	
	
	
	/**
	 * @return the idIntervention
	 */
	public Integer getIdIntervention() {
		return idIntervention;
	}

	/**
	 * @param idIntervention the idIntervention to set
	 */
	public void setIdIntervention(Integer idIntervention) {
		this.idIntervention = idIntervention;
	}

	/**
	 * @return the libellePrestation
	 */
	public String getLibellePrestation() {
		return libellePrestation;
	}

	/**
	 * @param libellePrestation the libellePrestation to set
	 */
	public void setLibellePrestation(String libellePrestation) {
		this.libellePrestation = libellePrestation;
	}

	/**
	 * @return the monAvis
	 */
	public RetourExperienceMasseur getMonAvis() {
		return monAvis;
	}

	/**
	 * @param monAvis the monAvis to set
	 */
	public void setMonAvis(RetourExperienceMasseur monAvis) {
		this.monAvis = monAvis;
	}






	/**
	 * @return the listeRetours
	 */
	public List<RetourExperienceMasseur> getListeRetours() {
		return listeRetours;
	}


	/**
	 * @param listeRetours the listeRetours to set
	 */
	public void setListeRetours(List<RetourExperienceMasseur> listeRetours) {
		this.listeRetours = listeRetours;
	}


	/**
	 * @return the interventionReelle
	 */
	public InterventionReelle getInterventionReelle() {
		return interventionReelle;
	}
	/**
	 * @param interventionReelle the interventionReelle to set
	 */
	public void setInterventionReelle(InterventionReelle interventionReelle) {
		this.interventionReelle = interventionReelle;
	}
	/**
	 * @return the connectedMasseur
	 */
	public Masseur getConnectedMasseur() {
		return connectedMasseur;
	}
	/**
	 * @param connectedMasseur the connectedMasseur to set
	 */
	public void setConnectedMasseur(Masseur connectedMasseur) {
		this.connectedMasseur = connectedMasseur;
	}
	/**
	 * @return the noteAccueil
	 */
	public Integer getNoteAccueil() {
		return noteAccueil;
	}
	/**
	 * @param noteAccueil the noteAccueil to set
	 */
	public void setNoteAccueil(Integer noteAccueil) {
		this.noteAccueil = noteAccueil;
	}
	/**
	 * @return the noteClientele
	 */
	public Integer getNoteClientele() {
		return noteClientele;
	}
	/**
	 * @param noteClientele the noteClientele to set
	 */
	public void setNoteClientele(Integer noteClientele) {
		this.noteClientele = noteClientele;
	}
	/**
	 * @return the noteAmbiance
	 */
	public Integer getNoteAmbiance() {
		return noteAmbiance;
	}
	/**
	 * @param noteAmbiance the noteAmbiance to set
	 */
	public void setNoteAmbiance(Integer noteAmbiance) {
		this.noteAmbiance = noteAmbiance;
	}
	/**
	 * @return the commentaire
	 */
	public String getCommentaire() {
		return commentaire;
	}
	/**
	 * @param commentaire the commentaire to set
	 */
	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}
	/**
	 * @return the nbClientsMasses
	 */
	public Integer getNbClientsMasses() {
		return nbClientsMasses;
	}
	/**
	 * @param nbClientsMasses the nbClientsMasses to set
	 */
	public void setNbClientsMasses(Integer nbClientsMasses) {
		this.nbClientsMasses = nbClientsMasses;
	}
	/**
	 * @return the montantGainsRecoltes
	 */
	public Double getMontantGainsRecoltes() {
		return montantGainsRecoltes;
	}
	/**
	 * @param montantGainsRecoltes the montantGainsRecoltes to set
	 */
	public void setMontantGainsRecoltes(Double montantGainsRecoltes) {
		this.montantGainsRecoltes = montantGainsRecoltes;
	}
	
	

}
