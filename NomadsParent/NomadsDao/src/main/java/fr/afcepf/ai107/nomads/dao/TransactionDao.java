package fr.afcepf.ai107.nomads.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.nomads.entities.Transaction;
import fr.afcepf.ai107.nomads.entities.TypePackMads;
import fr.afcepf.ai107.nomads.idao.TransactionIDao;

@Remote(TransactionIDao.class)
@Stateless
public class TransactionDao extends GenericDao<Transaction> implements TransactionIDao {

	@PersistenceContext(unitName = "NomadsPU")
	private EntityManager em;
	
	private static final String GET_BY_ID_MASSEUR="SELECT t FROM Transaction t INNER JOIN FETCH t.typePackMads WHERE t.masseur.id_masseur= :paramIdMasseur";
	private static final String GET_ALL_TYPEPACKMADS = "SELECT tp FROM TypePackMads";
	
	@Override
	public List<Transaction> getListTransactionByIdMasseur(Integer idMasseur) {
		List<Transaction> transactions = null;
		Query requete = em.createQuery(GET_BY_ID_MASSEUR);
		requete.setParameter("paramIdMasseur", idMasseur);
		transactions=requete.getResultList();
		return transactions;
	}
	
	public List<TypePackMads> getAllTypePackMads(){
		List<TypePackMads> allPackMads=null;
		Query query = em.createQuery(GET_ALL_TYPEPACKMADS);
		allPackMads=query.getResultList();
		return allPackMads;
	}

}
