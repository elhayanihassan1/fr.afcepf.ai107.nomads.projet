package fr.afcepf.ai107.nomads.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.nomads.entities.IndisponibiliteMasseur;
import fr.afcepf.ai107.nomads.entities.Masseur;
import fr.afcepf.ai107.nomads.entities.MotifIndisponibiliteMasseur;
import fr.afcepf.ai107.nomads.idao.IndisponibiliteMasseurIDao;

@Remote (IndisponibiliteMasseurIDao.class)
@Stateless
public class IndisponibiliteMasseurDao extends GenericDao<IndisponibiliteMasseur>
		implements IndisponibiliteMasseurIDao {

	@PersistenceContext (unitName = "NomadsPU")
	private EntityManager em;
	
	final String  getIndisponibles = "SELECT distinct m FROM Masseur m INNER JOIN FETCH m.indisponibilitesMasseur i WHERE i.dateFinIndisponibiliteMasseur > CURRENT_DATE OR (i.dateDebutIndisponibiliteMasseur is not null AND i.dateFinIndisponibiliteMasseur is null)"; //AND m.dateDepartDefinitif is null
	final String getMasseursDispo =  "SELECT distinct m FROM Masseur m  WHERE m not in :indispo and m.dateDepartDefinitifMasseur is null";
	private final String GET_INDISPO_A_VENIR_BY_MASS = "SELECT im FROM IndisponibiliteMasseur im INNER JOIN FETCH im.motifIndisponibiliteMasseur moin WHERE im.masseur.id_masseur = :paramMasseur AND (im.dateFinIndisponibiliteMasseur > current_date OR im.dateFinIndisponibiliteMasseur is null)";
	private final String GET_INDISPO_PASSEES_BY_MASS = "SELECT im FROM IndisponibiliteMasseur im INNER JOIN FETCH im.motifIndisponibiliteMasseur moin WHERE im.masseur.id_masseur = :paramMasseur AND im.dateFinIndisponibiliteMasseur < current_date";
	
	
	@Override
	public List<Masseur> getListeMasseursIndispo() {
		List<Masseur> masseursIndispo = null;
		Query requeteIndispo = em.createQuery(getIndisponibles);
		masseursIndispo = requeteIndispo.getResultList();
		
		return masseursIndispo;
	}
	
	
	@Override
	public List<Masseur> getListeMasseursActifs() {
		List<Masseur> masseursDispo = null;
		Query requeteDispo = em.createQuery(getMasseursDispo);
		requeteDispo.setParameter("indispo", getListeMasseursIndispo());
		masseursDispo=requeteDispo.getResultList();
			
		return masseursDispo;
	}
//
//	@Override
//	public List<Masseur> getListeMasseursActifs() {
//		List<Masseur> masseursNonIndispo = null;
//		List<Masseur> masseursIndispo = null;
//		
//		Query requeteIndispo = em.createQuery(getIndisponibles);
//		masseursIndispo = requeteIndispo.getResultList();
//		Query requeteNonIndispo = em.createQuery(getMasseursNonIndispo);
//		requeteNonIndispo.setParameter("indispo", masseursIndispo);
//		masseursNonIndispo=requeteNonIndispo.getResultList();
//			
//		return masseursNonIndispo;
//	}


	@Override
	public List<IndisponibiliteMasseur> getIndispoMasseurFutByIdMasseur(Integer idMasseur) {
		List<IndisponibiliteMasseur> indisps = null;
		Query requete = em.createQuery(GET_INDISPO_A_VENIR_BY_MASS);
		requete.setParameter("paramMasseur", idMasseur);
		indisps=requete.getResultList();
		return indisps;
	}


	@Override
	public List<IndisponibiliteMasseur> getIndispoMasseurPasseesByIdMasseur(Integer idMasseur) {
		List<IndisponibiliteMasseur> indisps = null;
		Query requete = em.createQuery(GET_INDISPO_PASSEES_BY_MASS);
		requete.setParameter("paramMasseur", idMasseur);
		indisps=requete.getResultList();
		return indisps;
	}


//	@Override
//	public List<MotifIndisponibiliteMasseur> getAllMotifsIndispoMass() {
//		// TODO Auto-generated method stub
//		return null;
//	}

	
	
	
	

}
