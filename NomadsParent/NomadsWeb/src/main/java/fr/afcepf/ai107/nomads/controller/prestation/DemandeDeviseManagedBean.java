package fr.afcepf.ai107.nomads.controller.prestation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import fr.afcepf.ai107.nomads.controller.email.emailAdmin;
import fr.afcepf.ai107.nomads.controller.email.emailPartenaire;
import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.LocationMateriel;
import fr.afcepf.ai107.nomads.entities.MotifAnnulationPrestation;
import fr.afcepf.ai107.nomads.entities.Partenaire;
import fr.afcepf.ai107.nomads.entities.Prestation;
import fr.afcepf.ai107.nomads.entities.RetourExperienceMasseur;
import fr.afcepf.ai107.nomads.entities.RetourExperiencePartenaire;
import fr.afcepf.ai107.nomads.entities.TypePaiement;
import fr.afcepf.ai107.nomads.entities.VilleCp;
import fr.afcepf.ai107.nomads.outils.ibusiness.OutilsIBusiness;
import fr.afcepf.ai107.nomads.prestation.ibusiness.PrestationIBusiness;

@ManagedBean(name = "mbDemandeDevise")
@ViewScoped
public class DemandeDeviseManagedBean implements Serializable{


	private static final long serialVersionUID = 1L;
	
	@EJB
	private PrestationIBusiness proxyPrestationBusiness;
	@EJB
	private OutilsIBusiness proxyVilleCpBusiness;
	
	private List<Prestation> prestations = new ArrayList<Prestation>();
	private Prestation prestation = new Prestation() ;
	@ManagedProperty(value = "#{mbAccount.partenaire}")
	private String libellePrestation;
	private Double coutDevisPrestation;
	private Date dateEmissionPrestation;
	private Date dateValidationPrestation;
	private Date heureDebutPrestation;
	private Date heureFinPrestation;
	private Integer nombreMasseursPrestation;
	private Integer nombreChaisesErgonomiquesPrestation;
	private Double montantRemunerationPrestation;
	private Date dateAnnulationPrestation;
	private Date datePaiementPrestation;
	private String adresseInterventionPrestation;
	private String specificitesPrestation;
	private Integer coutMads;
	private Partenaire partenaire; 
	private VilleCp villeCp;
	private TypePaiement typePaiement; 
	private List<InterventionReelle> interventionReelle = new ArrayList<InterventionReelle>();
	private InterventionReelle interR = new InterventionReelle();
	private Integer idInterventionReelle;
	private Date datePrestation;
	private String success;
	


	private List<String> codesPostaux = new ArrayList<String>();
	private List<String> villes = new ArrayList<String>();
	private String selectedCP = null;
	private String selectedVille = null;
	
	public void onCPChange() { 
		if (selectedCP !=null && !selectedCP.equals("")) {
			villes = proxyVilleCpBusiness.listeVilleByCp(selectedCP);
		}else {
			villes = new ArrayList<String>();
		}
	}
	
	private int prestationId;
	@PostConstruct
	public void init() {
		  if( FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id") != null ) {
			  prestationId = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id"));
			  prestation=proxyPrestationBusiness.getPrestationById(prestationId);
			  selectedCP = prestation.getVilleCp().getLibelleCp();	
			 }
		  
		// selectedVille = prestation.getVilleCp().getLibelleVille();

		prestations = proxyPrestationBusiness.getListeDemandePrestation();
		codesPostaux = proxyVilleCpBusiness.listeCp();
		
	}
    
	public String recupererInfosDeDevis () throws Exception {
	
	  	prestation.setDateValidationPrestation( new Date());
	  	
       	proxyPrestationBusiness.validationPrestation(prestation);
       	
        //String email = partenaire.getAdresseMailPartenaire();
       	//emailPartenaire.sendMail(email);
       	emailPartenaire.sendMail("partenaire207@gmail.com");
		
       	
       	return  "/ListeDesDemandeATraiter.xhtml?faces-redirect=true";
	

	}
	
//	 public void saveMessage() {
//	        FacesContext context = FacesContext.getCurrentInstance();
//	         
//	        context.addMessage(null, new FacesMessage("Successful : ",  "la prestation a bien été enregistré") );
//
//	    }
	
	//--------------------------- Coté admin------------------------------//
	
	
	
	public List<Prestation> getPrestations() {
		return prestations;
	}

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public int getPrestationId() {
		return prestationId;
	}

	public void setPrestationId(int prestationId) {
		this.prestationId = prestationId;
	}

	public InterventionReelle getInterR() {
		return interR;
	}

	public void setInterR(InterventionReelle interR) {
		this.interR = interR;
	}

	public void setPrestations(List<Prestation> prestations) {
		this.prestations = prestations;
	}


	public String getLibellePrestation() {
		return libellePrestation;
	}

	public void setLibellePrestation(String libellePrestation) {
		this.libellePrestation = libellePrestation;
	}

	public Double getCoutDevisPrestation() {
		return coutDevisPrestation;
	}

	public void setCoutDevisPrestation(Double coutDevisPrestation) {
		this.coutDevisPrestation = coutDevisPrestation;
	}

	public Date getDateEmissionPrestation() {
		return dateEmissionPrestation;
	}

	public void setDateEmissionPrestation(Date dateEmissionPrestation) {
		this.dateEmissionPrestation = dateEmissionPrestation;
	}

	public Date getDateValidationPrestation() {
		return dateValidationPrestation;
	}



	public Date getHeureDebutPrestation() {
		return heureDebutPrestation;
	}

	public void setHeureDebutPrestation(Date heureDebutPrestation) {
		this.heureDebutPrestation = heureDebutPrestation;
	}

	public Date getHeureFinPrestation() {
		return heureFinPrestation;
	}

	public void setHeureFinPrestation(Date heureFinPrestation) {
		this.heureFinPrestation = heureFinPrestation;
	}

	public void setDateValidationPrestation(Date dateValidationPrestation) {
		this.dateValidationPrestation = dateValidationPrestation;
	}

	public Integer getNombreMasseursPrestation() {
		return nombreMasseursPrestation;
	}

	public void setNombreMasseursPrestation(Integer nombreMasseursPrestation) {
		this.nombreMasseursPrestation = nombreMasseursPrestation;
	}

	public Integer getNombreChaisesErgonomiquesPrestation() {
		return nombreChaisesErgonomiquesPrestation;
	}

	public void setNombreChaisesErgonomiquesPrestation(Integer nombreChaisesErgonomiquesPrestation) {
		this.nombreChaisesErgonomiquesPrestation = nombreChaisesErgonomiquesPrestation;
	}

	public Double getMontantRemunerationPrestation() {
		return montantRemunerationPrestation;
	}

	public void setMontantRemunerationPrestation(Double montantRemunerationPrestation) {
		this.montantRemunerationPrestation = montantRemunerationPrestation;
	}

	public Date getDateAnnulationPrestation() {
		return dateAnnulationPrestation;
	}

	public void setDateAnnulationPrestation(Date dateAnnulationPrestation) {
		this.dateAnnulationPrestation = dateAnnulationPrestation;
	}

	public Date getDatePaiementPrestation() {
		return datePaiementPrestation;
	}

	public void setDatePaiementPrestation(Date datePaiementPrestation) {
		this.datePaiementPrestation = datePaiementPrestation;
	}

	public String getAdresseInterventionPrestation() {
		return adresseInterventionPrestation;
	}

	public void setAdresseInterventionPrestation(String adresseInterventionPrestation) {
		this.adresseInterventionPrestation = adresseInterventionPrestation;
	}

	public String getSpecificitesPrestation() {
		return specificitesPrestation;
	}

	public void setSpecificitesPrestation(String specificitesPrestation) {
		this.specificitesPrestation = specificitesPrestation;
	}

	public Integer getCoutMads() {
		return coutMads;
	}

	public void setCoutMads(Integer coutMads) {
		this.coutMads = coutMads;
	}

	public Partenaire getPartenaire() {
		return partenaire;
	}

	public void setPartenaire(Partenaire partenaire) {
		this.partenaire = partenaire;
	}

	public VilleCp getVilleCp() {
		return villeCp;
	}

	public void setVilleCp(VilleCp villeCp) {
		this.villeCp = villeCp;
	}

	public TypePaiement getTypePaiement() {
		return typePaiement;
	}

	public void setTypePaiement(TypePaiement typePaiement) {
		this.typePaiement = typePaiement;
	}

	public List<InterventionReelle> getInterventionReelle() {
		return interventionReelle;
	}

	public void setInterventionReelle(List<InterventionReelle> interventionReelle) {
		this.interventionReelle = interventionReelle;
	}

	public Integer getIdInterventionReelle() {
		return idInterventionReelle;
	}

	public void setIdInterventionReelle(Integer idInterventionReelle) {
		this.idInterventionReelle = idInterventionReelle;
	}

	public Date getDatePrestation() {
		return datePrestation;
	}

	public void setDatePrestation(Date datePrestation) {
		this.datePrestation = datePrestation;
	}

	public Prestation getPrestation() {
		return prestation;
	}

	public void setPrestation(Prestation prestation) {
		this.prestation = prestation;
	}

	public List<String> getCodesPostaux() {
		return codesPostaux;
	}

	public void setCodesPostaux(List<String> codesPostaux) {
		this.codesPostaux = codesPostaux;
	}

	public List<String> getVilles() {
		return villes;
	}

	public void setVilles(List<String> villes) {
		this.villes = villes;
	}

	public String getSelectedCP() {
		return selectedCP;
	}

	public void setSelectedCP(String selectedCP) {
		this.selectedCP = selectedCP;
	}

	public String getSelectedVille() {
		return selectedVille;
	}

	public void setSelectedVille(String selectedVille) {
		this.selectedVille = selectedVille;
	}
 
	
	
}
