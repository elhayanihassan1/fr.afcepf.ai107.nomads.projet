package fr.afcepf.ai107.nomads.idao;

import java.util.List;

import fr.afcepf.ai107.nomads.entities.ContratPartenaire;
import fr.afcepf.ai107.nomads.entities.IndisponibilitePartenaire;
import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.MotifFinContratPartenaire;
import fr.afcepf.ai107.nomads.entities.MotifIndisponibilitePartenaire;
import fr.afcepf.ai107.nomads.entities.Partenaire;
import fr.afcepf.ai107.nomads.entities.Prestation;

public interface PartenaireIDao extends GenericIDao<Partenaire> {

	
	//----------------------- Coté Admin -----------------------// 
	
	
	List<Partenaire> getAllPartenaires();
	Partenaire updatePartenaire(Partenaire partenaireRecupere);
	Partenaire add(Partenaire partenaire);
	Partenaire getInfosPartenaireById(int partenaireid);
	ContratPartenaire ajouterContratPartenaire(ContratPartenaire nouveauContrat);
	ContratPartenaire updateContratPartenaire(ContratPartenaire contratPartenaire);
	ContratPartenaire getContratPartenaireById (Partenaire partenaire);
	List<String> getAllMotifsFinContratPartenaire();
	MotifFinContratPartenaire getMotifByLibelleMotif(String motif);
	IndisponibilitePartenaire getIndispoEnCoursPart(Partenaire partenaire) throws Exception;
	IndisponibilitePartenaire updateIndispoPart(IndisponibilitePartenaire indispoPart);
	IndisponibilitePartenaire ajouterIndispoPart(IndisponibilitePartenaire nouvelleIndispoPart);
	List<Prestation> getListePrestaAVenirByIdPartenaire(int partenaireId);
	List<Prestation> getListePrestaPasseByIdPartenaire(int partenaireId);
	List<Partenaire> getAllPartenaireInactifs();
	List<Partenaire> getAllPartenaireActifs1();
	List<Partenaire> getAllPartenaireActifs2();

	List<Partenaire> getAllPartenaireTypePart(String typePartenaire);
	List<IndisponibilitePartenaire> getHistoriqueIndispoByPart(Partenaire partenaire) throws Exception;
	List<Partenaire> getPartenairesActifsAjD ();
	List<Partenaire> getNouveauxPartenaires1Mois();
	List<Partenaire> getDepartPartenaires1Mois();
	
	//---------------------Cote Partenaire------------------//
	
	Partenaire authenticate(String login, String password);
	Prestation addPrestation(Prestation prestation);
    InterventionReelle addIntervention(InterventionReelle interventionReelle);
    List<String> getAllMotifsIndispoPartenaire();
    MotifIndisponibilitePartenaire getMotifIndispoByLibelleMotifIndispo(String motif);
    List<InterventionReelle>getAllInterventionAvenirByPArtenaire(int partenaireId);
    List<InterventionReelle>getAllInterventionPasseesByPArtenaire(int partenaireId);
    

}
