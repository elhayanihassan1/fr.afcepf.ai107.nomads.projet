package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import javax.persistence.Table;

@Entity
@Table (name = "inscription_prestation")
public class InscriptionPrestationOld implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_inscription_prestation")
    private int idInscriptionPrestation;
	@Column(name = "date_prestation")
	private Date datePrestation;
	@Column(name = "date_inscription")
	private Date dateInscription;
//	@Column(name = "date_validation", nullable = true)
//	private Date dateValidation;
//	@Column(name = "date_esistement", nullable = true)
//	private Date dateDesistement;
	@Column(name = "cout_mads")
    private int coutMads;
//	@Column(name = "libelle_fb_masseur", nullable = true)
//    private String libelleFbMasseur;
//	@Column(name = "date_fb_masseur", nullable = true)
//    private Date dateFbMasseur;
//	@Column(name = "montant_presta_intervention", nullable = true)
//    private double montantPrestaIntervention;
//	@Column(name = "date_facture", nullable = true)
//    private Date dateFacture;
//	@Column(name = "note_feedback", nullable = true)
//    private Integer noteFeedback;
//	@Column(name = "nombre_clients", nullable = true)
//    private Integer nombreClients;
//	@Column(name = "chiffre_affaires", nullable = true)
//    private Double chiffreAffaires;
	@ManyToOne
	@JoinColumn (name = "id_masseur", referencedColumnName = "id_masseur")
	private MasseurOld masseur;
   
	public InscriptionPrestationOld() {
		super();
	}

	public InscriptionPrestationOld(int idInscriptionPrestation, Date datePrestation, Date dateInscription, int coutMads,
			MasseurOld masseur) {
		super();
		this.idInscriptionPrestation = idInscriptionPrestation;
		this.datePrestation = datePrestation;
		this.dateInscription = dateInscription;
		this.coutMads = coutMads;
		this.masseur = masseur;
	}

	public int getIdInscriptionPrestation() {
		return idInscriptionPrestation;
	}

	public void setIdInscriptionPrestation(int idInscriptionPrestation) {
		this.idInscriptionPrestation = idInscriptionPrestation;
	}

	public Date getDatePrestation() {
		return datePrestation;
	}

	public void setDatePrestation(Date datePrestation) {
		this.datePrestation = datePrestation;
	}

	public Date getDateInscription() {
		return dateInscription;
	}

	public void setDateInscription(Date dateInscription) {
		this.dateInscription = dateInscription;
	}

	public int getCoutMads() {
		return coutMads;
	}

	public void setCoutMads(int coutMads) {
		this.coutMads = coutMads;
	}

	public MasseurOld getMasseur() {
		return masseur;
	}

	public void setMasseur(MasseurOld masseur) {
		this.masseur = masseur;
	}

}
