package fr.afcepf.ai107.nomads.dao;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.nomads.idao.GenericIDao;


@Remote(GenericIDao.class)
@Stateless
public abstract class GenericDao<T> implements GenericIDao<T> {

	@PersistenceContext(unitName = "NomadsPU")
	private EntityManager em;

	@Override
	public T add(T t) {
		em.persist(t);
		return t;
	}

	@Override
	public boolean delete(T t) {
		boolean removed;
		try {
			t = em.merge(t);
			em.remove(t);
			removed = true;
		} catch (Exception e) {
			e.printStackTrace();
			removed = false;
		}
		return removed;
	}

	@Override
	public T update(T t) {
		em.merge(t);
		return t;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T getById(int i) {
		T t = null;
		try {
			String className = ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]
					.getTypeName();
			Class<?> clazz;
			clazz = Class.forName(className);
			t = (T) em.find(clazz, i);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return t;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<T> findAll() {
		 List<T> objects = null;
		try {
			String className= ((ParameterizedType) getClass().
					getGenericSuperclass()).getActualTypeArguments()[0].getTypeName();
			Class<?> clazz;
			clazz = Class.forName(className);
            Query query = em.createQuery("FROM " + clazz.getName());
            objects = query.getResultList();
		}catch (Exception e) {
			e.printStackTrace();
		}     
	        return objects;
	}
}
