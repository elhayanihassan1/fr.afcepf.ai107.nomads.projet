package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import javax.persistence.Table;

@Entity
@Table(name = "location_materiel")
public class LocationMateriel implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_location_materiel")
	private int idLocationMateriel;

	@Column(name = "article_location_materiel")
	private String articleLocationMateriel;

	@Column(name = "quantite_article_location_materiel")
	private int quantiteArticleLocationMateriel;

	@Column(name = "date_emprunt_location_materiel")
	private Date dateEmpruntLocationMateriel;

	@Column(name = "date_retour_location_materiel")
	private Date dateRetourLocationMateriel;

	@Column(name = "montant_caution_en_mads_location_materiel")
	private int montantCautionEnMadsLocationMateriel;

	@Column(name = "etat_emprunt_location_materiel")
	private String etatEmpruntLocationMateriel;

	@Column(name = "etat_retour_location_materiel")
	private String etatRetourLocationMateriel;

	@ManyToOne
	@JoinColumn(name = "id_prestation", referencedColumnName = "id_prestation")
	private Prestation prestation;
	
	@ManyToOne
	@JoinColumn(name ="id_masseur", referencedColumnName = "id_masseur")
	private Masseur masseur; 
	
	@ManyToOne
	@JoinColumn(name = "id_stock_materiel", referencedColumnName = "id_stock_materiel")
	private StockMateriel stockMateriel;
	
	/**
	 * 
	 */
	public LocationMateriel() {
		super();
	}

	/**
	 * @param idLocationMateriel
	 * @param articleLocationMateriel
	 * @param quantiteArticleLocationMateriel
	 * @param dateEmpruntLocationMateriel
	 * @param dateRetourLocationMateriel
	 * @param montantCautionEnMadsLocationMateriel
	 * @param etatEmpruntLocationMateriel
	 * @param etatRetourLocationMateriel
	 * @param prestation
	 * @param masseur
	 * @param stockMateriel
	 */
	public LocationMateriel(int idLocationMateriel, String articleLocationMateriel, int quantiteArticleLocationMateriel,
			Date dateEmpruntLocationMateriel, Date dateRetourLocationMateriel, int montantCautionEnMadsLocationMateriel,
			String etatEmpruntLocationMateriel, String etatRetourLocationMateriel, Prestation prestation,
			Masseur masseur, StockMateriel stockMateriel) {
		super();
		this.idLocationMateriel = idLocationMateriel;
		this.articleLocationMateriel = articleLocationMateriel;
		this.quantiteArticleLocationMateriel = quantiteArticleLocationMateriel;
		this.dateEmpruntLocationMateriel = dateEmpruntLocationMateriel;
		this.dateRetourLocationMateriel = dateRetourLocationMateriel;
		this.montantCautionEnMadsLocationMateriel = montantCautionEnMadsLocationMateriel;
		this.etatEmpruntLocationMateriel = etatEmpruntLocationMateriel;
		this.etatRetourLocationMateriel = etatRetourLocationMateriel;
		this.prestation = prestation;
		this.masseur = masseur;
		this.stockMateriel = stockMateriel;
	}

	/**
	 * @return the idLocationMateriel
	 */
	public int getIdLocationMateriel() {
		return idLocationMateriel;
	}

	/**
	 * @param idLocationMateriel the idLocationMateriel to set
	 */
	public void setIdLocationMateriel(int idLocationMateriel) {
		this.idLocationMateriel = idLocationMateriel;
	}

	/**
	 * @return the articleLocationMateriel
	 */
	public String getArticleLocationMateriel() {
		return articleLocationMateriel;
	}

	/**
	 * @param articleLocationMateriel the articleLocationMateriel to set
	 */
	public void setArticleLocationMateriel(String articleLocationMateriel) {
		this.articleLocationMateriel = articleLocationMateriel;
	}

	/**
	 * @return the quantiteArticleLocationMateriel
	 */
	public int getQuantiteArticleLocationMateriel() {
		return quantiteArticleLocationMateriel;
	}

	/**
	 * @param quantiteArticleLocationMateriel the quantiteArticleLocationMateriel to set
	 */
	public void setQuantiteArticleLocationMateriel(int quantiteArticleLocationMateriel) {
		this.quantiteArticleLocationMateriel = quantiteArticleLocationMateriel;
	}

	/**
	 * @return the dateEmpruntLocationMateriel
	 */
	public Date getDateEmpruntLocationMateriel() {
		return dateEmpruntLocationMateriel;
	}

	/**
	 * @param dateEmpruntLocationMateriel the dateEmpruntLocationMateriel to set
	 */
	public void setDateEmpruntLocationMateriel(Date dateEmpruntLocationMateriel) {
		this.dateEmpruntLocationMateriel = dateEmpruntLocationMateriel;
	}

	/**
	 * @return the dateRetourLocationMateriel
	 */
	public Date getDateRetourLocationMateriel() {
		return dateRetourLocationMateriel;
	}

	/**
	 * @param dateRetourLocationMateriel the dateRetourLocationMateriel to set
	 */
	public void setDateRetourLocationMateriel(Date dateRetourLocationMateriel) {
		this.dateRetourLocationMateriel = dateRetourLocationMateriel;
	}

	/**
	 * @return the montantCautionEnMadsLocationMateriel
	 */
	public int getMontantCautionEnMadsLocationMateriel() {
		return montantCautionEnMadsLocationMateriel;
	}

	/**
	 * @param montantCautionEnMadsLocationMateriel the montantCautionEnMadsLocationMateriel to set
	 */
	public void setMontantCautionEnMadsLocationMateriel(int montantCautionEnMadsLocationMateriel) {
		this.montantCautionEnMadsLocationMateriel = montantCautionEnMadsLocationMateriel;
	}

	/**
	 * @return the etatEmpruntLocationMateriel
	 */
	public String getEtatEmpruntLocationMateriel() {
		return etatEmpruntLocationMateriel;
	}

	/**
	 * @param etatEmpruntLocationMateriel the etatEmpruntLocationMateriel to set
	 */
	public void setEtatEmpruntLocationMateriel(String etatEmpruntLocationMateriel) {
		this.etatEmpruntLocationMateriel = etatEmpruntLocationMateriel;
	}

	/**
	 * @return the etatRetourLocationMateriel
	 */
	public String getEtatRetourLocationMateriel() {
		return etatRetourLocationMateriel;
	}

	/**
	 * @param etatRetourLocationMateriel the etatRetourLocationMateriel to set
	 */
	public void setEtatRetourLocationMateriel(String etatRetourLocationMateriel) {
		this.etatRetourLocationMateriel = etatRetourLocationMateriel;
	}

	/**
	 * @return the prestation
	 */
	public Prestation getPrestation() {
		return prestation;
	}

	/**
	 * @param prestation the prestation to set
	 */
	public void setPrestation(Prestation prestation) {
		this.prestation = prestation;
	}

	/**
	 * @return the masseur
	 */
	public Masseur getMasseur() {
		return masseur;
	}

	/**
	 * @param masseur the masseur to set
	 */
	public void setMasseur(Masseur masseur) {
		this.masseur = masseur;
	}

	/**
	 * @return the stockMateriel
	 */
	public StockMateriel getStockMateriel() {
		return stockMateriel;
	}

	/**
	 * @param stockMateriel the stockMateriel to set
	 */
	public void setStockMateriel(StockMateriel stockMateriel) {
		this.stockMateriel = stockMateriel;
	} 
	
	
}