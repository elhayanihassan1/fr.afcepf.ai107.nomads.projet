package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import javax.persistence.Table;

@Entity
@Table(name = "categorie_administrateur")
public class CategorieAdministrateur implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_categorie_administrateur")
	private int idCategorieAdministrateur;

	@Column(name = "libelle_categorie_administrateur")
	private String libelleCategorieAdministrateur;
	
	@OneToMany(mappedBy = "categorieAdministrateur", cascade = CascadeType.PERSIST)
	private List<DatesPresenceCategorieAdministrateur> datesPresenceCategorie;

	public CategorieAdministrateur() {
		super();
		
	}

	public CategorieAdministrateur(int idCategorieAdministrateur, String libelleCategorieAdministrateur,
			List<DatesPresenceCategorieAdministrateur> datesPresenceCategorie) {
		super();
		this.idCategorieAdministrateur = idCategorieAdministrateur;
		this.libelleCategorieAdministrateur = libelleCategorieAdministrateur;
		this.datesPresenceCategorie = datesPresenceCategorie;
	}


	/**
	 * @return the idCategorieAdministrateur
	 */
	public int getIdCategorieAdministrateur() {
		return idCategorieAdministrateur;
	}

	/**
	 * @param idCategorieAdministrateur the idCategorieAdministrateur to set
	 */
	public void setIdCategorieAdministrateur(int idCategorieAdministrateur) {
		this.idCategorieAdministrateur = idCategorieAdministrateur;
	}

	/**
	 * @return the libelleCategorieAdministrateur
	 */
	public String getLibelleCategorieAdministrateur() {
		return libelleCategorieAdministrateur;
	}

	/**
	 * @param libelleCategorieAdministrateur the libelleCategorieAdministrateur to set
	 */
	public void setLibelleCategorieAdministrateur(String libelleCategorieAdministrateur) {
		this.libelleCategorieAdministrateur = libelleCategorieAdministrateur;
	}


	/**
	 * @return the datesPresenceCategorie
	 */
	public List<DatesPresenceCategorieAdministrateur> getDatesPresenceCategorie() {
		return datesPresenceCategorie;
	}

	/**
	 * @param datesPresenceCategorie the datesPresenceCategorie to set
	 */
	public void setDatesPresenceCategorie(List<DatesPresenceCategorieAdministrateur> datesPresenceCategorie) {
		this.datesPresenceCategorie = datesPresenceCategorie;
	}

	
}