package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import javax.persistence.Table;

@Entity
@Table(name = "type_partenaire")
public class TypePartenaire implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_type_partenaire")
	private Integer idTypePartenaire;

	@Column(name = "libelle_type_partenaire")
	private String libelleTypePartenaire;

	@OneToMany(mappedBy = "typePartenaire", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	private List<Partenaire> partenaire;

	/**
	 * 
	 */
	public TypePartenaire() {
		super();
	}

	/**
	 * @param idTypePartenaire
	 * @param libelleTypePartenaire
	 * @param partenaire
	 */
	public TypePartenaire(Integer idTypePartenaire, String libelleTypePartenaire, List<Partenaire> partenaire) {
		super();
		this.idTypePartenaire = idTypePartenaire;
		this.libelleTypePartenaire = libelleTypePartenaire;
		this.partenaire = partenaire;
	}

	/**
	 * @return the idTypePartenaire
	 */
	public Integer getIdTypePartenaire() {
		return idTypePartenaire;
	}

	/**
	 * @param idTypePartenaire the idTypePartenaire to set
	 */
	public void setIdTypePartenaire(Integer idTypePartenaire) {
		this.idTypePartenaire = idTypePartenaire;
	}

	/**
	 * @return the libelleTypePartenaire
	 */
	public String getLibelleTypePartenaire() {
		return libelleTypePartenaire;
	}

	/**
	 * @param libelleTypePartenaire the libelleTypePartenaire to set
	 */
	public void setLibelleTypePartenaire(String libelleTypePartenaire) {
		this.libelleTypePartenaire = libelleTypePartenaire;
	}

	/**
	 * @return the partenaire
	 */
	public List<Partenaire> getPartenaire() {
		return partenaire;
	}

	/**
	 * @param partenaire the partenaire to set
	 */
	public void setPartenaire(List<Partenaire> partenaire) {
		this.partenaire = partenaire;
	}

	@Override
	public String toString() {
		return "TypePartenaire [idTypePartenaire=" + idTypePartenaire + ", libelleTypePartenaire="
				+ libelleTypePartenaire + ", partenaire=" + partenaire + "]";
	}

	
}