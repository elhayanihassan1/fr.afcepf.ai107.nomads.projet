package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "sexe")
public class SexeOld implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_sexe")
	private int idSexe;
	@Column(name = "libelle_sexe")
	private String libelleSexe;
	@OneToMany(mappedBy = "sexe", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<MasseurOld> masseurs;

	public SexeOld() {
		super();
	}

	public SexeOld(Integer idSexe, String libelleSexe, Set<MasseurOld> masseurs) {
		super();
		this.idSexe = idSexe;
		this.libelleSexe = libelleSexe;
		this.masseurs = masseurs;
	}

	public Integer getIdSexe() {
		return idSexe;
	}

	public void setIdSexe(Integer idSexe) {
		this.idSexe = idSexe;
	}

	public String getLibelleSexe() {
		return libelleSexe;
	}

	public void setLibelleSexe(String libelleSexe) {
		this.libelleSexe = libelleSexe;
	}

	public Set<MasseurOld> getMasseurs() {
		return masseurs;
	}

	public void setMasseurs(Set<MasseurOld> masseurs) {
		this.masseurs = masseurs;
	}

}
