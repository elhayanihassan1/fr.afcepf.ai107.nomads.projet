package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import javax.persistence.Table;

@Entity
@Table(name = "motif_indisponibilite_partenaire")
public class MotifIndisponibilitePartenaire implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_motif_indisponibilite_partenaire")
	private int idMotifIndisponibilite_Partenaire;

	@Column(name = "libelle_motif_indisponibilite_partenaire")
	private String libelleMotifIndisponibilitePartenaire;
	
	@OneToMany(mappedBy = "motifIndisponibilitePartenaire", cascade = CascadeType.PERSIST)
	List <IndisponibilitePartenaire> indisponibilitePartenaire;

	/**
	 * 
	 */
	public MotifIndisponibilitePartenaire() {
		super();
	}

	/**
	 * @param idMotifIndisponibilite_Partenaire
	 * @param libelleMotifIndisponibilitePartenaire
	 * @param indisponibilitePartenaire
	 */
	public MotifIndisponibilitePartenaire(int idMotifIndisponibilite_Partenaire,
			String libelleMotifIndisponibilitePartenaire, List<IndisponibilitePartenaire> indisponibilitePartenaire) {
		super();
		this.idMotifIndisponibilite_Partenaire = idMotifIndisponibilite_Partenaire;
		this.libelleMotifIndisponibilitePartenaire = libelleMotifIndisponibilitePartenaire;
		this.indisponibilitePartenaire = indisponibilitePartenaire;
	}

	/**
	 * @return the idMotifIndisponibilite_Partenaire
	 */
	public int getIdMotifIndisponibilite_Partenaire() {
		return idMotifIndisponibilite_Partenaire;
	}

	/**
	 * @param idMotifIndisponibilite_Partenaire the idMotifIndisponibilite_Partenaire to set
	 */
	public void setIdMotifIndisponibilite_Partenaire(int idMotifIndisponibilite_Partenaire) {
		this.idMotifIndisponibilite_Partenaire = idMotifIndisponibilite_Partenaire;
	}

	/**
	 * @return the libelleMotifIndisponibilitePartenaire
	 */
	public String getLibelleMotifIndisponibilitePartenaire() {
		return libelleMotifIndisponibilitePartenaire;
	}

	/**
	 * @param libelleMotifIndisponibilitePartenaire the libelleMotifIndisponibilitePartenaire to set
	 */
	public void setLibelleMotifIndisponibilitePartenaire(String libelleMotifIndisponibilitePartenaire) {
		this.libelleMotifIndisponibilitePartenaire = libelleMotifIndisponibilitePartenaire;
	}

	/**
	 * @return the indisponibilitePartenaire
	 */
	public List<IndisponibilitePartenaire> getIndisponibilitePartenaire() {
		return indisponibilitePartenaire;
	}

	/**
	 * @param indisponibilitePartenaire the indisponibilitePartenaire to set
	 */
	public void setIndisponibilitePartenaire(List<IndisponibilitePartenaire> indisponibilitePartenaire) {
		this.indisponibilitePartenaire = indisponibilitePartenaire;
	}

	
}