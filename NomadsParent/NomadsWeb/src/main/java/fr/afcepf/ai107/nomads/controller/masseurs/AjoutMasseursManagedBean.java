package fr.afcepf.ai107.nomads.controller.masseurs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import fr.afcepf.ai107.nomads.entities.AffectationCategorieMasseur;
import fr.afcepf.ai107.nomads.entities.ContratMasseur;
import fr.afcepf.ai107.nomads.entities.HistoriqueEvolutionProfessionnelleMasseur;
import fr.afcepf.ai107.nomads.entities.Masseur;
import fr.afcepf.ai107.nomads.entities.Sexe;
import fr.afcepf.ai107.nomads.masseurs.ibusiness.MasseurIBusiness;
import fr.afcepf.ai107.nomads.outils.ibusiness.OutilsIBusiness;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@ManagedBean (name = "mbAjoutMasseurs")
@ViewScoped
public class AjoutMasseursManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@EJB
	private MasseurIBusiness proxyMasseurBusiness;
	
	@EJB
	private OutilsIBusiness proxyOutilBusiness;
	

	private Masseur masseur = new Masseur();
	private Integer id_masseur;
	private String nomMasseur, prenomMasseur, photoMasseur;
	private String adresseMailMasseur, telephoneMasseur,adressePostaleMasseur;
	private String loginMasseur,passwordMasseur;

	private List<String> codesPostaux = new ArrayList<String>();
	private List<String> villes = new ArrayList<String>();
	private String selectedCP;
	private String selectedVille;

	
	private Date dateDeNaissanceMasseur, datePremierEnregistrementMasseur, dateDepartDefinitifMasseur;
	private int idSexeRecup;
	private AffectationCategorieMasseur affectCategoMass;
	private ContratMasseur nouveauContratM = null;
	
	
	private List<Sexe> listeSexe = new ArrayList<Sexe>();
	
	
	
	public void onCPChange() { 
		if (selectedCP !=null && !selectedCP.equals("")) {
			villes = proxyOutilBusiness.listeVilleByCp(selectedCP);
		}else {
			villes = new ArrayList<String>();
		}
	}
	
	@PostConstruct
	public void init() {
		
		listeSexe = proxyMasseurBusiness.getSexes();
		codesPostaux = proxyOutilBusiness.listeCp();
	
	}
	
	
	
	/**
	 * Fonction permettant l'ajout d'un nouveau masseur en base de donnée via la récupération de champs de saisie
	 * @return Crée un masseur avec Nom, prénom, adresse mail, téléphone, adresse postale, date de naissance, sexe, un login et un password. Ajoute une photo aléatoire en fonction du sexe
	 * et enregistre la création à la date de l'action.
	 */
	public String ajouterMasseur () {
		masseur.setNomMasseur(nomMasseur);
		masseur.setPrenomMasseur(prenomMasseur);
		masseur.setAdresseMailMasseur(adresseMailMasseur);
		masseur.setTelephoneMasseur(telephoneMasseur);
		masseur.setAdressePostaleMasseur(adressePostaleMasseur);
		masseur.setDateDeNaissanceMasseur(dateDeNaissanceMasseur);
		if(selectedVille != null) {
		masseur.setVilleCp(proxyOutilBusiness.VilleCpParNomVille(selectedVille)); 
		}
		masseur.setSexe(proxyMasseurBusiness.getSexeById(idSexeRecup));
		switch (idSexeRecup) {
		case 1:
			int i = new Random().nextInt(5)+1;
			photoMasseur = "une_masseuse"+i;			
			break;
		case 2:
			int j = new Random().nextInt(3)+1;
			photoMasseur = "un_masseur"+j;			
			break;
		case 3:
			int k = new Random().nextInt(3)+1;
			photoMasseur = "indeter"+k;			
			break;

		default:
			break;
		}
		masseur.setPhotoMasseur(photoMasseur);
		datePremierEnregistrementMasseur = new Date();
		masseur.setDatePremierEnregistrementMasseur(datePremierEnregistrementMasseur);
		masseur.setLoginMasseur(loginMasseur);
		masseur.setPasswordMasseur(passwordMasseur);
		
		List<HistoriqueEvolutionProfessionnelleMasseur> listeEv = new ArrayList<HistoriqueEvolutionProfessionnelleMasseur>();
		HistoriqueEvolutionProfessionnelleMasseur histoEvoPro = new HistoriqueEvolutionProfessionnelleMasseur(null, new Date(), null, masseur, proxyOutilBusiness.getEvoProMasseurById(1));
		listeEv.add(histoEvoPro);
		masseur.setEvolutionsProfessionnelles(listeEv);
		
		List<AffectationCategorieMasseur>listeAffect = new ArrayList<AffectationCategorieMasseur>();
		AffectationCategorieMasseur affectCategoMass = new AffectationCategorieMasseur(null, new Date(), null, masseur, proxyOutilBusiness.getCategoMasseurById(1));
		listeAffect.add(affectCategoMass);
		masseur.setAffectactionsCategorieMasseur(listeAffect);
		
		List<ContratMasseur> listeContrats = new ArrayList<ContratMasseur>();
		nouveauContratM = new ContratMasseur(null, new Date(), null, masseur, null);
		listeContrats.add(nouveauContratM);
		masseur.setContratsMasseur(listeContrats);
		
		proxyMasseurBusiness.ajouterMasseur(masseur);
		
		 		
		return "/MesMasseurs.xhtml?faces-redirect=true";
	}
	
	public void demo () {
		System.out.println("J'arrive dans la démo!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		nomMasseur = "Antonin";
		prenomMasseur = "Duraz";
		adressePostaleMasseur = "3 ter rue de la petite démonstration";
		selectedCP = "75001";
		
		selectedVille = "PARIS 1er arr.";
		adresseMailMasseur = "antoraz5@hotmail.fr";
		telephoneMasseur = "02.18.00.00.00";
		String pattern = "dd-MM-yyyy";
		String datedate="18-03-1993";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		Date dateN = new Date();
		try {
		dateN = simpleDateFormat.parse(datedate);
		} catch (ParseException e) {
		e.printStackTrace();
		}
		dateDeNaissanceMasseur = dateN;
									
		idSexeRecup = 2;
		loginMasseur = "mass";
		passwordMasseur = "sseur";
		selectedCP = "75002";
		selectedCP = "75001";
		System.out.println("j'ai affecté une valeur " + nomMasseur);
		
	}
	
	public Masseur getMasseur() {
		return masseur;
	}
	public void setMasseur(Masseur masseur) {
		this.masseur = masseur;
	}
	public int getId_masseur() {
		return id_masseur;
	}
	public void setId_masseur(int id_masseur) {
		this.id_masseur = id_masseur;
	}
	public String getNomMasseur() {
		return nomMasseur;
	}
	public void setNomMasseur(String nomMasseur) {
		this.nomMasseur = nomMasseur;
	}
	public String getPrenomMasseur() {
		return prenomMasseur;
	}
	public void setPrenomMasseur(String prenomMasseur) {
		this.prenomMasseur = prenomMasseur;
	}
	public String getPhotoMasseur() {
		return photoMasseur;
	}
	public void setPhotoMasseur(String photoMasseur) {
		this.photoMasseur = photoMasseur;
	}
	public String getAdresseMailMasseur() {
		return adresseMailMasseur;
	}
	public void setAdresseMailMasseur(String adresseMailMasseur) {
		this.adresseMailMasseur = adresseMailMasseur;
	}
	public String getTelephoneMasseur() {
		return telephoneMasseur;
	}
	public void setTelephoneMasseur(String telephoneMasseur) {
		this.telephoneMasseur = telephoneMasseur;
	}
	public String getAdressePostaleMasseur() {
		return adressePostaleMasseur;
	}
	public void setAdressePostaleMasseur(String adressePostaleMasseur) {
		this.adressePostaleMasseur = adressePostaleMasseur;
	}
	public String getLoginMasseur() {
		return loginMasseur;
	}
	public void setLoginMasseur(String loginMasseur) {
		this.loginMasseur = loginMasseur;
	}
	public String getPasswordMasseur() {
		return passwordMasseur;
	}
	public void setPasswordMasseur(String passwordMasseur) {
		this.passwordMasseur = passwordMasseur;
	}
	public Date getDateDeNaissanceMasseur() {
		return dateDeNaissanceMasseur;
	}
	public void setDateDeNaissanceMasseur(Date dateDeNaissanceMasseur) {
		this.dateDeNaissanceMasseur = dateDeNaissanceMasseur;
	}
	public Date getDatePremierEnregistrementMasseur() {
		return datePremierEnregistrementMasseur;
	}
	public void setDatePremierEnregistrementMasseur(Date datePremierEnregistrementMasseur) {
		this.datePremierEnregistrementMasseur = datePremierEnregistrementMasseur;
	}
	public Date getDateDepartDefinitifMasseur() {
		return dateDepartDefinitifMasseur;
	}
	public void setDateDepartDefinitifMasseur(Date dateDepartDefinitifMasseur) {
		this.dateDepartDefinitifMasseur = dateDepartDefinitifMasseur;
	}
	
	public List<Sexe> getListeSexe() {
		return listeSexe;
	}

	public void setListeSexe(List<Sexe> listeSexe) {
		this.listeSexe = listeSexe;
	}
	
	public List<String> getCodesPostaux() {
		return codesPostaux;
	}


	public void setCodesPostaux(List<String> codesPostaux) {
		this.codesPostaux = codesPostaux;
	}


	public List<String> getVilles() {
		return villes;
	}


	public void setVilles(List<String> villes) {
		this.villes = villes;
	}


	public String getSelectedCP() {
		return selectedCP;
	}


	public void setSelectedCP(String selectedCP) {
		this.selectedCP = selectedCP;
	}


	public String getSelectedVille() {
		return selectedVille;
	}


	public void setSelectedVille(String selectedVille) {
		this.selectedVille = selectedVille;
	}
	
	public int getIdSexeRecup() {
		return idSexeRecup;
	}

	public void setIdSexeRecup(int idSexeRecup) {
		this.idSexeRecup = idSexeRecup;
	}


	public AffectationCategorieMasseur getAffectCategoMass() {
		return affectCategoMass;
	}

	public void setAffectCategoMass(AffectationCategorieMasseur affectCategoMass) {
		this.affectCategoMass = affectCategoMass;
	}
	

	
}
