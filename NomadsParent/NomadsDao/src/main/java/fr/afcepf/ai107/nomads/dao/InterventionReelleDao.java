package fr.afcepf.ai107.nomads.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.nomads.entities.InscriptionMasseurs;
import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.idao.InterventionReelleIDao;

@Remote(InterventionReelleIDao.class)
@Stateless
public class InterventionReelleDao extends GenericDao<InterventionReelle> implements InterventionReelleIDao {

	@PersistenceContext(unitName = "NomadsPU")
	private EntityManager em;
	
	/**
	 * Permet la récupération de toutes les interventions réelles non annulée, par id masseur
	 * jointure sur l'inscription masseur
	 */
	private final String REQ_GET_ALL_INTERV_REELLE_BY_ID_MASSEUR = "SELECT distinct ir, im FROM InterventionReelle ir INNER JOIN FETCH ir.inscriptionMasseur im WHERE im.masseur.id_masseur= :paramIdMasseur AND im.dateDesinscriptionMasseur is null"; 
	/**
	 * Permet la récupération de toutes les interventions réelles non pourvues
	 */
	private final String REQ_GET_ALL_INTERV_REELLE_NON_POURVUES = "SELECT ir FROM InterventionReelle ir WHERE ir.datePrestation > CURRENT_DATE AND ir.idInterventionReelle NOT IN (SELECT im.interventionReelle.idInterventionReelle FROM InscriptionMasseurs im)";
	/**
	 * Selection des interventions d'un masseur idMasseur avec jonxtion sur les informations de prestation
	 */
	private final String REQ_GET_ALL_INFOS_INTERV_REELLE ="SELECT distinct ir, p FROM InterventionReelle ir INNER JOIN FETCH ir.prestation p WHERE ir IN :paramIntervReelleCompl";
	private final String REQ_GET_ALL_INTERV_REELLE_A_VENIR_BY_ID_MASSEUR = "SELECT distinct ir, im FROM InterventionReelle ir INNER JOIN FETCH ir.inscriptionMasseur im WHERE im.masseur.id_masseur= :paramIdMasseur AND im.dateDesinscriptionMasseur is null AND ir.datePrestation >= CURRENT_DATE AND ir.dateAnnulationIntervention is null"; 
	private final String REQ_GET_ALL_INTERV_REELLE_PASSEES_BY_ID_MASSEUR = "SELECT distinct ir, im FROM InterventionReelle ir INNER JOIN FETCH ir.inscriptionMasseur im WHERE im.masseur.id_masseur= :paramIdMasseur AND im.dateDesinscriptionMasseur is null AND ir.datePrestation < CURRENT_DATE AND ir.dateAnnulationIntervention is null"; 

	private final String REQ_GET_ALL_INSCRI_MASSEUR_BY_ID_INTERVENTION = "SELECT ins FROM InscriptionMasseurs ins INNER JOIN FETCH ins.interventionReelle intR WHERE ins.dateDesinscriptionMasseur is null and intR.idInterventionReelle = :idInterv";
	
	private final String REQ_GET_ALL_INTERV_REELLE_A_VENIR_BY_ID_MASSEUR3J = "SELECT distinct ir, im FROM InterventionReelle ir INNER JOIN FETCH ir.inscriptionMasseur im WHERE im.masseur.id_masseur= :paramIdMasseur AND im.dateDesinscriptionMasseur is null AND ir.datePrestation > CURRENT_DATE AND ir.datePrestation < (CURRENT_DATE + 3)"; 
	private final String REQ_GET_ALL_INTERV_7_DAYS ="SELECT ir FROM InterventionReelle ir WHERE ir.datePrestation>=CURRENT_DATE AND ir.datePrestation <= (CURRENT_DATE + 7) AND ir.prestation.dateAnnulationPrestation is null";
	private final String REQ_GET_ALL_INTERV_TODAY ="SELECT ir FROM InterventionReelle ir WHERE ir.datePrestation = CURRENT_DATE AND ir.prestation.dateAnnulationPrestation is null";
	
	private final String REQ_GET_ALL_INTERV_REELLE_NON_POURVUES_NEXT_7_DAYS = "SELECT ir FROM InterventionReelle ir WHERE ir.datePrestation > CURRENT_DATE AND ir.datePrestation <= (CURRENT_DATE + 7) AND ir.idInterventionReelle NOT IN (SELECT im.interventionReelle.idInterventionReelle FROM InscriptionMasseurs im)";
	private final String REQ_GET_ALL_INTERV_REELLE_BY_ID_MASSEUR_TODAY = "SELECT distinct ir, im FROM InterventionReelle ir INNER JOIN FETCH ir.inscriptionMasseur im WHERE im.masseur.id_masseur= :paramIdMasseur AND im.dateDesinscriptionMasseur is null AND ir.datePrestation = CURRENT_DATE"; 

	private final String REQ_GET_ALL_INTERV_3_DAYS_By_PART ="SELECT ir FROM InterventionReelle ir WHERE ir.datePrestation>=CURRENT_DATE AND ir.datePrestation <= (CURRENT_DATE + 3) AND ir.prestation.dateAnnulationPrestation is null AND ir.prestation.partenaire.idPartenaire= :paramId";
	private final String REQ_GET_ALL_INTERV_TODAY_By_PART ="SELECT ir FROM InterventionReelle ir WHERE ir.datePrestation>=CURRENT_DATE AND ir.datePrestation <= (CURRENT_DATE + 3) AND ir.prestation.dateAnnulationPrestation is null AND ir.prestation.partenaire.idPartenaire= :paramId";

	
	
//	String test = "SELECT im.masseur.id_masseur,im.dateDesinscriptionMasseur FROM InscriptionMasseurs im";
//	String test2 = "SELECT ir.datePrestation FROM InterventionReelle ir";

	private final String REQ_GET_INTERVENTION_REELLE_BY_ID = "SELECT i FROM InterventionReelle i WHERE i.idInterventionReelle = :paramId";
	
	/**
	 * Permet la récupération des interventions non annulées d'un masseur d'id idMasseur
	 * Dans un premier temps: sélection de toutes les interventions réeelles non annulées pour ce masseur
	 */
	@Override
	public List<InterventionReelle> getAllInterventionsByIdMasseur(Integer idMasseur) {
		List<InterventionReelle> interventionsCompletes = null;
		Query requete1 = em.createQuery(REQ_GET_ALL_INTERV_REELLE_A_VENIR_BY_ID_MASSEUR);
		requete1.setParameter("paramIdMasseur", idMasseur);
		interventionsCompletes = requete1.getResultList();
		System.out.println("########################J'ai fini la requete 1; taille de la liste retournée : "+ interventionsCompletes.size() +  "//////////////////////////////////////////////");
		if (interventionsCompletes.size()>0) {
		Query requete2 = em.createQuery(REQ_GET_ALL_INFOS_INTERV_REELLE);
		requete2.setParameter("paramIntervReelleCompl", interventionsCompletes);
		interventionsCompletes = requete2.getResultList();
		System.out.println("########################J'ai chargé la requete 2!! taille de la liste en retour: " + interventionsCompletes.size() + "  //////////////////////////////////////////////");
		}
		else {
			interventionsCompletes = null;
			System.out.println("###########################Je retourne une liste nulle!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		}
		return interventionsCompletes;
	}


	@Override
	public List<InterventionReelle> getAllInterventionsAVenirByIdMasseur(Integer idMasseur) {
		List<InterventionReelle> interventionsCompletes = null;
		Query requete1 = em.createQuery(REQ_GET_ALL_INTERV_REELLE_A_VENIR_BY_ID_MASSEUR);
		requete1.setParameter("paramIdMasseur", idMasseur);
		interventionsCompletes = requete1.getResultList();

		if (interventionsCompletes.size()>0) {
		Query requete2 = em.createQuery(REQ_GET_ALL_INFOS_INTERV_REELLE);
		requete2.setParameter("paramIntervReelleCompl", interventionsCompletes);
		interventionsCompletes = requete2.getResultList();

		}
		else {
			interventionsCompletes = null;
			System.out.println("###########################Je retourne une liste nulle!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		}
		return interventionsCompletes;
	}
	
	@Override
	public List<InterventionReelle> getAllInterventionsNonPourvues() {
		List<InterventionReelle> interventionsNonPourvues = null;
		Query req = em.createQuery(REQ_GET_ALL_INTERV_REELLE_NON_POURVUES);
	
		interventionsNonPourvues = req.getResultList();

		return interventionsNonPourvues;
	}



	@Override
	public List<InscriptionMasseurs> getAllInscriMasseurByIdIntervention(Integer idIntervention) {
		List<InscriptionMasseurs> listMassInscr = null;
		Query requete = em.createQuery(REQ_GET_ALL_INSCRI_MASSEUR_BY_ID_INTERVENTION);
		requete.setParameter("idInterv", idIntervention);
		listMassInscr = requete.getResultList();
		
		return listMassInscr;
	}


	@Override
	public List<InterventionReelle> getAllInterventions3JByIdMasseur(Integer idMasseur) {
		List<InterventionReelle> interventionsCompletes = null;
		Query requete1 = em.createQuery(REQ_GET_ALL_INTERV_REELLE_A_VENIR_BY_ID_MASSEUR3J);
		requete1.setParameter("paramIdMasseur", idMasseur);
		interventionsCompletes = requete1.getResultList();

		if (interventionsCompletes.size()>0) {
		Query requete2 = em.createQuery(REQ_GET_ALL_INFOS_INTERV_REELLE);
		requete2.setParameter("paramIntervReelleCompl", interventionsCompletes);
		interventionsCompletes = requete2.getResultList();
		}
		else {
			interventionsCompletes = null;
		}
		return interventionsCompletes;
	}


	@Override
	public InterventionReelle getInterventionReelleById(Integer idIntervention){
		List <InterventionReelle> interv = null;
		Query query = em.createQuery(REQ_GET_INTERVENTION_REELLE_BY_ID);
		query.setParameter("paramId", idIntervention);
		interv = query.getResultList();
		return interv.get(0);
		
	}


	@Override
	public List<InterventionReelle> getAllInterventionsPasseesByIdMasseur(Integer idMasseur) {
		List<InterventionReelle> interventionsCompletes = null;
		Query requete1 = em.createQuery(REQ_GET_ALL_INTERV_REELLE_PASSEES_BY_ID_MASSEUR);
		requete1.setParameter("paramIdMasseur", idMasseur);
		interventionsCompletes = requete1.getResultList();

		if (interventionsCompletes.size()>0) {
		Query requete2 = em.createQuery(REQ_GET_ALL_INFOS_INTERV_REELLE);
		requete2.setParameter("paramIntervReelleCompl", interventionsCompletes);
		interventionsCompletes = requete2.getResultList();

		}
		else {
			interventionsCompletes = null;
			System.out.println("###########################Je retourne une liste nulle!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		}
		return interventionsCompletes;
	}


	@Override
	public List<InterventionReelle> getAllInterventionFoNext7DAys() {
		List<InterventionReelle> interventions = null;
		Query requete = em.createQuery(REQ_GET_ALL_INTERV_7_DAYS);
		interventions = requete.getResultList();
		return interventions;
	}


	@Override
	public List<InterventionReelle> getAllTodaySInterventions() {
		List<InterventionReelle> interventions = null;
		Query requete = em.createQuery(REQ_GET_ALL_INTERV_TODAY);
		interventions = requete.getResultList();
		return interventions;
	}


	@Override
	public List<InterventionReelle> getAllInterventionsNonPourvuesNext7Days() {
		List<InterventionReelle> interventionsNonPourvues = null;
		Query req = em.createQuery(REQ_GET_ALL_INTERV_REELLE_NON_POURVUES_NEXT_7_DAYS);
	
		interventionsNonPourvues = req.getResultList();

		return interventionsNonPourvues;
	}


	@Override
	public List<InterventionReelle> getAllInterventionsByIdMasseurToday(Integer idMasseur) {
		List<InterventionReelle> interventionsCompletes = null;
		Query requete1 = em.createQuery(REQ_GET_ALL_INTERV_REELLE_BY_ID_MASSEUR_TODAY);
		requete1.setParameter("paramIdMasseur", idMasseur);
		interventionsCompletes = requete1.getResultList();

		if (interventionsCompletes.size()>0) {
		Query requete2 = em.createQuery(REQ_GET_ALL_INFOS_INTERV_REELLE);
		requete2.setParameter("paramIntervReelleCompl", interventionsCompletes);
		interventionsCompletes = requete2.getResultList();

		}
		else {
			interventionsCompletes = null;
		}
		return interventionsCompletes;
	}


	@Override
	public List<InterventionReelle> getAllInterventionFoNext3DAysByIdPart(Integer idPartenaire) {
		List<InterventionReelle> interventions = null;
		Query requete = em.createQuery(REQ_GET_ALL_INTERV_3_DAYS_By_PART);
		requete.setParameter("paramId", idPartenaire);
		interventions = requete.getResultList();
		return interventions;
	}


	@Override
	public List<InterventionReelle> getAllInterventionFoTodayByIdPart(Integer idPartenaire) {
		List<InterventionReelle> interventions = null;
		Query requete = em.createQuery(REQ_GET_ALL_INTERV_TODAY_By_PART);
		requete.setParameter("paramId", idPartenaire);
		interventions = requete.getResultList();
		return interventions;
	}

}
