package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import javax.persistence.Table;

@Entity
@Table(name = "contrat_partenaire")

public class ContratPartenaire implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_contrat_partenaire")
	private int idContratPartenaire;

	@Column(name = "date_debut_contrat_partenaire")
	private Date dateDebutContratPartenaire;

	@Column(name = "date_fin_contrat_partenaire")
	private Date dateFinContratPartenaire;
	
	@ManyToOne
	@JoinColumn (name = "id_motif_fin_contrat_partenaire", referencedColumnName = "id_motif_fin_contrat_partenaire")
	private MotifFinContratPartenaire motifFinContratPartenaire;
	
	@ManyToOne
	@JoinColumn(name = "id_partenaire", referencedColumnName = "id_partenaire")
	private Partenaire partenaire;

	
	public ContratPartenaire() {
		super();
	}
	
	

	public ContratPartenaire(int idContratPartenaire, Date dateDebutContratPartenaire, Date dateFinContratPartenaire,
			MotifFinContratPartenaire motifFinContratPartenaire, Partenaire partenaire) {
		super();
		this.idContratPartenaire = idContratPartenaire;
		this.dateDebutContratPartenaire = dateDebutContratPartenaire;
		this.dateFinContratPartenaire = dateFinContratPartenaire;
		this.motifFinContratPartenaire = motifFinContratPartenaire;
		this.partenaire = partenaire;
	}



	/**
	 * @return the idContratPartenaire
	 */
	public int getIdContratPartenaire() {
		return idContratPartenaire;
	}

	/**
	 * @param idContratPartenaire the idContratPartenaire to set
	 */
	public void setIdContratPartenaire(int idContratPartenaire) {
		this.idContratPartenaire = idContratPartenaire;
	}

	/**
	 * @return the dateDebutContratPartenaire
	 */
	public Date getDateDebutContratPartenaire() {
		return dateDebutContratPartenaire;
	}

	/**
	 * @param dateDebutContratPartenaire the dateDebutContratPartenaire to set
	 */
	public void setDateDebutContratPartenaire(Date dateDebutContratPartenaire) {
		this.dateDebutContratPartenaire = dateDebutContratPartenaire;
	}

	/**
	 * @return the dateFinContratPartenaire
	 */
	public Date getDateFinContratPartenaire() {
		return dateFinContratPartenaire;
	}

	/**
	 * @param dateFinContratPartenaire the dateFinContratPartenaire to set
	 */
	public void setDateFinContratPartenaire(Date dateFinContratPartenaire) {
		this.dateFinContratPartenaire = dateFinContratPartenaire;
	}

	/**
	 * @return the motifFinContratPartenaire
	 */
	public MotifFinContratPartenaire getMotifFinContratPartenaire() {
		return motifFinContratPartenaire;
	}

	/**
	 * @param motifFinContrat the motifFinContratPartenaire to set
	 */
	public void setMotifFinContratPartenaire(MotifFinContratPartenaire motifFinContrat) {
		this.motifFinContratPartenaire = motifFinContrat;
	}

	/**
	 * @return the partenaire
	 */
	public Partenaire getPartenaire() {
		return partenaire;
	}

	/**
	 * @param partenaire the partenaire to set
	 */
	public void setPartenaire(Partenaire partenaire) {
		this.partenaire = partenaire;
	}

	
}