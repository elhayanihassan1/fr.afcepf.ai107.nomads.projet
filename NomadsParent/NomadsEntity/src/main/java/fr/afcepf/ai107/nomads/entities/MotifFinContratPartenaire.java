package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import javax.persistence.Table;

@Entity
@Table(name = "motif_fin_contrat_partenaire")
public class MotifFinContratPartenaire implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_motif_fin_contrat_partenaire")
	private int idMotifFinContratPartenaire;

	@Column(name = "libelle_motif_fin_contrat_partenaire")
	private String libelleMotifFinContratPartenaire;
	
	@OneToMany (mappedBy = "motifFinContratPartenaire", cascade = CascadeType.PERSIST)
	private List<ContratPartenaire> contratPartenaire;
	
	

	public MotifFinContratPartenaire() {
		super();
		
	}

	
	public MotifFinContratPartenaire(int idMotifFinContratPartenaire, String libelleMotifFinContratPartenaire,
			List<ContratPartenaire> contratPartenaire) {
		super();
		this.idMotifFinContratPartenaire = idMotifFinContratPartenaire;
		this.libelleMotifFinContratPartenaire = libelleMotifFinContratPartenaire;
		this.contratPartenaire = contratPartenaire;
	}


	/**
	 * @return the idMotifFinContratPartenaire
	 */
	public int getIdMotifFinContratPartenaire() {
		return idMotifFinContratPartenaire;
	}

	/**
	 * @param idMotifFinContratPartenaire the idMotifFinContratPartenaire to set
	 */
	public void setIdMotifFinContratPartenaire(int idMotifFinContratPartenaire) {
		this.idMotifFinContratPartenaire = idMotifFinContratPartenaire;
	}

	/**
	 * @return the libelleMotifFinContratPartenaire
	 */
	public String getLibelleMotifFinContratPartenaire() {
		return libelleMotifFinContratPartenaire;
	}

	/**
	 * @param libelleMotifFinContratPartenaire the libelleMotifFinContratPartenaire to set
	 */
	public void setLibelleMotifFinContratPartenaire(String libelleMotifFinContratPartenaire) {
		this.libelleMotifFinContratPartenaire = libelleMotifFinContratPartenaire;
	}

	/**
	 * @return the contratPartenaire
	 */
	public List<ContratPartenaire> getContratPartenaire() {
		return contratPartenaire;
	}

	/**
	 * @param contratPartenaire the contratPartenaire to set
	 */
	public void setContratPartenaire(List<ContratPartenaire> contratPartenaire) {
		this.contratPartenaire = contratPartenaire;
	}

	
}