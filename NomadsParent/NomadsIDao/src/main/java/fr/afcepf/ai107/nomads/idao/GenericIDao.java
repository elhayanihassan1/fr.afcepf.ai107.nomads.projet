package fr.afcepf.ai107.nomads.idao;

import java.util.List;

import fr.afcepf.ai107.nomads.entities.InterventionReelle;

public interface GenericIDao<T>{
	
	T add(T t);
	boolean delete(T t);
	T update(T t);
	T getById(int i);
	List<T> findAll();
	
}
