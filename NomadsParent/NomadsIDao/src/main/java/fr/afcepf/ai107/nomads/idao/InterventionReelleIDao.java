package fr.afcepf.ai107.nomads.idao;

import java.util.List;

import fr.afcepf.ai107.nomads.entities.InscriptionMasseurs;
import fr.afcepf.ai107.nomads.entities.InterventionReelle;

public interface InterventionReelleIDao extends GenericIDao<InterventionReelle> {

	List<InterventionReelle> getAllInterventionsByIdMasseur (Integer idMasseur);
	List<InterventionReelle> getAllInterventionsAVenirByIdMasseur (Integer idMasseur);
	InterventionReelle getInterventionReelleById(Integer idIntervention);
	List<InterventionReelle> getAllInterventionsNonPourvues();

	List<InscriptionMasseurs> getAllInscriMasseurByIdIntervention (Integer idIntervention);
	List<InterventionReelle> getAllInterventions3JByIdMasseur (Integer idMasseur);
	List<InterventionReelle> getAllInterventionsPasseesByIdMasseur(Integer idMasseur);	
	List<InterventionReelle> getAllInterventionFoNext7DAys ();
	List<InterventionReelle> getAllTodaySInterventions ();
	List<InterventionReelle> getAllInterventionsNonPourvuesNext7Days();

	List<InterventionReelle> getAllInterventionsByIdMasseurToday (Integer idMasseur);
	List<InterventionReelle> getAllInterventionFoNext3DAysByIdPart (Integer idPartenaire);
	List<InterventionReelle> getAllInterventionFoTodayByIdPart (Integer idPartenaire);


}

