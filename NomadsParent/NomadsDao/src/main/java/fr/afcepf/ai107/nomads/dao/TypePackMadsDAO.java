package fr.afcepf.ai107.nomads.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.nomads.entities.TypePackMads;
import fr.afcepf.ai107.nomads.idao.TypePackMadsIDao;

@Remote (TypePackMadsIDao.class)
@Stateless
public class TypePackMadsDAO extends GenericDao<TypePackMads> implements TypePackMadsIDao {
	
	@PersistenceContext (unitName = "NomadsPU")
	private EntityManager em;
	
	private final String REQ_ALL_NON_REMOVED_PACK = "SELECT tpm from TypePackMads tpm  WHERE tpm.removedTypePackMads = false";
	private final String REQ_ALL_REMOVED_PACK = "SELECT tpm from TypePackMads tpm  WHERE tpm.removedTypePackMads = true";
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TypePackMads> getAllNonRemovedPacks() {
		Query query = em.createQuery(REQ_ALL_NON_REMOVED_PACK);
		List <TypePackMads> allNonRemovedPack = query.getResultList();
		return allNonRemovedPack;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TypePackMads> getAllRemovedPacks() {
		Query query = em.createQuery(REQ_ALL_REMOVED_PACK);
		List <TypePackMads> allRemovedPack = query.getResultList();
		return allRemovedPack;
	}
}
