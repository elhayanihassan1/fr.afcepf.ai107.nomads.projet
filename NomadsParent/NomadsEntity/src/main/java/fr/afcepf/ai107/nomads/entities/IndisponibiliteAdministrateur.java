package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import javax.persistence.Table;

@Entity
@Table(name = "indisponibilite_administrateur")
public class IndisponibiliteAdministrateur implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_indisponibilite_administrateur")
	private int idIndisponibiliteAdministrateur;

	@Column(name = "date_debut_indisponibilite_administrateur")
	private Date dateDebutIndisponibiliteAdministrateur;

	@Column(name = "date_fin_indisponibilite_administrateur")
	private Date dateFinIndisponibiliteAdministrateur;

	@ManyToOne
	@JoinColumn(name = "id_administrateur", referencedColumnName = "id_administrateur")
	private Administrateur administrateur;

	/**
	 * 
	 */
	public IndisponibiliteAdministrateur() {
		super();
	}

	/**
	 * @param idIndisponibiliteAdministrateur
	 * @param dateDebutIndisponibiliteAdministrateur
	 * @param dateFinIndisponibiliteAdministrateur
	 * @param administrateur
	 */
	public IndisponibiliteAdministrateur(int idIndisponibiliteAdministrateur,
			Date dateDebutIndisponibiliteAdministrateur, Date dateFinIndisponibiliteAdministrateur,
			Administrateur administrateur) {
		super();
		this.idIndisponibiliteAdministrateur = idIndisponibiliteAdministrateur;
		this.dateDebutIndisponibiliteAdministrateur = dateDebutIndisponibiliteAdministrateur;
		this.dateFinIndisponibiliteAdministrateur = dateFinIndisponibiliteAdministrateur;
		this.administrateur = administrateur;
	}

	/**
	 * @return the idIndisponibiliteAdministrateur
	 */
	public int getIdIndisponibiliteAdministrateur() {
		return idIndisponibiliteAdministrateur;
	}

	/**
	 * @param idIndisponibiliteAdministrateur the idIndisponibiliteAdministrateur to set
	 */
	public void setIdIndisponibiliteAdministrateur(int idIndisponibiliteAdministrateur) {
		this.idIndisponibiliteAdministrateur = idIndisponibiliteAdministrateur;
	}

	/**
	 * @return the dateDebutIndisponibiliteAdministrateur
	 */
	public Date getDateDebutIndisponibiliteAdministrateur() {
		return dateDebutIndisponibiliteAdministrateur;
	}

	/**
	 * @param dateDebutIndisponibiliteAdministrateur the dateDebutIndisponibiliteAdministrateur to set
	 */
	public void setDateDebutIndisponibiliteAdministrateur(Date dateDebutIndisponibiliteAdministrateur) {
		this.dateDebutIndisponibiliteAdministrateur = dateDebutIndisponibiliteAdministrateur;
	}

	/**
	 * @return the dateFinIndisponibiliteAdministrateur
	 */
	public Date getDateFinIndisponibiliteAdministrateur() {
		return dateFinIndisponibiliteAdministrateur;
	}

	/**
	 * @param dateFinIndisponibiliteAdministrateur the dateFinIndisponibiliteAdministrateur to set
	 */
	public void setDateFinIndisponibiliteAdministrateur(Date dateFinIndisponibiliteAdministrateur) {
		this.dateFinIndisponibiliteAdministrateur = dateFinIndisponibiliteAdministrateur;
	}

	/**
	 * @return the administrateur
	 */
	public Administrateur getAdministrateur() {
		return administrateur;
	}

	/**
	 * @param administrateur the administrateur to set
	 */
	public void setAdministrateur(Administrateur administrateur) {
		this.administrateur = administrateur;
	}
	
	
}