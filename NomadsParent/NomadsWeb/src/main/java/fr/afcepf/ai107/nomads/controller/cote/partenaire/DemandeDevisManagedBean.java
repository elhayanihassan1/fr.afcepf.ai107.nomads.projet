package fr.afcepf.ai107.nomads.controller.cote.partenaire;

import java.util.ArrayList;
import java.util.Date;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import fr.afcepf.ai107.nomads.controller.email.emailAdmin;
import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.Partenaire;
import fr.afcepf.ai107.nomads.entities.Prestation;

import fr.afcepf.ai107.nomads.outils.ibusiness.OutilsIBusiness;
import fr.afcepf.ai107.nomads.partenaire.ibusiness.CotePartenaireIBusiness;
import fr.afcepf.ai107.nomads.partenaire.ibusiness.PartenaireIBusiness;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@ManagedBean(name = "mbDemandeDevisPart")
@ViewScoped
public class DemandeDevisManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private OutilsIBusiness proxyOutilBusiness;

	@EJB
	private CotePartenaireIBusiness proxyCotePartenaireIBusiness;
	@EJB
	private PartenaireIBusiness proxy;
	

	
	private Prestation prestation = new Prestation();
	@ManagedProperty(value = "#{mbAccount.partenaire}")
	private Partenaire partenaire;
	private Integer idPrestation;
	private String libellePrestation;
	private String adresseInterventionPrestation;
	private Integer nombreMasseursPrestation;
	private Integer nombreChaisesErgonomiquesPrestation;
	private Double coutDevisPrestation;
	private Integer idInterventionReelle;
	private int idPartenaire;
	private Date datePrestation;
	private Date heureDebutPrestation;
	private Date heureFinPrestation;
	private Date datePaiementPrestation;
	private Date dateEmissionPrestation = new Date();
	private String specificitesPrestation;

	private List<String> codesPostaux = new ArrayList<String>();
	private List<String> villes = new ArrayList<String>();
	private String selectedCP;
	private String selectedVille;

	private List<String> typePartenaire = new ArrayList<String>();
	private List<String> partenaires = new ArrayList<String>();
	private String selectedTypePartenaire;
	private String selectedPartenaire;
	private List<Partenaire> listePartenaire ;

	public void onCPChange() {
		if (selectedCP != null && !selectedCP.equals("")) {
			villes = proxyOutilBusiness.listeVilleByCp(selectedCP);
		} else {
			villes = new ArrayList<String>();
		}
	}

	@PostConstruct
	public void init() {

		codesPostaux = proxyOutilBusiness.listeCp();
		//listePartenaire=proxy.getAllPartenaires();
	}


	public String AjouterdemandeDevisPartenaire() throws Exception {
	
		// prestation.setPartenaire(partenaire.getNomPartenaire());
		prestation.setAdresseInterventionPrestation(adresseInterventionPrestation);
		prestation.setCoutDevisPrestation(coutDevisPrestation);
		prestation.setDateEmissionPrestation(dateEmissionPrestation);
		prestation.setHeureDebutPrestation(heureDebutPrestation);
		prestation.setHeureFinPrestation(heureFinPrestation);
		prestation.setNombreMasseursPrestation(nombreMasseursPrestation);
		prestation.setLibellePrestation(libellePrestation); // nom du partenaire
		prestation.setSpecificitesPrestation(specificitesPrestation);
		if (selectedVille != null) {
			prestation.setVilleCp(proxyOutilBusiness.VilleCpParNomVille(selectedVille));
		}
		
        prestation.setPartenaire(partenaire);
		List<InterventionReelle> interR = new ArrayList<InterventionReelle>();
		InterventionReelle interventionReelle = new InterventionReelle();
		interventionReelle.setDatePrestation(datePrestation);
		interventionReelle.setPrestation(prestation);
		interventionReelle.setIdInterventionReelle(null);
		interR.add(interventionReelle);
		prestation.setInterventionReelle(interR);
		proxyCotePartenaireIBusiness.ajoutDemandeDevisPartenaire(prestation);
		
		emailAdmin.sendMail("nomadsafcepf@gmail.com");
		partenaire = null;
		idPrestation = 0;
		libellePrestation = "";
		adresseInterventionPrestation = "";
		nombreMasseursPrestation = 0;
		nombreChaisesErgonomiquesPrestation = 0;
		coutDevisPrestation = 0.0;
		idInterventionReelle = 0;
		idPartenaire = 0;
		datePrestation = null;
		heureDebutPrestation = null;
		heureFinPrestation = null;
		datePaiementPrestation = null;
		dateEmissionPrestation = null;
	    selectedCP = null;
		selectedVille=null;
		specificitesPrestation=null;
      
		return "/MesPrestationsPartenaire.xhtml?faces-redirect=true";
	}
	
	 public void saveMessage() {
	        FacesContext context = FacesContext.getCurrentInstance();
	         
	        context.addMessage(null, new FacesMessage("Successful : ",  "votre demande a bien été envoyée") );

	    }

	 
	 public void demo() {
		 adresseInterventionPrestation = "5 rue Saint-Bernard";
		 nombreMasseursPrestation = 6;
		try{
			datePrestation = (new SimpleDateFormat("dd-MM-yyyy")).parse("02-06-2020");
		heureDebutPrestation = (new SimpleDateFormat("hh:mm")).parse("19:30") ;
		heureFinPrestation = (new SimpleDateFormat("hh:mm")).parse("22:45");
		} catch (ParseException e) {
			e.printStackTrace();	
		}
		specificitesPrestation = "La thématique sera sur le bleu au sein de la vie urbaine. Venir en bleu. Boissons soft offertes pendant le service!";
	 }
	 
     


	public String getSpecificitesPrestation() {
		return specificitesPrestation;
	}

	public void setSpecificitesPrestation(String specificitesPrestation) {
		this.specificitesPrestation = specificitesPrestation;
	}

	public List<Partenaire> getListePartenaire() {
		return listePartenaire;
	}

	public void setListePartenaire(List<Partenaire> listePartenaire) {
		this.listePartenaire = listePartenaire;
	}

	public int getIdPartenaire() {
		return idPartenaire;
	}

	public void setIdPartenaire(int idPartenaire) {
		this.idPartenaire = idPartenaire;
	}

	public Prestation getPrestation() {
		return prestation;
	}

	public void setPrestation(Prestation prestation) {
		this.prestation = prestation;
	}

	public Partenaire getPartenaire() {
		return partenaire;
	}

	public void setPartenaire(Partenaire partenaire) {
		this.partenaire = partenaire;
	}

	public Integer getIdPrestation() {
		return idPrestation;
	}

	public void setIdPrestation(Integer idPrestation) {
		this.idPrestation = idPrestation;
	}

	public String getLibellePrestation() {
		return libellePrestation;
	}

	public void setLibellePrestation(String libellePrestation) {
		this.libellePrestation = libellePrestation;
	}

	public String getAdresseInterventionPrestation() {
		return adresseInterventionPrestation;
	}

	public void setAdresseInterventionPrestation(String adresseInterventionPrestation) {
		this.adresseInterventionPrestation = adresseInterventionPrestation;
	}

	public Integer getNombreMasseursPrestation() {
		return nombreMasseursPrestation;
	}

	public void setNombreMasseursPrestation(Integer nombreMasseursPrestation) {
		this.nombreMasseursPrestation = nombreMasseursPrestation;
	}

	public Integer getNombreChaisesErgonomiquesPrestation() {
		return nombreChaisesErgonomiquesPrestation;
	}

	public void setNombreChaisesErgonomiquesPrestation(Integer nombreChaisesErgonomiquesPrestation) {
		this.nombreChaisesErgonomiquesPrestation = nombreChaisesErgonomiquesPrestation;
	}

	public Double getCoutDevisPrestation() {
		return coutDevisPrestation;
	}

	public void setCoutDevisPrestation(Double coutDevisPrestation) {
		this.coutDevisPrestation = coutDevisPrestation;
	}

	public Integer getIdInterventionReelle() {
		return idInterventionReelle;
	}

	public void setIdInterventionReelle(Integer idInterventionReelle) {
		this.idInterventionReelle = idInterventionReelle;
	}
    
	public Date getDatePrestation() {
		return datePrestation;
	}

	public void setDatePrestation(Date datePrestation) {
		this.datePrestation = datePrestation;
	}



	public Date getHeureDebutPrestation() {
		return heureDebutPrestation;
	}

	public void setHeureDebutPrestation(Date heureDebutPrestation) {
		this.heureDebutPrestation = heureDebutPrestation;
	}

	public Date getHeureFinPrestation() {
		return heureFinPrestation;
	}

	public void setHeureFinPrestation(Date heureFinPrestation) {
		this.heureFinPrestation = heureFinPrestation;
	}

	public Date getDatePaiementPrestation() {
		return datePaiementPrestation;
	}

	public void setDatePaiementPrestation(Date datePaiementPrestation) {
		this.datePaiementPrestation = datePaiementPrestation;
	}

	public Date getDateEmissionPrestation() {
		return dateEmissionPrestation;
	}

	public void setDateEmissionPrestation(Date dateEmissionPrestation) {
		this.dateEmissionPrestation = dateEmissionPrestation;
	}


	public List<String> getCodesPostaux() {
		return codesPostaux;
	}

	public void setCodesPostaux(List<String> codesPostaux) {
		this.codesPostaux = codesPostaux;
	}

	public List<String> getVilles() {
		return villes;
	}

	public void setVilles(List<String> villes) {
		this.villes = villes;
	}

	public String getSelectedCP() {
		return selectedCP;
	}

	public void setSelectedCP(String selectedCP) {
		this.selectedCP = selectedCP;
	}

	public String getSelectedVille() {
		return selectedVille;
	}

	public void setSelectedVille(String selectedVille) {
		this.selectedVille = selectedVille;
	}

	public List<String> getTypePartenaire() {
		return typePartenaire;
	}

	public void setTypePartenaire(List<String> typePartenaire) {
		this.typePartenaire = typePartenaire;
	}

	public List<String> getPartenaires() {
		return partenaires;
	}

	public void setPartenaires(List<String> partenaires) {
		this.partenaires = partenaires;
	}

	public String getSelectedTypePartenaire() {
		return selectedTypePartenaire;
	}

	public void setSelectedTypePartenaire(String selectedTypePartenaire) {
		this.selectedTypePartenaire = selectedTypePartenaire;
	}

	public String getSelectedPartenaire() {
		return selectedPartenaire;
	}

	public void setSelectedPartenaire(String selectedPartenaire) {
		this.selectedPartenaire = selectedPartenaire;
	}

}
