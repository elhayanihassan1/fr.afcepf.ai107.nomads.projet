package fr.afcepf.ai107.nomads.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.nomads.entities.Administrateur;
import fr.afcepf.ai107.nomads.idao.AdministrateurIDao;

@Remote(AdministrateurIDao.class)
@Stateless
public class AdministrateurDao  extends GenericDao<Administrateur> implements AdministrateurIDao{
	
	@PersistenceContext (unitName = "NomadsPU")
	private EntityManager em;
	
	
	@SuppressWarnings("unchecked")
	@Override
	public Administrateur authenticate(String login, String password) {
	
		Administrateur admin = null;
		List<Administrateur> admins = null;
		Query queryAdmin = em.createQuery("SELECT a  FROM  Administrateur a WHERE a.loginAdministrateur = :paramLogin AND a.passwordAdministrateur = :paramPassword");
		queryAdmin.setParameter("paramLogin", login);
		queryAdmin.setParameter("paramPassword", password);
		admins=queryAdmin.getResultList();
		if(admins.size() > 0) {
			admin= admins.get(0);
		}
		return admin;
	}

	
}
