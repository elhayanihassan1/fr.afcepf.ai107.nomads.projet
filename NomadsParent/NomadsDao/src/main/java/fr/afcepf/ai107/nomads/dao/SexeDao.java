package fr.afcepf.ai107.nomads.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.nomads.entities.Sexe;
import fr.afcepf.ai107.nomads.idao.SexeIDao;

@Remote(SexeIDao.class)
@Stateless
public class SexeDao extends GenericDao<Sexe> implements SexeIDao{

	@PersistenceContext(unitName = "NomadsPU")
	private EntityManager em;
	
	private String ReqGetAll = "SELECT s FROM Sexe s";
	
	@Override
	public List<Sexe> getAll() {
		List<Sexe> sexes = null;
		
		Query requete = em.createQuery(ReqGetAll);
		sexes = requete.getResultList();
		
		return sexes;
	}
}
