package fr.afcepf.ai107.nomads.idao;

import java.util.List;

import fr.afcepf.ai107.nomads.entities.Masseur;
import fr.afcepf.ai107.nomads.entities.Sexe;

public interface MasseurIDao extends GenericIDao<Masseur> {

	List<Masseur> getListMasseurs(Sexe sexe);
	Masseur authenticate(String login, String password);
	boolean exists (Masseur masseur);
	List<Masseur> getAllInfoForMasseursActifs();
	List<Masseur> getAllInfoForMasseursInactifs(); 
	List<Masseur> getAllInfoForMasseursActifsByLvl(String niveau);
	List<Masseur> getAllInfoForMasseursInactifsByLvl(String niveau);     //UTILE???
	List<Masseur> getAllInfosForMasseursActifsByCatego (String categorie);
	Masseur getInfosbyId (Integer idMasseur);
	List<Masseur> getRecentMasseurs();

	List<Masseur> getMasseurForCreationPresta(Integer idCategorie);

	List<Masseur> getRecentMasseurs1month();
	List<Masseur> getRecentDepartMasseurs1month();
	

	
	
	
	
	
	
	
//	List<Masseur>getAllInfosForMasseurs(String actif_inactif);

}
