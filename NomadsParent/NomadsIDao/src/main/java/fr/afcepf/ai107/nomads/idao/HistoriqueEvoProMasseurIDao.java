package fr.afcepf.ai107.nomads.idao;

import java.util.List;

import fr.afcepf.ai107.nomads.entities.HistoriqueEvolutionProfessionnelleMasseur;

public interface HistoriqueEvoProMasseurIDao extends GenericIDao<HistoriqueEvolutionProfessionnelleMasseur> {

	List<HistoriqueEvolutionProfessionnelleMasseur> historiqueByIdMasseur (Integer idMasseur);
	boolean EstNeonomad (Integer idMasseur);

}
