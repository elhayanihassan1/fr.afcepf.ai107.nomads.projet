package fr.afcepf.ai107.nomads.controller.admin;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.Masseur;
import fr.afcepf.ai107.nomads.entities.Partenaire;
import fr.afcepf.ai107.nomads.masseurs.ibusiness.MasseurIBusiness;
import fr.afcepf.ai107.nomads.partenaire.ibusiness.PartenaireIBusiness;
import fr.afcepf.ai107.nomads.prestation.ibusiness.PrestationIBusiness;

@ManagedBean (name = "mbAccueilAdmin")
@ViewScoped
public class AccueilAdminManagedBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private MasseurIBusiness proxyMasseurBusiness;
	
	@EJB
	private PrestationIBusiness proxyPrestaBusiness;
	
	@EJB
	private PartenaireIBusiness proxyPartenaireBusiness;
	
	private List<Masseur> masseurs1mois;
	private List<Masseur> masseursPartis1mois;
	private List<InterventionReelle> interventions7J;
	private List<InterventionReelle> interventionsDuJour;
	private List<InterventionReelle> interventionsNonPourvues7Days;
	private Double pctPrestaPourvues7Jours;
	private List<Partenaire> partenairesActifs;
	private List<Partenaire> nouveauxPartenaires;
	private List<Partenaire> departsRecentsPart;
	
	@PostConstruct
	public void init() {
		masseurs1mois = proxyMasseurBusiness.getRecentMasseurs1month();
		masseursPartis1mois = proxyMasseurBusiness.getRecentDepartMasseurs1month();
		interventions7J = proxyPrestaBusiness.getAllInterventionFoNext7DAys();
		interventionsDuJour = proxyPrestaBusiness.getAllTodaySInterventions();
		interventionsNonPourvues7Days = proxyPrestaBusiness.getListInterventionsNonPourvues7Days();
		if(interventionsNonPourvues7Days.size() !=0 && interventions7J.size() !=0) {
		pctPrestaPourvues7Jours = (double) (1.0-(((double) interventions7J.size()-interventionsNonPourvues7Days.size())/interventions7J.size()));
		}else pctPrestaPourvues7Jours = 0.0;
		System.out.println("************************************************* J'AI CHARGÉ LE NOMBRE DES PRESATAS PREVUES POUR LES 7 PROCHAINS JOURS");
		partenairesActifs = proxyPartenaireBusiness.getPartenairesActifsAjD();
		System.out.println("************************************************* J'AI CHARGÉ LA LISTE DE PARTENAIRES ACTIFS");
		
		nouveauxPartenaires = proxyPartenaireBusiness.getNouveauxPartenaires1Month();
		System.out.println("************************************************* J'AI CHARGÉ LA LISTE DE NOUVEAUX PARTENAIRES");
		departsRecentsPart = proxyPartenaireBusiness.getDepartPartenaires1Mois();
		System.out.println("************************************************* J'AI CHARGÉ LA LISTE DE PARTENAIRES RÉCEMMENT PARTIS");
}

	public List<Masseur> getMasseurs1mois() {
		return masseurs1mois;
	}

	public void setMasseurs1mois(List<Masseur> masseurs1mois) {
		this.masseurs1mois = masseurs1mois;
	}

	public List<Masseur> getMasseursPartis1mois() {
		return masseursPartis1mois;
	}

	public void setMasseursPartis1mois(List<Masseur> masseursPartis1mois) {
		this.masseursPartis1mois = masseursPartis1mois;
	}

	public List<InterventionReelle> getInterventions7J() {
		return interventions7J;
	}

	public void setInterventions7J(List<InterventionReelle> interventions7j) {
		interventions7J = interventions7j;
	}

	public List<InterventionReelle> getInterventionsDuJour() {
		return interventionsDuJour;
	}

	public void setInterventionsDuJour(List<InterventionReelle> interventionsDuJour) {
		this.interventionsDuJour = interventionsDuJour;
	}

	public List<InterventionReelle> getInterventionsNonPourvues7Days() {
		return interventionsNonPourvues7Days;
	}

	public void setInterventionsNonPourvues7Days(List<InterventionReelle> interventionsNonPourvues7Days) {
		this.interventionsNonPourvues7Days = interventionsNonPourvues7Days;
	}

	public Double getPctPrestaPourvues7Jours() {
		return pctPrestaPourvues7Jours;
	}

	public void setPctPrestaPourvues7Jours(Double pctPrestaPourvues7Jours) {
		this.pctPrestaPourvues7Jours = pctPrestaPourvues7Jours;
	}

	public List<Partenaire> getPartenairesActifs() {
		return partenairesActifs;
	}

	public void setPartenairesActifs(List<Partenaire> partenairesActifs) {
		this.partenairesActifs = partenairesActifs;
	}

	/**
	 * @return the nouveauxPartenaires
	 */
	public List<Partenaire> getNouveauxPartenaires() {
		return nouveauxPartenaires;
	}

	/**
	 * @param nouveauxPartenaires the nouveauxPartenaires to set
	 */
	public void setNouveauxPartenaires(List<Partenaire> nouveauxPartenaires) {
		this.nouveauxPartenaires = nouveauxPartenaires;
	}

	/**
	 * @return the departsRecentsPart
	 */
	public List<Partenaire> getDepartsRecentsPart() {
		return departsRecentsPart;
	}

	/**
	 * @param departsRecentsPart the departsRecentsPart to set
	 */
	public void setDepartsRecentsPart(List<Partenaire> departsRecentsPart) {
		this.departsRecentsPart = departsRecentsPart;
	}

}
