package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.Date;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "intervention_reelle")
public class InterventionReelle implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_intervention_reelle")
	private Integer idInterventionReelle;

	@Temporal(value = TemporalType.DATE)
	@Column(name = "date_prestation")
	private Date datePrestation;
	
	@Temporal(value = TemporalType.DATE)
	@Column(name="date_annulation_intervention")
	private Date dateAnnulationIntervention;

	@ManyToOne
	@JoinColumn(name = "id_prestation", referencedColumnName = "id_prestation")
	private Prestation prestation;

	@OneToMany(mappedBy = "interventionReelle", cascade = CascadeType.PERSIST)
	private Set<InscriptionMasseurs> inscriptionMasseur;
	
	@OneToMany (mappedBy = "interventionReelle", cascade = CascadeType.PERSIST,fetch = FetchType.LAZY )
	private Set<RetourExperienceMasseur> retoursExperienceMasseurs;
	
	@OneToMany(mappedBy = "interventionReelle", cascade = CascadeType.PERSIST,fetch = FetchType.LAZY)
	private List<RetourExperiencePartenaire> retourExperiencePartenaire;
	
	@ManyToOne
	@JoinColumn(name = "id_motif_annulation_prestation", referencedColumnName = "id_motif_annulation_prestation")
	private MotifAnnulationPrestation motifAnnulationPrestation;
	
	public InterventionReelle(Prestation prestation) {
		super();
		this.prestation = prestation;
	}

	/**
	 * 
	 */
	public InterventionReelle() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	
	
	
	
	public InterventionReelle(Integer idInterventionReelle, Date datePrestation, Date dateAnnulationIntervention,
			Prestation prestation, Set<InscriptionMasseurs> inscriptionMasseur,
			Set<RetourExperienceMasseur> retoursExperienceMasseurs,
			List<RetourExperiencePartenaire> retourExperiencePartenaire,
			MotifAnnulationPrestation motifAnnulationPrestation) {
		super();
		this.idInterventionReelle = idInterventionReelle;
		this.datePrestation = datePrestation;
		this.dateAnnulationIntervention = dateAnnulationIntervention;
		this.prestation = prestation;
		this.inscriptionMasseur = inscriptionMasseur;
		this.retoursExperienceMasseurs = retoursExperienceMasseurs;
		this.retourExperiencePartenaire = retourExperiencePartenaire;
		this.motifAnnulationPrestation = motifAnnulationPrestation;
	}

	
	/**
	 * @return the dateAnnulationIntervention
	 */
	public Date getDateAnnulationIntervention() {
		return dateAnnulationIntervention;
	}

	/**
	 * @param dateAnnulationIntervention the dateAnnulationIntervention to set
	 */
	public void setDateAnnulationIntervention(Date dateAnnulationIntervention) {
		this.dateAnnulationIntervention = dateAnnulationIntervention;
	}

	/**
	 * @return the retourExperiencePartenaire
	 */
	public List<RetourExperiencePartenaire> getRetourExperiencePartenaire() {
		return retourExperiencePartenaire;
	}

	/**
	 * @param retourExperiencePartenaire the retourExperiencePartenaire to set
	 */
	public void setRetourExperiencePartenaire(List<RetourExperiencePartenaire> retourExperiencePartenaire) {
		this.retourExperiencePartenaire = retourExperiencePartenaire;
	}

	/**
	 * @return the motifAnnulationPrestation
	 */
	public MotifAnnulationPrestation getMotifAnnulationPrestation() {
		return motifAnnulationPrestation;
	}

	/**
	 * @param motifAnnulationPrestation the motifAnnulationPrestation to set
	 */
	public void setMotifAnnulationPrestation(MotifAnnulationPrestation motifAnnulationPrestation) {
		this.motifAnnulationPrestation = motifAnnulationPrestation;
	}

	/**
	 * @return the idInterventionReelle
	 */
	public Integer getIdInterventionReelle() {
		return idInterventionReelle;
	}

	/**
	 * @param idInterventionReelle the idInterventionReelle to set
	 */
	public void setIdInterventionReelle(Integer idInterventionReelle) {
		this.idInterventionReelle = idInterventionReelle;
	}

	/**
	 * @return the datePrestation
	 */
	public Date getDatePrestation() {
		return datePrestation;
	}

	/**
	 * @param datePrestation the datePrestation to set
	 */
	public void setDatePrestation(Date datePrestation) {
		this.datePrestation = datePrestation;
	}

	/**
	 * @return the prestation
	 */
	public Prestation getPrestation() {
		return prestation;
	}

	/**
	 * @param prestation the prestation to set
	 */
	public void setPrestation(Prestation prestation) {
		this.prestation = prestation;
	}

	/**
	 * @return the inscriptionMasseur
	 */
	public Set<InscriptionMasseurs> getInscriptionMasseur() {
		return inscriptionMasseur;
	}

	/**
	 * @param inscriptionMasseur the inscriptionMasseur to set
	 */
	public void setInscriptionMasseur(Set<InscriptionMasseurs> inscriptionMasseur) {
		this.inscriptionMasseur = inscriptionMasseur;
	}

	public Set<RetourExperienceMasseur> getRetoursExperienceMasseurs() {
		return retoursExperienceMasseurs;
	}

	public void setRetoursExperienceMasseurs(Set<RetourExperienceMasseur> retoursExperienceMasseurs) {
		this.retoursExperienceMasseurs = retoursExperienceMasseurs;
	}

	@Override
	public String toString() {
		return "InterventionReelle [idInterventionReelle=" + idInterventionReelle + ", datePrestation=" + datePrestation
				+ "]";
	}

	
}