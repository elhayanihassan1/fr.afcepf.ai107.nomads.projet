package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import javax.persistence.Table;

@Entity
@Table(name = "evolution_professionnelle")

public class EvolutionProfessionnelle implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_evolution_professionnelle")
	private int idEvolutionProfessionnelle;

	@Column(name = "libelle_evolution_professionnelle")
	private String libelleEvolutionProfessionnelle;
	
	@OneToMany (mappedBy = "evolutionsProfessionnelle", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private Set<HistoriqueEvolutionProfessionnelleMasseur> historiqueEvolutionProfessionnelle;

	public EvolutionProfessionnelle() {
		super();
		
	}

	public EvolutionProfessionnelle(int idEvolutionProfessionnelle, String libelleEvolutionProfessionnelle,
			Set<HistoriqueEvolutionProfessionnelleMasseur> historiqueEvolutionProfessionnelle) {
		super();
		this.idEvolutionProfessionnelle = idEvolutionProfessionnelle;
		this.libelleEvolutionProfessionnelle = libelleEvolutionProfessionnelle;
		this.historiqueEvolutionProfessionnelle = historiqueEvolutionProfessionnelle;
	}

	public int getIdEvolutionProfessionnelle() {
		return idEvolutionProfessionnelle;
	}

	public void setIdEvolutionProfessionnelle(int idEvolutionProfessionnelle) {
		this.idEvolutionProfessionnelle = idEvolutionProfessionnelle;
	}

	public String getLibelleEvolutionProfessionnelle() {
		return libelleEvolutionProfessionnelle;
	}

	public void setLibelleEvolutionProfessionnelle(String libelleEvolutionProfessionnelle) {
		this.libelleEvolutionProfessionnelle = libelleEvolutionProfessionnelle;
	}

	public Set<HistoriqueEvolutionProfessionnelleMasseur> getHistoriqueEvolutionProfessionnelle() {
		return historiqueEvolutionProfessionnelle;
	}

	public void setHistoriqueEvolutionProfessionnelle(
			Set<HistoriqueEvolutionProfessionnelleMasseur> historiqueEvolutionProfessionnelle) {
		this.historiqueEvolutionProfessionnelle = historiqueEvolutionProfessionnelle;
	}
	
}