package fr.afcepf.ai107.nomads.retours.business;

import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.Masseur;
import fr.afcepf.ai107.nomads.entities.RetourExperienceMasseur;
import fr.afcepf.ai107.nomads.idao.InterventionReelleIDao;
import fr.afcepf.ai107.nomads.idao.MasseurIDao;
import fr.afcepf.ai107.nomads.idao.PartenaireIDao;
import fr.afcepf.ai107.nomads.idao.PrestationIDao;
import fr.afcepf.ai107.nomads.idao.RetoursMasseursIDao;
import fr.afcepf.ai107.nomads.retours.ibusiness.RetoursMasseursIBusiness;

@Remote(RetoursMasseursIBusiness.class)
@Stateless
public class RetoursMasseursBusiness implements RetoursMasseursIBusiness{
	
	@EJB
	private PrestationIDao proxyPrestationDao;
	
	@EJB
	private PartenaireIDao proxyPartenaireDao;
	
	@EJB
	private InterventionReelleIDao proxyInterventionReelleDao;
	
	@EJB
	private RetoursMasseursIDao proxyRetoursMasseursDao;
	
	@EJB
	private MasseurIDao proxyMasseurDao;
	
	public List<RetourExperienceMasseur> getCurrentMonthRetourMasseur() {
		return proxyRetoursMasseursDao.getCurrentMonthRetourMasseur();
	}
 
	public RetourExperienceMasseur ajouterUnRetour(RetourExperienceMasseur monRetour) {
		return proxyRetoursMasseursDao.insertRetour(monRetour);
		
	}

	@Override
	public List<RetourExperienceMasseur> getRetourExByIdIntervention(InterventionReelle intervention, Masseur masseur) {
		// TODO Auto-generated method stub
		return proxyRetoursMasseursDao.getRetourByIdIntervention(intervention, masseur);
	}
	
	
}
