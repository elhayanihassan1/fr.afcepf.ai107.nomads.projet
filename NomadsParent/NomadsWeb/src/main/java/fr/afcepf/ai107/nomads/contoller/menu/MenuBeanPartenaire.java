package fr.afcepf.ai107.nomads.contoller.menu;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

@ManagedBean
@ViewScoped
public class MenuBeanPartenaire {
	

	private MenuModel model = new DefaultMenuModel();

	@PostConstruct //à l'initialisation, création des menus?
	public void init() {
		addMenu("Accueil", "Retour à mon Accueil");
		addMenu("Mon Profil", "Consulter mon profil");
		addMenu("Mes prestations", "Consulter mes prestations", "Demander une prestation");

	}


	//Constructeur par défaut pour ajouter des sous-menus?
	public DefaultSubMenu addMenu(String label, String... items) {  
		return addMenu(null, label, items);
	}

	//Constructeur surchargé avec un sous menu "Parent" en param
	public DefaultSubMenu addMenu(DefaultSubMenu parentMenu,String label, String... items) {

		DefaultSubMenu theMenu = new DefaultSubMenu(label);

		for (Object item : items) {
			DefaultMenuItem mi = new DefaultMenuItem(item);
			mi.setUrl(displayURL(item.toString()));
			theMenu.addElement(mi);
		}


		if (parentMenu == null) {
			model.addElement(theMenu);
		} else {
			parentMenu.addElement(theMenu);
		}
		return theMenu;
	}

	public MenuModel getMenuModel() {
		return model;
	}

	private String displayURL (String nomMenu) {


		String URLretournee = "#";
		switch (nomMenu) {
		case "Retour à mon Accueil":
			URLretournee="AccueilPartenaire.xhtml";
			break;
		case "Consulter mon profil":
			URLretournee="FichePartenaire.xhtml";
			break;

		case "Consulter mes prestations":
			URLretournee="MesPrestationsPartenaire.xhtml";
			break;
		case "Demander une prestation":
			URLretournee="DemandeDevisPartenaire.xhtml";
			break;

		default:
			break;
		}

		return URLretournee;
	}


}
