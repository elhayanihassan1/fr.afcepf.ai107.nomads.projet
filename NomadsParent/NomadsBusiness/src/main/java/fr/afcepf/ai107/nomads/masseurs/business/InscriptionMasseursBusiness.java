package fr.afcepf.ai107.nomads.masseurs.business;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai107.nomads.entities.InscriptionMasseurs;
import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.Masseur;
import fr.afcepf.ai107.nomads.idao.InscriptionMasseursIDao;
import fr.afcepf.ai107.nomads.masseurs.ibusiness.InscriptionMasseursIBusiness;

@Remote(InscriptionMasseursIBusiness.class)
@Stateless
public class InscriptionMasseursBusiness implements InscriptionMasseursIBusiness {
	
	@EJB
	private InscriptionMasseursIDao proxyInscriptionMass;


	@Override
	public List<Masseur> getMasseursInscritsPourUneIntervention(Integer idIntervention) {
		// TODO Auto-generated method stub
		List<Masseur> mesMass = proxyInscriptionMass.getMasseursInscritsSurUneIntervention(idIntervention);
		System.out.println(mesMass + "SSSSSSVVVVVVVVVPPPPPPPPPPPP BUUUUU");
		return mesMass;
	}


	@Override
	public List<InscriptionMasseurs> getInscriptionMasseurFuturesByIdMasseur(Integer idMasseur, Date dateDesinscri) {
		List<InscriptionMasseurs> inscri = null;
		inscri = proxyInscriptionMass.getInscriptionMasseurIntervAVenirByIdMasseur(idMasseur, dateDesinscri);
		return inscri;
	}


	@Override
	public void mettreAJourInscriptionMasseur(InscriptionMasseurs inscription) {
		proxyInscriptionMass.update(inscription);
		
	}


	@Override
	public InscriptionMasseurs GetInscriptionByMasseurAndInterv(Masseur masseur, InterventionReelle interv) {
		InscriptionMasseurs inscri = null;
		inscri = proxyInscriptionMass.GetInscriptionByMasseurAndInterv(masseur, interv);
		return inscri;
	}

}
